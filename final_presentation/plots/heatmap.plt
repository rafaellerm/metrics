#!/usr/bin/gnuplot

set terminal png
set output "example.png"
set title "Matriz de Correlação"
unset key
set tic scale 0

# Color runs from red to green
set palette defined (-1 "red",0 "yellow", 1 "green")
#set palette rgbformula 3,2,2
set cbrange [-1:1]
set cblabel "Coeficiente de Pearson"
set cbtics -1,1,1

set xrange [-0.5:4.5]
set yrange [-0.5:4.5]

set grid

set xtics ( \
"MP1" 0, \
"MP2" 1, \
"MP3" 2, \
"MP4" 3, \
"MP5" 4 \
)

set ytics ( \
" MB1" 0, \
" MB2" 1, \
" MB3" 2, \
" MB4" 3, \
" MB5" 4 \
)

set ylabel "Métricas de Baixo Nível"
set xlabel "Métricas de Projeto"

set view map
splot '-' matrix with image
1 0.5 0.5 1 0
-1 1 0.4 0.2 1
0.32 0.8 -0.2 1 0
0 -0.9 0 0.78.3 0
-0.4 1 0.45 0.7 -0.3
e
e

