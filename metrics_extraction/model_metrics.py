#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# TODO - make this more platform-independent
astah_cmd = "/usr/lib/astah_professional/astah-run.sh"
sdmetric_helper_jar = "./sdmetrics_helper/out/artifacts/sdmetrics_helper_jar/sdmetrics_helper.jar"

import subprocess as sp
import os
import json

sdmetric_helper_jar = os.path.realpath(os.path.join(
        os.path.split(__file__)[0],
        sdmetric_helper_jar
        ))


class ModelMetrics(object):
    def __init__(self, model_file):
        self.model_file = os.path.realpath(model_file)
        self.createXmi()
        self.loadMetrics()

    def createXmi(self):
        """Create XMI file by calling the Asth command-line interface.
        The process is not without problems - certain exceptions are thrown,
        but that does not mean no outpus is produced.

        The Astah command line program works in an unusual way. The first argument 
        is the directory in which the project file is located, the second is
        the output type, the third is the filename of the file (no directory path), and
        the last is the output file, which is always produced in the original
        directory."""
        self.xmi_file = self.model_file + ".xmi"
        parts = os.path.split(self.model_file)
        cmd_args = (astah_cmd,
            os.path.split(self.model_file)[0],
            "xml",
            parts[1],
            parts[1]+".xmi")
        with open(os.devnull,'w') as devnull:
            sp.call(cmd_args, stdout=devnull)
        # Testing if the file really exists. Even in normal situations, the
        # Astah program output exceptions when exporting the data
        #print("Model converted.")
        assert(os.path.exists(self.xmi_file))

    def loadMetrics(self):
        """Calls Java helper program that extracts metrics using 
        SDMetrics Core library."""
        cmd_args = (
            "java", "-jar",
            sdmetric_helper_jar,
            self.xmi_file
            )
        json_text = sp.check_output(cmd_args)
        jData = json.loads(json_text.decode(encoding="utf-8"))
        #pprint(data)

        # Reconstruct matrix from json data
        self.joinedMetrics = dict()
        metricsTable = jData['metrics']
        dataTable = jData['values']
        for category in metricsTable.keys():
#        for category in ['class']:
            self.joinedMetrics[category] = dict()

            cData = dataTable[category]
            for mindex, metric in enumerate(metricsTable[category]):
                metricData = [element[mindex] for element in cData.values()]
                self.joinedMetrics[category][metric] = self.joinF(metric,metricData)
        print("\tJSON data reconstructed")
    def joinF(self, metric, metricData):
        mean = lambda x : sum(x)/len(x)
        functionTable = {
            "NumCls" : sum,
            "NumCls_tc": sum,
            "NumOpsCls" : sum,
            "NumInterf" : sum,
            "R" : max,
            "H" : max,
            "Ca" : max,
            "Ce" : max,
            "I" : max,
            "A" : mean,
            "D" : mean,
            "DN" : mean,
            "Nesting" : max,
            "ConnComp" : max,
            "Dep_Out" : max,
            "Dep_In" : max,
            "DepPack" : max,
            "StimSent_Outside" : max,
            "StimRecv_Outside" : max,
            "StimSent_within" : max,
            "MsgSent_Outside" : max,
            "MsgRecv_Outside" : max,
            "MsgSent_within" : max,
            "Diags" : mean,
            "NumAttr" : max,
            "NumOps" : max,
            "NumPubOps" : max,
            "Setters" : mean,
            "Getters" : mean,
            "Nesting" : max,
            "IFImpl" : mean,
            "NOC" : max,
            "NumDesc" : max,
            "NumAnc" : max,
            "DIT" : max,
            "CLD" : max,
            "OpsInh" : max,
            "AttrInh": max,
            "Dep_Out": mean,
            "Dep_In" : mean,
            "NumAssEl_ssc" : max,
            "NumAssEl_sb" : max,
            "NumAssEl_nsb" : max,
            "EC_Attr" : max,
            "IC_Attr" : max,
            "EC_Par" : max,
            "IC_Par" : max,
            "ObjInst" : max,
            "StimSent" : mean,
            "StimRecv" : mean,
            "StimSelf" : mean,
            "ClassifInst" : max,
            "MsgSent" : max,
            "MsgRecv" : max,
            "MsgSelf" : max
        }
        try:
            return functionTable[metric](metricData)
        except KeyError:
            # Fallback is the sum function
            return max(metricData)

if __name__ == "__main__":
    from sys import argv
    assert (len(argv)>1)

    met = ModelMetrics(argv[1])

    print("Generated file "+met.xmi_file)
 
