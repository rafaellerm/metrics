#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from random import randint
import os
from sys import argv
import imp
import subprocess
import tempfile

# TODO: Is there a way to make this more portable?
und_dir = "/opt/scitools/bin/linux64/"

class UnderstandInterface(object):
    """Maintains open a command-line process for the Understand program, communicating via pipes"""
    und_command = os.path.join(und_dir,"und")
    und_process = None
    und_mod = imp.load_dynamic("understand",  os.path.join(und_dir,  "python/understand.so"))
    def __init__(self):
        super(UnderstandInterface, self).__init__()
    def calculate_dir(self,  dirname):
        """Add files in new database and calculate metrics"""
        print("\tOpening Understand instance")
        with open(os.devnull, 'w') as fnull:
            und_process = subprocess.Popen(self.und_command,
                stdin=subprocess.PIPE,
                stdout=fnull)
        
            tempname = "{}/{}.udb".format(tempfile.gettempdir(), randint(1, 1000))
            str_cmd = """create -db "{tempname}" -languages java
add "{dname}"
settings -metrics all
analyze -all
metrics
quit
""".format(
            tempname=tempname,
            dname=dirname)
 #settings -metricsOutputFile "{outfile}"
       
            #print(str_cmd)
            und_process.communicate(str_cmd.encode('utf-8'))

        print("\tCreated Understand Database")
        db = UnderstandInterface.und_mod.open(tempname)
        self.metrics = db.metric(db.metrics())
        print("\tRetrieved information from database")
#        for k,v in sorted(self.metrics.items()):
#              print (k,"=",v)

if __name__ == "__main__":
    assert(len(argv))>1
    und = UnderstandInterface()
    for i,d in enumerate(argv[1:]):
        print("Processing directory {}".format(i))
        und.calculate_dir(d, "./temp{}.csv".format(i))

