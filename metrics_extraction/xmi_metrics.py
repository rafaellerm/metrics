#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import subprocess as sp
import os
import json

# TODO - make this more platform-independent
sdmetric_helper_jar = "./sdmetrics_helper/out/artifacts/sdmetrics_helper_jar/sdmetrics_helper.jar"

sdmetric_helper_jar = os.path.realpath(os.path.join(
        os.path.split(__file__)[0],
        sdmetric_helper_jar
        ))


class XmiMetrics(object):
    def __init__(self, model_file):
        self.xmi_file = os.path.realpath(model_file)
        self.loadMetrics()

    def loadMetrics(self):
        """Calls Java helper program that extracts metrics using 
        SDMetrics Core library."""
        cmd_args = (
            "java", "-jar",
            sdmetric_helper_jar,
            self.xmi_file,
            "-xmi2"
            )
        json_text = sp.check_output(cmd_args)
        jData = json.loads(json_text.decode(encoding="utf-8"))
        # print(jData)

        # Reconstruct matrix from json data
        self.joinedMetrics = dict()
        metricsTable = jData['metrics']
        dataTable = jData['values']
        for category in metricsTable.keys():
#        for category in ['class']:
            self.joinedMetrics[category] = dict()

            cData = dataTable[category]
            for mindex, metric in enumerate(metricsTable[category]):
                metricData = [element[mindex] for element in cData.values()]
                self.joinedMetrics[category][metric] = self.joinF(metric,metricData)
        print("\tJSON data reconstructed")
    def joinF(self, metric, metricData):
        mean = lambda x : sum(x)/len(x)
        functionTable = {
            "NumCls" : sum,
            "NumCls_tc": sum,
            "NumOpsCls" : sum,
            "NumInterf" : sum,
            "R" : max,
            "H" : max,
            "Ca" : max,
            "Ce" : max,
            "I" : max,
            "A" : mean,
            "D" : mean,
            "DN" : mean,
            "Nesting" : max,
            "ConnComp" : max,
            "Dep_Out" : max,
            "Dep_In" : max,
            "DepPack" : max,
            "StimSent_Outside" : max,
            "StimRecv_Outside" : max,
            "StimSent_within" : max,
            "MsgSent_Outside" : max,
            "MsgRecv_Outside" : max,
            "MsgSent_within" : max,
            "Diags" : mean,
            "NumAttr" : max,
            "NumOps" : max,
            "NumPubOps" : max,
            "Setters" : mean,
            "Getters" : mean,
            "Nesting" : max,
            "IFImpl" : mean,
            "NOC" : max,
            "NumDesc" : max,
            "NumAnc" : max,
            "DIT" : max,
            "CLD" : max,
            "OpsInh" : max,
            "AttrInh": max,
            "Dep_Out": mean,
            "Dep_In" : mean,
            "NumAssEl_ssc" : max,
            "NumAssEl_sb" : max,
            "NumAssEl_nsb" : max,
            "EC_Attr" : max,
            "IC_Attr" : max,
            "EC_Par" : max,
            "IC_Par" : max,
            "ObjInst" : max,
            "StimSent" : mean,
            "StimRecv" : mean,
            "StimSelf" : mean,
            "ClassifInst" : max,
            "MsgSent" : max,
            "MsgRecv" : max,
            "MsgSelf" : max
        }
        try:
            return functionTable[metric](metricData)
        except KeyError:
            # Fallback is the sum function
            return max(metricData)

if __name__ == "__main__":
    from sys import argv
    assert (len(argv)>1)

    met = ModelMetrics(argv[1])

    print("Used file "+met.xmi_file)
 
