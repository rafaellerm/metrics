AvgCyclomatic	Average cyclomatic complexity for all nested functions or methods.
AvgCyclomaticModified	Average modified cyclomatic complexity for all nested functions or methods.
AvgCyclomaticStrict	Average strict cyclomatic complexity for all nested functions or methods.
AvgEssential	Average Essential complexity for all nested functions or methods.
AvgLine	Average number of lines for all nested functions or methods.
AvgLineBlank	Average number of blank for all nested functions or methods.
AvgLineCode	Average number of lines containing source code for all nested functions or methods.
AvgLineComment	Average number of lines containing comment for all nested functions or methods.
CountDeclClass	Number of classes.
CountDeclClassMethod	Number of class methods.
CountDeclClassVariable	Number of class variables.
CountDeclFile	Number of files.
CountDeclFunction	Number of functions.
CountDeclInstanceMethod	Number of instance methods. [aka NIM]
CountDeclInstanceVariable	Number of instance variables. [aka NIV]
CountDeclMethod	Number of local methods.
CountDeclMethodAll	Number of methods, including inherited ones. [aka RFC (response for class)]
CountDeclMethodDefault	Number of local default methods.
CountDeclMethodPrivate	Number of local private methods. [aka NPM]
CountDeclMethodProtected	Number of local protected methods.
CountDeclMethodPublic	Number of local public methods. [aka NPRM]
CountLine	Number of all lines. [aka NL]
CountLineBlank	Number of blank lines. [aka BLOC]
CountLineCode	Number of lines containing source code. [aka LOC]
CountLineCodeDecl	Number of lines containing declarative source code.
CountLineCodeExe	Number of lines containing executable source code.
CountLineComment	Number of lines containing comment. [aka CLOC]
CountPath	Number of possible paths, not counting abnormal exits or gotos. [aka NPATH]
CountSemicolon	Number of semicolons.
CountStmt	Number of statements.
CountStmtDecl	Number of declarative statements.
CountStmtExe	Number of executable statements.
Cyclomatic	Cyclomatic complexity.
CyclomaticModified	Modified cyclomatic complexity.
CyclomaticStrict	Strict cyclomatic complexity.
Essential	Essential complexity. [aka Ev(G)]
MaxCyclomatic	Maximum cyclomatic complexity of all nested functions or methods.
MaxCyclomaticModified	Maximum modified cyclomatic complexity of nested functions or methods.
MaxCyclomaticStrict	Maximum strict cyclomatic complexity of nested functions or methods.
MaxInheritanceTree	Maximum depth of class in inheritance tree. [aka DIT]
MaxNesting	Maximum nesting level of control constructs.
RatioCommentToCode	Ratio of comment lines to code lines.
SumCyclomatic	Sum of cyclomatic complexity of all nested functions or methods. [aka WMC]
SumCyclomaticModified	Sum of modified cyclomatic complexity of all nested functions or methods.
SumCyclomaticStrict	Sum of strict cyclomatic complexity of all nested functions or methods.
SumEssential	Sum of essential complexity of all nested functions or methods.
