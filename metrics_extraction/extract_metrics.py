#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import os
import csv

import code_metrics
import model_metrics
import xmi_metrics

# File Extension for Astah models
astah_ext = ".asta"
xmi_ext = ".xmi"
# Expected name for source directory
src_ext = "src"

def find_file(extension,  base):
    """Finds the path to the first file that has
    the defined extension, located under directory 'base'."""
    for root, dirs, files in os.walk(base):
        for file in files:
            if file.endswith(extension):
                return os.path.join(root,file)
def find_dir(name,  base):
    """Finds the path to the first directory with
    the supplied name, under directory 'base'."""
    for root, dirs, files in os.walk(base):
        for dir in dirs:
            if dir.strip().lower() == name:
                return os.path.join(root,dir)

if __name__ == "__main__":
    try:
        assert(len(sys.argv)>1)
    except AssertionError as e:
        print(
"""
Error: unsufficient arguments
usage: extract_metrics.py [-xmi2] <directory_names>
If -xmi2 is passed, the script will look for XMI v2.0 
files in the specified directories, otherwise it looks 
for Astah project files.
Output will be in "report.csv" file.
""")
        exit(1)

    first_arg = 1
    pure_xmi = False
    if sys.argv[1] == "-xmi2":
        first_arg = 2
        pure_xmi = True
        print('XMI')
    und = code_metrics.UnderstandInterface()

    availableMetrics = []
    projectsList = []
    for i, project_dir in enumerate(sys.argv[first_arg:]):
        # Find source code directory
        src_dir = find_dir(src_ext,  project_dir)

        print("Directory: "+project_dir)
#            print("\t src_dir: "+src_dir)
#            print("\t model file: "+model_file)
        
        # Code metric extraction
#            code_metrics_csv = "./temp{}.csv".format(i)
        print("\tSource directory: "+src_dir)
        und.calculate_dir(src_dir)

        # Transform astah model to XMI
        model = None
        if pure_xmi:
            model_file = find_file(xmi_ext,  project_dir)
            print("\tModel file:" +model_file)
            model = xmi_metrics.XmiMetrics(model_file)
        else:
            model_file = find_file(astah_ext,  project_dir)
            print("\tModel file:" +model_file)
            model = model_metrics.ModelMetrics(model_file)
        # Process XMI file with Java program

        # Create a unified table of the metrics
        projectMetrics = dict(und.metrics)
        for category,item in  model.joinedMetrics.items():
            for metric,value in item.items():
                projectMetrics['{}::{}'.format(category,metric)] = value
        # (Very) unpythonic, but we need to maitain the order
        for k in sorted(und.metrics.keys()):
            if k not in availableMetrics:
                availableMetrics.append(k)
        for k in sorted(projectMetrics.keys()):
            if k not in availableMetrics:
                availableMetrics.append(k)
        projectMetrics['Project'] = os.path.dirname(project_dir).split('/')[-1]
#        availableMetrics.update(projectMetrics.keys())
        projectsList.append(projectMetrics)

    with open('report.csv', 'w', newline='') as outF:
        csvF = csv.DictWriter(outF, ['Project']+availableMetrics, 
            delimiter=";", 
            extrasaction='ignore')
        csvF.writeheader()
        csvF.writerows(projectsList)


