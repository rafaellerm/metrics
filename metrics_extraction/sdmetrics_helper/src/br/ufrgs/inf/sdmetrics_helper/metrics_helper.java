package br.ufrgs.inf.sdmetrics_helper;

import com.sdmetrics.metrics.Metric;
import com.sdmetrics.metrics.MetricStore;
import com.sdmetrics.metrics.MetricsEngine;
import com.sdmetrics.model.*;
import com.sdmetrics.util.XMLParser;

import java.net.URL;
import java.net.URLDecoder;
import java.util.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class metrics_helper {

    public static void main(String[] args) throws Exception {
        String xmiFile = args[0];       // XMI file with the UML model

        String metaModelURL = "/com/sdmetrics/resources/metamodel.xml";  // metamodel definition to use
        String metricsURL = "/com/sdmetrics/resources/metrics.xml";
        String xmiTransURL = "/com/sdmetrics/resources/xmiTrans1_1.xml";   // XMI tranformations to use

        if (args.length > 1 && args[1].equals("-xmi2")){
            metaModelURL = "/com/sdmetrics/resources/metamodel2.xml";  // metamodel definition to use
            metricsURL = "/com/sdmetrics/resources/metrics2.xml";
            xmiTransURL = "/com/sdmetrics/resources/xmiTrans2_0.xml";   // XMI tranformations to use
        }

        String[] filters = { "#.java", "#.javax", "#.org.xml" };

        metaModelURL = Object.class.getResource(metaModelURL).toExternalForm();
        xmiTransURL = Object.class.getResource(xmiTransURL).toExternalForm();
        metricsURL = Object.class.getResource(metricsURL).toExternalForm();

        XMLParser parser;
        MetaModel metaModel;
        try {
            parser = new XMLParser();
            metaModel = new MetaModel();
            parser.parse(metaModelURL, metaModel.getSAXParserHandler());
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        XMITransformations trans = new XMITransformations(metaModel);
        parser.parse(xmiTransURL, trans.getSAXParserHandler());

        Model model = new Model(metaModel);
        XMIReader xmiReader = new XMIReader(trans, model);
        parser.parse(xmiFile, xmiReader);
        model.setFilter(filters, false, false);

        // Read the metric definition file
        MetricStore metricStore = new MetricStore(metaModel);
        parser.parse(metricsURL, metricStore.getSAXParserHandler());

        MetricsEngine metricsEngine = new MetricsEngine(metricStore, model);

        // Find metamodel elements that have metrics
        HashMap<MetaModelElement, Collection<Metric>> typeMetrics = new HashMap<MetaModelElement, Collection<Metric>>();
        for(MetaModelElement metaEl : metaModel) {
            Collection<Metric> availableMetrics = new ArrayList<Metric>(metricStore.getMetrics(metaEl));
            // Remove internal metrics
            Iterator<Metric> it = availableMetrics.iterator();
            while( it.hasNext() ) {
                Metric m = it.next();
                if( m.isInternal() ) it.remove();
            }
            if(!availableMetrics.isEmpty()) {
                typeMetrics.put(metaEl, availableMetrics);
            }
        }

        JSONObject jsonFile = new JSONObject();

//        // Shows the available metrics for each type in the metamodel
//        for(Map.Entry<MetaModelElement, Collection<Metric>> it : typeMetrics.entrySet()) {
//            Collection<ModelElement> typeElements = model.getAcceptedElements(it.getKey());
//            if(typeElements.isEmpty()) {
//                continue;
//            }
//
//            System.out.println("Type "+it.getKey().getName());
//            for(Metric m : it.getValue()) {
//                if(!m.isInternal()) {
//                    System.out.println("\t"+m.getName()+"\t"+m.getBriefDescription());
//                }
//            }
//        }


        JSONObject valuesObj = new JSONObject();
        JSONObject metricsObj = new JSONObject();
        // Iterate through all elements by type
        for(Map.Entry<MetaModelElement,Collection<Metric>> metaEl : typeMetrics.entrySet()){
            Collection<ModelElement> typeElements = model.getAcceptedElements(metaEl.getKey());
            if(typeElements.isEmpty()) {
                continue;
            }

            JSONObject typeObj = new JSONObject();
            JSONArray namesObj = new JSONArray();

            for(Metric m : metaEl.getValue()) {
                namesObj.add(m.getName());
            }

//            System.out.println("\n\nMetric for "+metaEl.getKey().getName());
//            for (Metric metric : metaEl.getValue()){
//                System.out.print("\t"+metric.getName());
//            }

            for(ModelElement element : typeElements){
//                System.out.print("\n"+URLDecoder.decode(element.getFullName(), "UTF-8"));
                JSONArray rowArr = new JSONArray();

                for(Metric metric : metaEl.getValue()){
//                    System.out.print("\t"+metricsEngine.getMetricValue(element, metric));
                    rowArr.add(metricsEngine.getMetricValue(element, metric));
                }

                typeObj.put(URLDecoder.decode(element.getFullName(), "UTF-8"), rowArr);
            }

            valuesObj.put(URLDecoder.decode(metaEl.getKey().getName(), "UTF-8"), typeObj);
            metricsObj.put(URLDecoder.decode(metaEl.getKey().getName(), "UTF-8"), namesObj);
        }
//        System.out.print('\n');

        jsonFile.put("metrics", metricsObj);
        jsonFile.put("values", valuesObj);
        System.out.println(jsonFile.toJSONString());

    }

}
