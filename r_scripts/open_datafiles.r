# Function definitions
ReturnZeroVarianceColumns = function(mat){
    colVars <- apply(mat, 2, var);
#Set variance threshold, 
#store columns that are <= that threshold
    isConstant <-(colVars <= 0);
    return(isConstant);
}
KeepColumns = function(mat, colList) { 
    return(mat[,colList]); 
}

#########################################################################################

# Default files
soft_file = "../metrics_extraction/report.csv"
physical_file = "../physical_metrics/table.csv"

# If filenames were passed by commandline, use them
args <- commandArgs(trailingOnly = TRUE)
# print(args)
if (length(args) > 0) {
	soft_file <- args[1]
}
if (length(args) > 1) {
	physical_file <- args[2]
}

# Opening data files
file = read.csv(file=soft_file, sep=";", row.names=1, header=TRUE)
softMetrics = as.matrix(file)

# Remove columns regarded as useless (i.e. too much missing information)
source('useless_cols.r')


idx <- which(is.na(softMetrics), arr.ind=TRUE)
softMetrics[idx] <- 0

file = read.csv(file=physical_file, sep=";", row.names=1, header=TRUE)
phys = as.matrix(file)

# Separating model and code metrics
model <- softMetrics[,-1:-46]
code <- softMetrics[,1:46]
# Removing list of unwanted cols
model <- model[ , !(colnames(model) %in% softRemove)]
code <- code[ , !(colnames(code) %in% softRemove)]
phys <- phys[ , !(colnames(phys) %in% softRemove)]
# Removing constant cols 
model <- KeepColumns(model, !ReturnZeroVarianceColumns(model))
code <- KeepColumns(code, !ReturnZeroVarianceColumns(code))


phys_limits = 1:(dim(phys)[1]/3)
##########################################################################################
 
