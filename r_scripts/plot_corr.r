source('open_datafiles.r')

# Calculation and plot of correlation matrixes
library(corrplot)

# Correlation between code and physical metrics
corrMat = cor(code, phys[phys_limits,], use="complete.obs")

idx <- which(is.na(corrMat), arr.ind=TRUE)
corrMat[idx] <- 0
corrplot(corrMat, method="color", tl.cex=0.6)
#corrplot(corrMat, method="color", order="hclust", tl.cex=0.6)

# Correlation between model and physical metrics
corrMat = cor(model, phys[phys_limits,], use="complete.obs")

idx <- which(is.na(corrMat), arr.ind=TRUE)
corrMat[idx] <- 0
corrplot(corrMat, method="color", tl.cex=0.6)


# # correlation between physical metrics
# corrMat = cor(phys[phys_limits,], phys[phys_limits,], use="complete.obs")

# idx <- which(is.na(corrMat), arr.ind=TRUE)
# corrMat[idx] <- 0
# corrplot(corrMat, method="color", tl.cex=0.6)


# Correlation between code metrics
# corrMat = cor(code, code, use="complete.obs")
# idx <- which(is.na(corrMat), arr.ind=TRUE)
# corrMat[idx] <- 0
# corrplot(corrMat, method="color", tl.cex=0.6)

# Correlation between model metrics
# corrMat = cor(model, model, use="complete.obs")
# idx <- which(is.na(corrMat), arr.ind=TRUE)
# corrMat[idx] <- 0
# corrplot(corrMat, method="color", tl.cex=0.6)

 
library(caret)

corrMat = cor(phys, phys, use="complete.obs")
toRemove <- findCorrelation(corrMat, cutoff = .95, verbose = FALSE)
phys2 <- phys[,-toRemove]


corrMat = cor(code, code, use="complete.obs")
toRemove <- findCorrelation(corrMat, cutoff = .95, verbose = FALSE)
code2 <- code[,-toRemove]
corrMat = cor(code2, phys2[phys_limits,], use="complete.obs")
idx <- which(is.na(corrMat), arr.ind=TRUE)
corrMat[idx] <- 0
corrplot(corrMat, method="color", tl.cex=0.6, cl.ratio=2)


corrMat = cor(model, model, use="complete.obs")
toRemove <- findCorrelation(corrMat, cutoff = .95, verbose = FALSE)
model2 <- model[,-toRemove]
corrMat = cor(model2,  phys2[phys_limits,], use="complete.obs")
idx <- which(is.na(corrMat), arr.ind=TRUE)
corrMat[idx] <- 0
corrplot(corrMat, method="color", tl.cex=0.6, cl.ratio=2)

corrMat = cor(code2, model2[phys_limits,], use="complete.obs")
idx <- which(is.na(corrMat), arr.ind=TRUE)
corrMat[idx] <- 0
corrplot(corrMat, method="color", tl.cex=0.6)
