 
plotScatterCorrelated = function(A, B, correlation, threshold=0.8){
	relevant <- abs(correlation) > threshold;
	# print (relevant)
	for (aMetric in 1:dim(relevant)[1]){
		for (bMetric in 1:dim(relevant)[2])
			if (relevant[aMetric, bMetric]){
				plot(A[,aMetric],B[,bMetric], xlab=colnames(A)[aMetric], ylab=colnames(B)[bMetric]);
				abline(lm(B[,bMetric] ~ A[,aMetric]));
				# title(main="test", xlab=colnames(A)[aMetric], ylab=colnames(B)[bMetric])
				# print(colnames(A)[aMetric])
				# print(colnames(B)[bMetric])
			}
	}
}
