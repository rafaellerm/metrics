source('open_datafiles.r')
source("plotScatterCorrelated.r")

# Calculation and plot of correlation matrixes
library(corrplot)

.pardefault <- par()

# Correlation between code metrics
codeCorr = cor(code, code, use="complete.obs")
idx <- which(is.na(codeCorr), arr.ind=TRUE)
codeCorr[idx] <- 0
corrplot(codeCorr, method="color", tl.cex=0.6, type=c("lower"))

# Correlation between model metrics
modelCorr = cor(model, model, use="complete.obs")
idx <- which(is.na(modelCorr), arr.ind=TRUE)
modelCorr[idx] <- 0
corrplot(modelCorr, method="color", tl.cex=0.6, type=c("lower"))

# Correlation between physical metrics
physCorr = cor(phys[phys_limits,], phys[phys_limits,], use="complete.obs")
idx <- which(is.na(physCorr), arr.ind=TRUE)
physCorr[idx] <- 0
corrplot(physCorr, method="color", tl.cex=0.6, type=c("lower"))

par(.pardefault)

diag(codeCorr) <-0
diag(modelCorr) <-0
diag(physCorr) <-0
plotScatterCorrelated(code, code, codeCorr)
plotScatterCorrelated(model, model, modelCorr)
plotScatterCorrelated(phys, phys, physCorr)
