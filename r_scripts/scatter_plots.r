source("open_datafiles.r") 
source("plotScatterCorrelated.r")
phys = phys[phys_limits,]
# Calculation and plot of correlation matrixes
library(corrplot)


# Correlation between code and physical metrics
codeCorr = cor(code, phys, use="complete.obs")
# Correlation between model and physical metrics
modelCorr = cor(model, phys, use="complete.obs")
# correlation between physical metrics
physCorr = cor(phys, phys, use="complete.obs")

diag(codeCorr) <- 0
plotScatterCorrelated(code, phys, codeCorr)
diag(codeCorr) <- 0
plotScatterCorrelated(model, phys, modelCorr)
diag(physCorr) <- 0
plotScatterCorrelated(phys, phys, physCorr)
# sdev <- apply(code, 2, var)^.5
# m <- colMeans(code)