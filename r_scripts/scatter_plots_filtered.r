source("open_datafiles.r") 
source("plotScatterCorrelated.r")

# Filtering correlated features
library(caret)

corrMat = cor(phys, phys, use="complete.obs")
toRemove <- findCorrelation(corrMat, cutoff = .95, verbose = FALSE)
phys <- phys[,-toRemove]
phys = phys[phys_limits,]

corrMat = cor(code, code, use="complete.obs")
toRemove <- findCorrelation(corrMat, cutoff = .95, verbose = FALSE)
code <- code[,-toRemove]

corrMat = cor(model, model, use="complete.obs")
toRemove <- findCorrelation(corrMat, cutoff = .95, verbose = FALSE)
model <- model[,-toRemove]


# Calculation and plot of correlation matrixes
library(corrplot)


# Correlation between code and physical metrics
codeCorr = cor(code, phys, use="complete.obs")
# Correlation between model and physical metrics
modelCorr = cor(model, phys, use="complete.obs")
# correlation between physical metrics
physCorr = cor(phys, phys, use="complete.obs")

diag(codeCorr) <- 0
plotScatterCorrelated(code, phys, codeCorr, threshold=0.6)
diag(codeCorr) <- 0
plotScatterCorrelated(model, phys, modelCorr, threshold=0.6)
diag(physCorr) <- 0
plotScatterCorrelated(phys, phys, physCorr, threshold=0.6)
# sdev <- apply(code, 2, var)^.5
# m <- colMeans(code)