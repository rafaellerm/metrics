USER_DELAY=$2
PERF_LEVEL=$3

if [ $# -lt 2 ]
then
	echo Missing argument
	exit
fi

cd $1

JAR=${1}.jar
PERF="perf stat --output ../perf.log -e cycles${PERF_LEVEL} -e instructions${PERF_LEVEL}"

${PERF} java -jar ${JAR} 500

PERF="perf stat --output ../cache.log -e cache-references${PERF_LEVEL} -e cache-misses${PERF_LEVEL}"

${PERF} java -jar ${JAR} 500
