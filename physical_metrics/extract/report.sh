#!/bin/sh

if [ $# -lt 1 ]
then
	echo "Usage: $0 <cycles, instructions, misses, references or _N>"
	exit
fi

printf "%15s ; %15s ; %15s ; %15s\n" "id" "attribute" "mean" "stddev"
find ./ -name stats -exec cat {} + | grep $1 | cut -d' ' -f 2- | sed 's/\"//g'
