#!/usr/bin/python

import sys

freq       = 1.66e9
mem_access  = 8e-9
mem_leak = 0.709
pe_power      = 2.42

f = open(sys.argv[1], 'r')

sys.stdout.write("%15s;%15s;%15s;%15s;%15s;%15s;%15s;%15s;%15s;%15s;%15s;%15s\n" % ( "id", "instructions", "cycles", "cache access", "cache miss", "cpi", "energy (J)", "eps (J/s)", "pe_energy (J)", "mem_energy (J)", "mem_leak (J)", "mem_access (J)"))

for l in f:
    lv           = l.split(";")
    inst         = float(lv[1])
    cycles       = float(lv[2])
    access       = float(lv[3])
    miss         = float(lv[4])

    cpi          = cycles/inst
    time         = cycles/freq

    pe_energy    = pe_power * time

    mem_access_e = mem_access * miss
    mem_leak_e  = mem_leak * time
    mem_energy   = mem_access_e + mem_leak_e

    energy       = pe_energy + mem_energy

    eps          = energy / time

    sys.stdout.write("%15s;%15.2e;%15.2e;%15.2e;%15.2e;%15.2e;%15.2e;%15.4e;%15.2e;%15.2e;%15.2e;%15.2e\n" % (lv[0], inst, cycles, access, miss, cpi, energy, eps, pe_energy, mem_energy, mem_leak_e, mem_access_e))
