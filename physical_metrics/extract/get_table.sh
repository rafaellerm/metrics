TARGETS=('all user kernel')
TABLE=table.csv
TMP_TABLE=/tmp/${TABLE}

rm -f $TMP_TABLE

for j in ${TARGETS[@]}
do
	for i in $(seq 0 5)
	do
		echo $j $i
		./make_table.r "${j}/${i}" | cut -d\" -f 2 >> $TMP_TABLE
	done
done

./energy.py $TMP_TABLE > $TABLE
