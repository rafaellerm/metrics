TARGETS=('all user kernel')

for j in ${TARGETS[@]}
do
	cd $j
	for i in $(seq 0 5)
	do
		cd $i
		cat perf.log | grep cycles | awk '{print $1}' > cycles.data
		cat perf.log | grep instructions | awk '{print $1}' > instructions.data
		cat cache.log | grep references | awk '{print $1}' > references.data
		cat cache.log | grep misses | awk '{print $1}' > misses.data
		cat cache.log | grep misses | awk '{print $4}' > misses_perc.data
		sed -i 's/,//g' *.data
		../../stats.r ${j}_${i}
		cd ..
	done
	cd ../
done
