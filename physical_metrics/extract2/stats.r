#!/usr/bin/Rscript

args <- commandArgs(trailingOnly=TRUE)
id <- args[1]

cycles<-read.table(file="cycles.data", header=FALSE)
instructions<-read.table(file="instructions.data", header=FALSE)
references<-read.table(file="references.data", header=FALSE)
misses<-read.table(file="misses.data", header=FALSE)
misses_perc<-read.table(file="misses_perc.data", header=FALSE)

sink(file="stats")
sprintf("%15s ; %15s ; %15.2e ; %15.2e", id, "cycles", sapply(cycles, mean), sapply(cycles, sd))
sprintf("%15s ; %15s ; %15.2e ; %15.2e", id, "instructions", sapply(instructions, mean), sapply(instructions, sd))
sprintf("%15s ; %15s ; %15.2e ; %15.2e", id, "references", sapply(references, mean), sapply(references, sd))
sprintf("%15s ; %15s ; %15.2e ; %15.2e", id, "misses", sapply(misses, mean), sapply(misses, sd))
sprintf("%15s ; %15s ; %15.2f ; %15.2f", id, "misses_perc", sapply(misses_perc, mean), sapply(misses_perc, sd))
