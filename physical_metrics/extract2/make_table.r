#!/usr/bin/Rscript

args <- commandArgs(trailingOnly=TRUE)
id <- args[1]

cycles<-read.table(file=paste(id, "cycles.data", sep="/"), header=FALSE)
instructions<-read.table(file=paste(id, "instructions.data", sep="/"), header=FALSE)
references<-read.table(file=paste(id, "references.data", sep="/"), header=FALSE)
misses<-read.table(file=paste(id, "misses.data", sep="/"), header=FALSE)
#misses_perc<-read.table(file="misses_perc.data", header=FALSE)

#sprintf("%15s;%15s;%15s;%15s;%15s", "id", "instructions", "cycles", "cache access", "cache miss")
sprintf("%15s;%15.2e;%15.2e;%15.2e;%15.2e", id, sapply(instructions, mean), sapply(cycles, mean), sapply(references, mean), sapply(misses, mean))
