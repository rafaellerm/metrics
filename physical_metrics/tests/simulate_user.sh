USER_DELAY=$2
PERF_LEVEL=$3

if [ $# -lt 2 ]
then
	echo Missing argument
	exit
fi

cd $1

JAR=${1}.jar
PERF="perf stat --output ../perf.log -e cycles${PERF_LEVEL} -e instructions${PERF_LEVEL}"

# Your application goes here
git checkout ./*

cat inst | \
grep -v "#" | \
while read line; do echo $line ; sleep $USER_DELAY; done | \
${PERF} \
java -jar ${JAR}

git checkout ./*
# Your application ends here

PERF="perf stat --output ../cache.log -e cache-references${PERF_LEVEL} -e cache-misses${PERF_LEVEL}"
cat inst | \
grep -v "#" | \
while read line; do echo $line ; sleep $USER_DELAY; done | \
${PERF} \
java -jar ${JAR}
