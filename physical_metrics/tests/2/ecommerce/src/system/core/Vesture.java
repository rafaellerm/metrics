package system.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import system.enumerators.VestureGender;
import system.enumerators.VestureSize;

public class Vesture extends Product {
	
	private VestureSize size;
	private VestureGender gender;
	
	public Vesture(){}
	
	public Vesture(int Code, 
				   String Description, 
				   String Size, 
				   float Price, 
				   int NumberInStock, 
				   String Color, 
				   String Gender)
	{
		super(Code, Description, Price, NumberInStock, Color);
		
		switch(Size.toUpperCase())
		{
			case "PP":
				this.size = VestureSize.PP;
				break;
			case "P":
				this.size = VestureSize.P;
				break;
			case "M":
				this.size = VestureSize.M;
				break;
			case "G":
				this.size = VestureSize.G;
				break;
			case "XG":
				this.size = VestureSize.XG;
				break;
			default:
				break;
		}
		
		switch(Gender.toLowerCase())
		{
			case "feminino":
				this.gender = VestureGender.FEMININO;
				break;
			case "masculino":
				this.gender = VestureGender.MASCULINO;
				break;
			case "unissex":
				this.gender = VestureGender.UNISSEX;
				break;
			default:
				break;
		}
		
	}
	
	
	public VestureGender getGender()
	{
		return this.gender;
	}
	
	public VestureSize getSize()
	{
		return this.size;
	}
	
	public void UpdateStockFile() throws IOException
	{
		FileWriter eletroFileWriter = null;
		FileReader eletroFileReader;
		BufferedReader eletroReader = null;
		
		String oldFileName = "vestuario.txt";
		String tmpFileName = "vestuario_tmp.txt";

		eletroFileWriter = new FileWriter(tmpFileName);
		eletroFileReader = new FileReader(oldFileName);
		eletroReader = new BufferedReader(eletroFileReader);
			
		String line;
		while((line = eletroReader.readLine()) != null)
		{
			String[] eletroData = line.split(",");
			
			if(Integer.parseInt(eletroData[0]) == this.getCode())
			{
				eletroFileWriter.write(eletroData[0] + "," +
									    eletroData[1] + "," + 
										eletroData[2] + "," +
									    eletroData[3] + "," +
									    String.valueOf(this.getNumberInStock()) + "," +
									    eletroData[5] + "," +
									    eletroData[6] + 
									    System.getProperty("line.separator"));
			}
			else
			{
				eletroFileWriter.write(line + System.getProperty("line.separator"));
			}
		}

		
		eletroFileWriter.close();
		eletroReader.close();
		
		File oldFile = new File(oldFileName);
		oldFile.delete();
		
		File newFile = new File(tmpFileName);
		newFile.renameTo(oldFile);
	}
	
}
