package system.core;

import java.io.*;

public class Eletronic extends Product {
	private float height;
	private float width;
	private float depth;
	
	
	public Eletronic(){}
	
	public Eletronic(int Code, 
					String Description,
					float Height,
					float Width, 
					float Depth, 
					float Price, 
					int NumberInStock, 
					String Color)
	{
		super(Code, Description, Price, NumberInStock, Color);
		this.height = Height;
		this.width = Width;
		this.depth = Depth;
	}
	
	
	public float getHeight()
	{
		return this.height;
	}
	
	public float getWidth()
	{
		return this.width;
	}
	
	public float getDepth()
	{
		return this.depth;
	}
	
	public void UpdateStockFile() throws IOException
	{
		FileWriter eletroFileWriter = null;
		FileReader eletroFileReader;
		BufferedReader eletroReader = null;
		
		String oldFileName = "eletro.txt";
		String tmpFileName = "eletro_tmp.txt";

		eletroFileWriter = new FileWriter(tmpFileName);
		eletroFileReader = new FileReader(oldFileName);
		eletroReader = new BufferedReader(eletroFileReader);
			
		String line;
		while((line = eletroReader.readLine()) != null)
		{
			String[] eletroData = line.split(",");
			
			if(Integer.parseInt(eletroData[0]) == this.getCode())
			{
				eletroFileWriter.write(eletroData[0] + "," +
									    eletroData[1] + "," + 
										eletroData[2] + "," +
									    eletroData[3] + "," +
										eletroData[4] + "," +
									    eletroData[5] + "," +
									    String.valueOf(this.getNumberInStock()) + "," +
									    eletroData[7] + 
									    System.getProperty("line.separator"));
			}
			else
			{
				eletroFileWriter.write(line + System.getProperty("line.separator"));
			}
		}

		
		eletroFileWriter.close();
		eletroReader.close();
		
		File oldFile = new File(oldFileName);
		oldFile.delete();
		
		File newFile = new File(tmpFileName);
		newFile.renameTo(oldFile);
	}
	
}
