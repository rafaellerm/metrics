package system.core;


import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

import org.junit.Test;

import system.Ecommerce;

public class PurchaseTest {

	@Test
	public void testSavePurchase() throws IOException {
		Ecommerce.LOGWRITER = new FileWriter("log.txt", true);
		Date data = new Date(System.currentTimeMillis());
		Purchase purchase = new Purchase(1, data, 1, 2, (float)10.0, (float)20.0);
		purchase.savePurchase();
		
	}

}
