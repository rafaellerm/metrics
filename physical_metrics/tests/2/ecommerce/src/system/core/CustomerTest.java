package system.core;

import static org.junit.Assert.*;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import system.Ecommerce;

public class CustomerTest {
	
	Date data = new Date(System.currentTimeMillis()); 
	Customer Cliente = new Customer(9, "Name", "LastName", "Password", "CPF", "Adress", data);
	
	@Before
	public void init() throws NumberFormatException, IOException {
		Ecommerce.loadProductData();
		Ecommerce.loadProductData();
		Ecommerce.loadPurchaseData();
		Ecommerce.LOGWRITER = new FileWriter("log.txt", true);
	}
	
	@Test
	public void testCreateShoppingCart() {
		Cliente.createShoppingCart();
		assertTrue(Cliente.hasCreatedPurchase());
	}
	
	@Test
	public void testHasCreatedPurchase() {
		assertFalse(Cliente.hasCreatedPurchase());
		Cliente.createShoppingCart();
		assertTrue(Cliente.hasCreatedPurchase());
		Cliente.cancelCart();
		assertFalse(Cliente.hasCreatedPurchase());
	}
	
	@Test
	public void testCancelCart() {
		Cliente.cancelCart();
		assertFalse(Cliente.hasCreatedPurchase());
	}
	
	@Test
	public void testRemoveProductFromCart() {
		Cliente.createShoppingCart();
		Cliente.removeProductFromCart(9, 1);
		Cliente.addProductInCart(9, 10);
		Cliente.removeProductFromCart(9, 8);
		ShoppingCart Cart = Cliente.getShoppingCart();
		List<CartProduct> currentProducts = Cart.getProducts();
		for(CartProduct cartProduct : currentProducts)
		{
			Product product = cartProduct.getProduct();
			if(product.getCode() == 9)
			{
				assertTrue(cartProduct.getQuantity()==2);
			}
		}
	}
	
	@Test
	public void testAddProductInCart()  {
		Cliente.createShoppingCart();
		Cliente.addProductInCart(9, 10);
		ShoppingCart Cart = Cliente.getShoppingCart();
		List<CartProduct> currentProducts = Cart.getProducts();
		for(CartProduct cartProduct : currentProducts)
		{
			Product product = cartProduct.getProduct();
			if(product.getCode() == 9)
			{
				assertTrue(cartProduct.getQuantity()==10);
			}
		}
	}
	
	@Test
	public void testCompletePurchase() {
		System.out.println("CompetePurchase:");
		Cliente.createShoppingCart();
		Cliente.addProductInCart(13, 5);
		Cliente.completePurchase();
	}
	
	@Test
	public void testShowItems() {
		System.out.println("ShowItems:");
		Cliente.createShoppingCart();
		Cliente.addProductInCart(6, 5);
		Cliente.showItemsOnCart();
	}

}
