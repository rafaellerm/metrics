package system.core;

import static org.junit.Assert.*;

import org.junit.Test;

public class CartProductTest {

	Eletronic product = new Eletronic(1, "Desc", 10, 9, 8, (float) 50.4, 1500, "cor");
	CartProduct cartProduct = new CartProduct(product, 10);
	
	@Test
	public void testGetProduct() {
		assertTrue(product.equals(cartProduct.getProduct()));
	}
	
	@Test
	public void testGetQuantity() {
		assertTrue(cartProduct.getQuantity()==10);
	}
	
	@Test
	public void testAddQuantity() {
		cartProduct.addQuantity(18);
		assertTrue(cartProduct.getQuantity()==28);
	}

}
