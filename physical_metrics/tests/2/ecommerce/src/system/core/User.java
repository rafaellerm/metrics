package system.core;

public class User {
	
	private String name;
	private String lastName;
	private int acessCode;
	private String password;
	
	public User(){}
	
	public User(int AcessCode, String Name, String LastName, String Password)
	{
		this.acessCode = AcessCode;
		this.name = Name;
		this.lastName = LastName;
		this.password = Password;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public String getLastName()
	{
		return this.lastName;
	}
	
	public int getAcessCode()
	{
		return this.acessCode;
	}
	
	public String getPassword()
	{
		return this.password;
	}
	
	
}
