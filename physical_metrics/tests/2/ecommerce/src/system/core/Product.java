package system.core;


import java.io.IOException;

public abstract class Product {
	
	private int code;
	private String description;
	private float price;
	private int numberInStock;
	private String color;
	
	public Product(){}
	
	public Product(int Code, String Description, float Price, int NumberInStock, String Color)
	{
		this.code = Code;
		this.description = Description;
		this.price = Price;
		this.numberInStock = NumberInStock;
		this.color = Color;
	}
	
	public static boolean isDescriptionValid(String description)
	{
		if(description.length() > 140)
		{
			return false;
		}
		else
			return true;
	}
	
	public int getCode()
	{
		return this.code;
	}
	
	public String getDescription()
	{
		return this.description;
	}
	
	public float getPrice()
	{
		return this.price;
	}
	
	public int getNumberInStock()
	{
		return this.numberInStock;
	}
	
	public void setNumberInStock(int NumberInStock)
	{
		this.numberInStock = NumberInStock;
	}
	
	public String getColor()
	{
		return this.color;
	}
	
	public abstract void UpdateStockFile() throws IOException;

	
	
}
