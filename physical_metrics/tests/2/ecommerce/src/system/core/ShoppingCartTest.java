package system.core;

import static org.junit.Assert.*;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import system.Ecommerce;

public class ShoppingCartTest {
	
	ShoppingCart Cart = new ShoppingCart();
	
	@Before
	public void init() throws NumberFormatException, IOException{
		Ecommerce.loadProductData();
		Ecommerce.loadProductData();
		Ecommerce.loadPurchaseData();
		Ecommerce.LOGWRITER = new FileWriter("log.txt", true);
		Cart.addItem(2, 3);
		Cart.addItem(10, 2);
		Cart.showItems();
	}
	
	@Test
	public void testRemoveItem() {
		Cart.removeItem(2, 1);
		List<CartProduct> currentProducts = Cart.getProducts();
		for(CartProduct cartProduct : currentProducts)
		{
			Product product = cartProduct.getProduct();
			if(product.getCode() == 2)
			{
				assertTrue(cartProduct.getQuantity()==2);
			}
		}
	}
	
	@Test
	public void testAddItem() {
		Cart.addItem(10, 1);
		List<CartProduct> currentProducts = Cart.getProducts();
		for(CartProduct cartProduct : currentProducts)
		{
			Product product = cartProduct.getProduct();
			if(product.getCode() == 10)
			{
				assertTrue(cartProduct.getQuantity()==3);
			}
		}
	}
	
	@Test
	public void testCompletePucrhase() {
		System.out.println("Complete Purchase:");
		Cart.completePurchase(1);
		
		
	}
}
