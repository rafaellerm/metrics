package system.core;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import system.Ecommerce;

public class ShoppingCart {
	
	private List<CartProduct> products;
	private float totalValue;
	
	public ShoppingCart(){
		this.products = new ArrayList<CartProduct>();
	}
	
	public float getTotalValue(){
		return this.totalValue;
	}
	public List<CartProduct> getProducts(){
		return this.products;
	}
	
	public void addItem(int itemCode, int quantity)
	{
		for(CartProduct cartProduct : this.products)
		{
			Product product = cartProduct.getProduct();
			if(product.getCode() == itemCode)
			{
				if(product.getNumberInStock() >= quantity)
				{
					cartProduct.addQuantity(quantity);
					System.out.println("Product(s) added to cart.");
					return;
				}
				else
				{
					System.out.println("There's not enough of this product on stock.");
				}
			}

		}
		
		for(Product product : Ecommerce.PRODUCTLIST)
		{
			if(product.getCode() == itemCode)
			{
				if(product.getNumberInStock() >= quantity)
				{
					CartProduct newCartProduct = new CartProduct(product, quantity);
					this.products.add(newCartProduct);
					this.totalValue = this.totalValue + (product.getPrice() * (float)quantity);
					System.out.println("Product(s) added to cart.");
					return;
				}
				else
				{
					System.out.println("There's not enough of this product on stock.");
				}
			}
		}
		
		System.out.println("Inexistent code.");
		
		
	}
	

	public void removeItem(int itemCode, int quantity)
	 {
		
		for(CartProduct cartProduct : this.products)
		{
			if(cartProduct.getProduct().getCode() == itemCode)
			{
				if(cartProduct.getQuantity() <= quantity)
				{
					this.totalValue = this.totalValue - (cartProduct.getProduct().getPrice() * (float)cartProduct.getQuantity());
					this.products.remove(cartProduct);
					System.out.println("Product(s) removed from cart.");
					return;
				}
				else
				{
					this.totalValue = this.totalValue - (cartProduct.getProduct().getPrice() * (float)quantity);
					cartProduct.addQuantity(-quantity);
					System.out.println("Product(s) removed from cart.");
					return;
				}
			}
		}
		
		System.out.println("There's no product with tihs code on your cart.");
	 }
	
	public void showItems()
	{
		if(this.products.isEmpty())
		{
			System.out.println("Your cart is empty.");
		}
		else
		{
			for(CartProduct cartProduct : this.products)
			{
				Product product = cartProduct.getProduct();
				System.out.println("Product Code:" + product.getCode() + " Description:" + product.getDescription() + " Quantity:" + cartProduct.getQuantity() + " Unit Value:" + product.getPrice());
			}
		}
	}
	
	public void completePurchase(int customerCode)
	{
		Date purchaseDate = new Date();
		int vestureQuantity = 0;
		int eletronicQuantity = 0;
		float vestureAmountSpent = 0;
		float eletronicAmountSpent = 0;
		
		for(CartProduct cartProduct : this.products)
		{
			Product product = cartProduct.getProduct();
			if(product instanceof Eletronic)
			{
				eletronicQuantity += cartProduct.getQuantity();
				eletronicAmountSpent += product.getPrice() * cartProduct.getQuantity();
			}
			else
			{
				vestureQuantity += cartProduct.getQuantity();
				vestureAmountSpent += product.getPrice() * cartProduct.getQuantity();
			}
			try
			{
				int newStock = product.getNumberInStock() - cartProduct.getQuantity();
				product.setNumberInStock(newStock);
				product.UpdateStockFile();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		Purchase purchase = new Purchase(customerCode, 
										 purchaseDate, 
										 vestureQuantity, 
										 eletronicQuantity, 
										 vestureAmountSpent, 
										 eletronicAmountSpent);
		
		Ecommerce.PURCHASELIST.add(purchase);
		
		purchase.savePurchase();
	}
	

	 
}
