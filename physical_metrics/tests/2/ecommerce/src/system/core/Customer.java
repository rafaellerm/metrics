package system.core;

import java.util.Date;


public class Customer extends User {
	private String cpf;
	private String address;
	private Date dayOfBirth;
	private ShoppingCart shoppingCart;
	
	public Customer(){}
	
	public Customer(int AcessCode, String Name, String LastName, String Password, String Cpf, String Adress, Date DayOfBirth)
	{
		super(AcessCode, Name, LastName, Password);
		
		this.cpf = Cpf;
		this.address = Adress;
		this.dayOfBirth = DayOfBirth;
		
	}
	
	public ShoppingCart getShoppingCart(){
		return this.shoppingCart;
	}

	
	public void createShoppingCart()
	 {
			this.shoppingCart = new ShoppingCart();
	 }


	
	public boolean hasCreatedPurchase()
	{
		if(this.shoppingCart != null)
		{
			return true;
		}
		
		else
		{
			return false;
		}
	}
	
	public void addProductInCart(int productCode, int quantity)
	{
		this.shoppingCart.addItem(productCode, quantity);
	}
	
	public void removeProductFromCart(int productCode, int quantity)
	{
		this.shoppingCart.removeItem(productCode, quantity);
	}
	
	public void showItemsOnCart()
	{
		this.shoppingCart.showItems();
	}
	
	public void cancelCart()
	{
		this.shoppingCart = null;
	}
	
	public void completePurchase()
	{
		this.shoppingCart.completePurchase(this.getAcessCode());
		this.cancelCart();
	}
	
	public String getAddress()
	{
		return this.address;
	}
	
	public String getCpf()
	{
		return this.cpf;
	}
	
	public Date getDayOfBirth()
	{
		return this.dayOfBirth;
	}

}
