package system.core;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import system.Ecommerce;

public class Purchase {
	private int customerCode;
	private Date purchaseDate;
	private int vestureQuantity;
	private int eletronicQuantity;
	private float vestureAmountSpent;
	private float eletronicAmountSpent;
	
	public Purchase(){}
	
	public Purchase(int CustomerCode, 
					Date PurchaseDate,
					int VestureQuantity,
					int EletronicQuantity,
					float VestureAmountSpent,
					float EletronicAmountSpent)
	{
		this.customerCode = CustomerCode;
		this.purchaseDate = PurchaseDate;
		this.vestureQuantity = VestureQuantity;
		this.eletronicQuantity = EletronicQuantity;
		this.vestureAmountSpent = VestureAmountSpent;
		this.eletronicAmountSpent = EletronicAmountSpent;
	}
	

	public void savePurchase()
	{
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		try {
			Ecommerce.LOGWRITER.write(String.valueOf(this.customerCode) + "," + 
									  //String.valueOf(this.purchaseDate.getDay()) + "/" + String.valueOf(this.purchaseDate.getMonth()) + "/" + String.valueOf(this.purchaseDate.getYear()) + "," +
									  dateFormat.format(this.purchaseDate) + "," +
									  String.valueOf(this.vestureQuantity) + "," +
									  String.valueOf(this.eletronicQuantity) + "," +
									  String.valueOf(this.vestureAmountSpent) + "," +
									  String.valueOf(this.eletronicAmountSpent) + 
									  System.getProperty("line.separator"));
			
			Ecommerce.LOGWRITER.flush();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	
	}
	
	public Date getPurchaseDate()
	{
		return this.purchaseDate;
	}
	
	public float getVestureAmountSpent()
	{
		return this.vestureAmountSpent;
	}
	
	public float getEletronicAmountSpent()
	{
		return this.eletronicAmountSpent;
	}
	
}
