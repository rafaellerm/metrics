package system.core;

public class CartProduct {
	
	private Product product;
	private int quantity;
	
	public CartProduct(){}
	
	public CartProduct(Product Product, int Quantity)
	{
		this.product = Product;
		this.quantity = Quantity;
	}
	
	public Product getProduct()
	{
		return this.product;
	}
	
	public int getQuantity()
	{
		return this.quantity;
	}
	
	public void addQuantity(int Quantity)
	{
		this.quantity = this.quantity + Quantity;
	}

}
