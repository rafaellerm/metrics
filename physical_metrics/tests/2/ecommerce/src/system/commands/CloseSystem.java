package system.commands;

import system.enumerators.CommandResponse;
import system.ui.*;

public class CloseSystem extends Command {

	public CloseSystem(){}
	
	public CloseSystem(String Description, UserInterface CurrentInterface)
	{
		super(Description, CurrentInterface);
	}
	
	@Override
	public CommandResponse execute() {
		
		if(this.getCurrentInterface() != null)
		{
			Logout logoutCommand = new Logout("Logout", this.getCurrentInterface());
			
			CommandResponse logoutResponse = logoutCommand.execute();
		
			if(logoutResponse == CommandResponse.STOP)
			{
				return CommandResponse.CLOSE;
			}
			else
			{
				return logoutResponse;
			}
		}
		else
		{
			return CommandResponse.CLOSE;
		}
		
		

	}

}
