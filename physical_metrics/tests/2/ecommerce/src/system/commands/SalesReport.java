package system.commands;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import system.Ecommerce;
import system.enumerators.CommandResponse;
import system.ui.UserInterface;
import system.core.*;

public class SalesReport extends Command {

	public SalesReport(){}
	
	public SalesReport(String Description, UserInterface CurrentInterface)
	{
		super(Description, CurrentInterface);
	}

	@Override
	public CommandResponse execute() {
		
		boolean isInformationValid = false;
		
		int day = 0;
		int month = 0;
		int year = 0;
		float totalValue = 0;
		
		while(!isInformationValid)
		{
			System.out.println("Type the report's date:");
			try {
				System.out.println("Day:");
				day = Integer.parseInt(Ecommerce.IOREADER.readLine());
				
				System.out.println("Month:");
				month = Integer.parseInt(Ecommerce.IOREADER.readLine());
				
				System.out.println("Year:");
				year = Integer.parseInt(Ecommerce.IOREADER.readLine());
				
				isInformationValid = true;
				
			} 
			catch (Exception e) {
				System.out.println("Invalid information.");
				e.printStackTrace();
				continue;
			}			
		}
		
		boolean foundPurchase = false;
		
		for(Purchase purchase : Ecommerce.PURCHASELIST)
		{
			Date date = purchase.getPurchaseDate();
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

			String[] dateString = dateFormat.format(date).split("/");
			int dateDay = 0;
		    int dateMonth = 0;
		    int dateYear = 0;
		    
		    try {
				dateDay = Integer.parseInt(dateString[0]);
				dateMonth = Integer.parseInt(dateString[1]);
			    dateYear = Integer.parseInt(dateString[2]);
		    }
		    catch(Exception e)
		    {
		    	e.printStackTrace();
		    }
		    
			if(dateDay == day && dateMonth == month && dateYear == year)
			{
				totalValue += purchase.getEletronicAmountSpent() + purchase.getVestureAmountSpent();
				foundPurchase = true;
			}
					
		}
		
		if(!foundPurchase)
		{
			System.out.println("There were no purchases on this day.");
		}
		else
		{
			System.out.println("Total value on purchases this day: " + totalValue);
		}
		
		
		

		return CommandResponse.CONTINUE;
	}

}
