package system.commands;

import system.enumerators.CommandResponse;
import system.ui.UserInterface;
import system.core.*;

public class NewPurchase extends Command {

	public NewPurchase(){}
	
	public NewPurchase(String Description, UserInterface CurrentInterface)
	{
		super(Description, CurrentInterface);
	}
	@Override
	public CommandResponse execute() {
		Customer customer = (Customer)this.getCurrentInterface().getCurrentUser();

		if(customer.hasCreatedPurchase())
		{
			System.out.println("You've already started a purchase.");
		}
		else
		{
			customer.createShoppingCart();
			System.out.println("Purchase started, now you can add products to your cart.");
		}
		
		return CommandResponse.CONTINUE;
	}

}
