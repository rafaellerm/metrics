package system.commands;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;

import system.core.Customer;
import system.ui.UserInterface;

public class CancelPurchaseTest {
	
	Date data = new Date(System.currentTimeMillis()); 
	Customer Cliente = new Customer(9, "Name", "LastName", "Password", "CPF", "Adress", data);
	UserInterface CurrentInterface = new UserInterface(Cliente);
	CancelPurchase CmdCancelPurchase = new CancelPurchase("Descricao", CurrentInterface);
	
	@Test
	public void testExecute() {
		CmdCancelPurchase.execute();
		assertFalse(Cliente.hasCreatedPurchase());
	}

}
