package system.commands;

import static org.junit.Assert.*;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

import org.junit.Test;

import system.Ecommerce;
import system.core.Customer;
import system.ui.UserInterface;

public class CompletePurchaseTest {

	Date data = new Date(System.currentTimeMillis()); 
	Customer Cliente = new Customer(9, "Name", "LastName", "Password", "CPF", "Adress", data);
	UserInterface CurrentInterface = new UserInterface(Cliente);
	CompletePurchase CmdCompletePurchase = new CompletePurchase("Descricao", CurrentInterface);
	
	@Test
	public void testExecute() throws NumberFormatException, IOException {
		Ecommerce.loadProductData();
		Ecommerce.loadPurchaseData();
		Ecommerce.LOGWRITER = new FileWriter("log.txt", true);
		CmdCompletePurchase.execute();
		Cliente.createShoppingCart();
		assertTrue(Cliente.hasCreatedPurchase());
		Cliente.addProductInCart(11, 2);
		CmdCompletePurchase.execute();
		assertFalse(Cliente.hasCreatedPurchase());
	}

}
