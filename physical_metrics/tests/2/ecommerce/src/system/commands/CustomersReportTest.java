package system.commands;



import java.io.IOException;
import java.text.ParseException;


import org.junit.Test;

import system.Ecommerce;

import system.core.Manager;
import system.ui.UserInterface;

public class CustomersReportTest {

	Manager Gerente = new Manager(30, "Name", "LastName", "Password");
	UserInterface CurrentInterface = new UserInterface(Gerente);
	CustomersReport CmdCustomersReport = new CustomersReport("Descricao", CurrentInterface);
	
	@Test
	public void testExecute() throws NumberFormatException, IOException, ParseException {
		Ecommerce.loadUserData();
		CmdCustomersReport.execute();
	}
}
