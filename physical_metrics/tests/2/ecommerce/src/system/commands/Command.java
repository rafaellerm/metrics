package system.commands;

import system.enumerators.CommandResponse;
import system.ui.*;

public abstract class Command {
	
	private UserInterface currentInterface;
	private String description;
	
	public Command(){}
	
	public Command(String Description, UserInterface CurrentInterface)
	{
		this.description = Description;
		this.currentInterface = CurrentInterface;
	}
	
	public String getDescription()
	{
		return this.description;
	}
	
	public UserInterface getCurrentInterface()
	{
		return this.currentInterface;
	}
	
	public abstract CommandResponse execute();
}
