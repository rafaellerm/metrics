package system.commands;

import system.Ecommerce;
import system.core.*;
import system.enumerators.CommandResponse;
import system.ui.UserInterface;

public class Logout extends Command {
	
	public Logout(){}
	
	public Logout(String Description, UserInterface CurrentInterface)
	{
		super(Description, CurrentInterface);
	}

	@Override
	public CommandResponse execute() {
		
		boolean isOptionValid = false;
		
		while(!isOptionValid)
		{
			System.out.println("Y - Yes");
			System.out.println("N - No");
			System.out.print("Are you sure you want to logout? ");
			String choosenOption = "";
			try
			{
				choosenOption = Ecommerce.IOREADER.readLine();
			}
			catch(Exception e)
			{
				System.out.println("Invalid Option.");
				e.printStackTrace();
				continue;
			}
			
			if(choosenOption.toUpperCase().equals("Y"))
			{
				User loggedUser = this.getCurrentInterface().getCurrentUser();
				
				if(loggedUser instanceof Customer)
				{
					Customer loggedCustomer = (Customer)loggedUser;
					
					loggedCustomer.cancelCart();
				}
				
				return CommandResponse.STOP;
				
			}
			if(choosenOption.toUpperCase().equals("N"))
			{
				return CommandResponse.CONTINUE;
			}
			else
			{
				System.out.println("Invalid Option.");
			}
		}
		
		return CommandResponse.CONTINUE;	
		
	}

}
