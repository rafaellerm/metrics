package system.commands;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;

import system.core.Customer;
import system.ui.UserInterface;

public class NewPurchaseTest {

	Date data = new Date(System.currentTimeMillis());
	Customer Cliente = new Customer(9, "Name", "LastName", "Password", "CPF", "Adress", data);
	UserInterface CurrentInterface = new UserInterface(Cliente);
	NewPurchase CmdNewPurchase = new NewPurchase("Descricao", CurrentInterface);
	
	@Test
	public void testExecute() {
		CmdNewPurchase.execute();
		assertTrue(Cliente.hasCreatedPurchase());
	}

}
