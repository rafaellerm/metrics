package system.commands;

import system.core.Customer;
import system.enumerators.CommandResponse;
import system.ui.UserInterface;

public class CompletePurchase extends Command {

	public CompletePurchase(){}
	
	public CompletePurchase(String Description, UserInterface CurrentInterface)
	{
		super(Description, CurrentInterface);
	}
	@Override
	public CommandResponse execute() {

		Customer customer = (Customer)this.getCurrentInterface().getCurrentUser();

		if(!customer.hasCreatedPurchase())
		{
			System.out.println("You haven't started a purchase yet.");
		}
		else
		{
			customer.completePurchase();
			System.out.println("Purchase completed.");
		}
		
		return CommandResponse.CONTINUE;
	}

}
