package system.commands;

import system.Ecommerce;
import system.core.*;
import system.enumerators.CommandResponse;
import system.ui.UserInterface;

public class CustomersReport extends Command {

	public CustomersReport(){}
	
	public CustomersReport(String Description, UserInterface CurrentInterface)
	{
		super(Description, CurrentInterface);
	}
	@Override
	public CommandResponse execute() {
		
		System.out.println("Customers list:");
		
		for(User selectedUser : Ecommerce.USERSLIST)
		{
			if(selectedUser instanceof Customer)
			{
				Customer selectedCustomer = (Customer)selectedUser;
				
				System.out.println("Acess code:" + selectedCustomer.getAcessCode() + 
								   " Name:" + selectedCustomer.getName() + 
								   " Last Name:" + selectedCustomer.getLastName() +
								   " CPF: " + selectedCustomer.getCpf());
				
								
			}
		}
		
		return CommandResponse.CONTINUE;
	}

}
