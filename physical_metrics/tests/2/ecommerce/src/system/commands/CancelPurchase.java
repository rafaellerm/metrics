package system.commands;

import system.core.Customer;
import system.enumerators.CommandResponse;
import system.ui.*;

public class CancelPurchase extends Command {

	public CancelPurchase(){}
	
	public CancelPurchase(String Description, UserInterface CurrentInterface){
		super(Description, CurrentInterface);
	}
	
	@Override
	public CommandResponse execute() {
		
		Customer customer = (Customer)this.getCurrentInterface().getCurrentUser();

		if(!customer.hasCreatedPurchase())
		{
			System.out.println("You haven't started a purchase yet.");
		}
		else
		{
			customer.cancelCart();
			System.out.println("Purchase canceled.");
		}
		
		return CommandResponse.CONTINUE;
	}

}
