package system.commands;

import system.Ecommerce;
import system.core.Product;
import system.enumerators.CommandResponse;
import system.ui.UserInterface;

public class StockReport extends Command {

	public StockReport(){}
	
	public StockReport(String Description, UserInterface CurrentInterface)
	{
		super(Description, CurrentInterface);
	}
	@Override
	public CommandResponse execute() {
		
		for(Product product : Ecommerce.PRODUCTLIST)
		{
			System.out.println("Code:" + product.getCode() +
							  " Description:" + product.getDescription() + 
							  " Number In Stock:" + product.getNumberInStock());
			
		}
			
		return CommandResponse.CONTINUE;
	}

}
