package system.commands;

import system.Ecommerce;
import system.core.*;
import system.enumerators.CommandResponse;
import system.ui.*;

public class Login extends Command {

	
	public Login(){}
	
	public Login(String Description, UserInterface CurrentInterface)
	{
		super(Description, CurrentInterface);
	}
	
	@Override
	public CommandResponse execute() {
		
		boolean isCombinationValid = false;
		User loggedUser = null;
		
		while(!isCombinationValid)
		{
			String acessCode, password;
			
			System.out.println("Type your Acess code and Password. Type '-1' to exit");
			
			try
			{
				System.out.print("Acess Code: ");
				acessCode = Ecommerce.IOREADER.readLine();
				
				if(acessCode.equals("-1"))
				{
					return CommandResponse.STOP;
				}
					
				
				System.out.print("Password: ");
				password = Ecommerce.IOREADER.readLine();
				
				if(password.equals("-1"))
					return CommandResponse.STOP;
				
				loggedUser = this.getUser(acessCode, password);
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
				continue;
			}
			
			loggedUser = this.getUser(acessCode, password);
			
			if(loggedUser != null)
			{
				isCombinationValid = true;
			}
			else
			{
				System.out.println("Invalid Combination.");
			}
		}
		
		if(loggedUser instanceof Manager)
		{
			ManagerInterface nextInterface = new ManagerInterface(loggedUser);			
			return nextInterface.showUI(); 
		}
		if(loggedUser instanceof Customer)
		{
			CustomerInterface nextInterface = new CustomerInterface(loggedUser);
			return nextInterface.showUI();
		}
		
		return CommandResponse.CONTINUE;
		
				
	}
	
	public User getUser(String acessCode, String password) 
	{
		int AcessCode = Integer.parseInt(acessCode);
		User loggedUser = null;
		
		for(User user : Ecommerce.USERSLIST)
		{
			if(user.getAcessCode() == AcessCode && user.getPassword().equals(password))
				loggedUser = user;
		}
		
		return loggedUser;
	}
	
	

}
