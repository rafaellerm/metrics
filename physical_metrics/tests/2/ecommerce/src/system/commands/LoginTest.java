package system.commands;

import static org.junit.Assert.*;

import java.io.IOException;
import java.text.ParseException;

import org.junit.Test;

import system.Ecommerce;

import system.ui.UserInterface;

public class LoginTest {

	UserInterface CurrentInterface = new UserInterface();
	Login login = new Login("Login", CurrentInterface);
	
	@Test
	public void testGetUser() throws NumberFormatException, ParseException, IOException {
		Ecommerce.loadUserData();
		assertNull(login.getUser("1", "123"));
		assertNotNull(login.getUser("1", "abc"));
	}

}
