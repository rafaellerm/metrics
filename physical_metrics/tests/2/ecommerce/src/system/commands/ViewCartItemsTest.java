package system.commands;


import java.io.IOException;
import java.util.Date;

import org.junit.Test;

import system.Ecommerce;
import system.core.Customer;
import system.ui.UserInterface;

public class ViewCartItemsTest {

	@Test
	public void test() throws NumberFormatException, IOException {
		Ecommerce.loadProductData();
		Date data = new Date(System.currentTimeMillis()); 
		Customer Cliente = new Customer(9, "Name", "LastName", "Password", "CPF", "Adress", data);
		UserInterface CurrentInterface = new UserInterface(Cliente);
		ViewCartItems CmdViewCartItems = new ViewCartItems("Descricao", CurrentInterface);
		Cliente.createShoppingCart();
		Cliente.addProductInCart(11, 2);
		Cliente.addProductInCart(5, 1);
		CmdViewCartItems.execute();
	}

}
