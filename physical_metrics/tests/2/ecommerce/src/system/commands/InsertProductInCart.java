package system.commands;


import system.Ecommerce;
import system.enumerators.CommandResponse;
import system.ui.UserInterface;
import system.core.*;

public class InsertProductInCart extends Command {

	public InsertProductInCart(){}
	
	public InsertProductInCart(String Description, UserInterface CurrentInterface)
	{
		super(Description, CurrentInterface);
	}
	@Override
	public CommandResponse execute() {
		Customer customer = (Customer)this.getCurrentInterface().getCurrentUser();
		
		if(customer.hasCreatedPurchase())
		{
			boolean isInformationValid = false;
			
			while(!isInformationValid)
			{
				System.out.println("Type the product code:");
				int productCode;
				try {
					productCode = Integer.parseInt(Ecommerce.IOREADER.readLine());
				} 
				catch (Exception e) {
					System.out.println("Invalid code.");
					e.printStackTrace();
					continue;
				}
				
				System.out.println("Type the quantity:");
				int quantity;
				try {
					quantity = Integer.parseInt(Ecommerce.IOREADER.readLine());
				} 
				catch (Exception e) {
					System.out.println("Invalid code.");
					e.printStackTrace();
					continue;
				}
				
				customer.addProductInCart(productCode, quantity);
				isInformationValid = true;
			}
		}
		else
		{
			System.out.println("You haven't started a purchase yet.");
		}
		
		return CommandResponse.CONTINUE;
	}

}
