package system.commands;


import java.io.IOException;

import system.Ecommerce;
import system.core.*;
import system.enumerators.CommandResponse;
import system.ui.UserInterface;

public class SearchProduct extends Command {

	public SearchProduct(){}
	
	public SearchProduct(String Description, UserInterface CurrentInterface)
	{
		super(Description, CurrentInterface);
	}
	@Override
	public CommandResponse execute() {
		
		boolean isValidOption = false;
		
		while(!isValidOption)
		{
			System.out.println("What kind of product are you searching for?");
			System.out.println("0 - Eletronic");
			System.out.println("1 - Vesture");
			System.out.println("2 - Show them all");
			System.out.println("3 - Cancel search");
			
			int option = 0;
			
			try {
				option = Integer.parseInt(Ecommerce.IOREADER.readLine());
				
				switch(option)
				{
					case 0:
						this.searchEletronics();
						isValidOption = true;
						break;
					case 1:
						this.searchVesture();
						isValidOption = true;
						break;
					case 2:
						this.showAllProducts();
						isValidOption = true;
						break;
					case 3:
						return CommandResponse.CONTINUE;
					default:
						System.out.println("Invalid option.");		
				}	
				
			} 
			catch (NumberFormatException | IOException e) {
				System.out.println("Invalid option.");
				continue;
			}
					
		}
	
		return CommandResponse.CONTINUE;
	}
	
	public void showAllProducts()
	{
		for(Product product : Ecommerce.PRODUCTLIST)
		{
			System.out.println("Code:" + product.getCode() +
							  " Description:" + product.getDescription() +
							  " Price:" + product.getPrice());
		}
	}
	
	public void searchEletronics() throws NumberFormatException, IOException
	{

		boolean isValidOption = false;
		int option = 0;
		String value="";
		while(!isValidOption)
		{
			try{
				System.out.println("Which characteristic do you want to search by?");
				System.out.println("0 - Description");
				System.out.println("1 - Height");
				System.out.println("2 - Width");
				System.out.println("3 - Depth");
				System.out.println("4 - Price");
				System.out.println("5 - Color");
				System.out.println("6 - Cancel Search");
				
				option = Integer.parseInt(Ecommerce.IOREADER.readLine());
				
				if (option>=0 && option <=6)
					isValidOption = true;
				else
					System.out.println("Invalid option.");
				
			}catch(Exception e){
				System.out.println("Invalid option.");
			}
			
		}
		
		if (option == 6)
			return;
		
		System.out.println("What is the value of this characteristic?");
		value = Ecommerce.IOREADER.readLine();
		
		for(Product product : Ecommerce.PRODUCTLIST)
		{
			if(product instanceof Eletronic)
			{
				if (option==0 && product.getDescription().equals(value))
					showEletronicProduct(product);				
				if (option==1 && ((Eletronic) product).getHeight() == Float.parseFloat(value))
					showEletronicProduct(product);				
				if (option==2 && ((Eletronic) product).getWidth() == Float.parseFloat(value))
					showEletronicProduct(product);				
				if (option==3 && ((Eletronic) product).getDepth() == Float.parseFloat(value))
					showEletronicProduct(product);			
				if (option==4 && product.getPrice() == Float.parseFloat(value))
					showEletronicProduct(product);
				if (option==5 && product.getColor().equals(value))
					showEletronicProduct(product);
			}
		}
	}

	public void searchVesture() throws NumberFormatException, IOException
	{
		boolean isValidOption = false;
		int option = 0;
		String value="";
		while(!isValidOption)
		{
			try{
				System.out.println("Which characteristic do you want to search by?");
				System.out.println("0 - Description");
				System.out.println("1 - Size");
				System.out.println("2 - Gender");
				System.out.println("3 - Price");
				System.out.println("4 - Color");
				System.out.println("5 - Cancel Search");
				
				option = Integer.parseInt(Ecommerce.IOREADER.readLine());
				
				if (option>=0 && option <=5)
					isValidOption = true;
				else
					System.out.println("Invalid option.");
				
			}catch(Exception e){
				System.out.println("Invalid option.");
			}
			
		}
		
		if (option == 5)
			return;
		
		System.out.println("What is the value of this characteristic?");
		value = Ecommerce.IOREADER.readLine();
		
		for(Product product : Ecommerce.PRODUCTLIST)
		{
			if(product instanceof Vesture)
			{
				if (option==0 && product.getDescription().equals(value))
					showVestureProduct(product);				
				if (option==1 && ((Vesture) product).getSize().equals(value))
					showVestureProduct(product);				
				if (option==2 && ((Vesture) product).getGender().equals(value))
					showVestureProduct(product);						
				if (option==3 && product.getPrice() == Float.parseFloat(value))
					showVestureProduct(product);
				if (option==4 && product.getColor().equals(value))
					showVestureProduct(product);
			}
		}
	}
	
	public void showEletronicProduct(Product product){
		System.out.println("Code:" + product.getCode() +
				  " Description:" + product.getDescription() + 
				  " Number In Stock:" + product.getNumberInStock() +
				  " Price:" + product.getPrice() +
				  " Color:" + product.getColor() +
				  " Number In Stock:" + product.getNumberInStock() +
				  " Height:" + ((Eletronic) product).getHeight() +
				  " Width:" + ((Eletronic) product).getWidth() +
				  " Depth:" + ((Eletronic) product).getDepth()
				  );
	}
	
	public void showVestureProduct(Product product){
		System.out.println("Code:" + product.getCode() +
				  " Description:" + product.getDescription() + 
				  " Number In Stock:" + product.getNumberInStock() +
				  " Price:" + product.getPrice() +
				  " Color:" + product.getColor() +
				  " Number In Stock:" + product.getNumberInStock() +
				  " Gender:" + ((Vesture) product).getGender() +
				  " Size:" + ((Vesture) product).getSize()
				  );
	}
}
