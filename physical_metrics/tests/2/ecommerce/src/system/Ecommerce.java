package system;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import system.core.*;
import system.enumerators.CommandResponse;
import system.ui.LoginInterface;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;



public class Ecommerce {
	
	public static List<Product> PRODUCTLIST;
	public static List<User> USERSLIST;
	public static List<Purchase> PURCHASELIST;
	public static BufferedReader IOREADER;
	public static FileWriter LOGWRITER;
	
	public Ecommerce() {
	}
	 
	public static void main(String[] args) throws IOException, ParseException
	{
		Ecommerce.loadSystemData();
		Ecommerce.IOREADER = new BufferedReader(new InputStreamReader(System.in));
		Ecommerce.LOGWRITER = new FileWriter("log.txt", true);
		
		
		LoginInterface initialInterface = new LoginInterface();
		
		CommandResponse behavior = CommandResponse.CONTINUE;
		
		while(behavior != CommandResponse.CLOSE)
		{
			behavior = initialInterface.showUI();
		}
		
		Ecommerce.LOGWRITER.close();

		System.out.println("System closed.");
		
	}
	
	
	public static void loadSystemData() 
	{
		try
		{
			loadUserData();
			loadProductData();	
			loadPurchaseData();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	public static void loadPurchaseData() throws IOException
	{
		PURCHASELIST = new ArrayList<Purchase>();
		
		FileReader logFile = new FileReader("log.txt");
		BufferedReader logReader = new BufferedReader(logFile);
		
		String line;
		
		while((line = logReader.readLine()) != null)
		{
			String[] purchaseData = line.split(",");
			
			int CustomerCode = Integer.parseInt(purchaseData[0]);

			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			Date PurchaseDate = new Date();
			
			try{
				PurchaseDate = dateFormat.parse(purchaseData[1]);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			int VestureQuantity = Integer.parseInt(purchaseData[2]);
			int EletronicQuantity = Integer.parseInt(purchaseData[3]);
			float VestureAmountSpent = Float.parseFloat(purchaseData[4]);
			float EletronicAmountSpent = Float.parseFloat(purchaseData[5]);
			
			Purchase purchase = new Purchase(CustomerCode,
											 PurchaseDate,
											 VestureQuantity,
											 EletronicQuantity,
											 VestureAmountSpent,
											 EletronicAmountSpent);
			
			PURCHASELIST.add(purchase);
		}
		
		logReader.close();
	}
	
	public static void loadUserData() throws ParseException, NumberFormatException, IOException
	{
		USERSLIST = new ArrayList<User>();
		
		FileReader usersFile = new FileReader("usuarios.txt");
		BufferedReader usersReader = new BufferedReader(usersFile);
		String line;
		
		while((line = usersReader.readLine()) != null)
		{
			String[] userData = line.split(",");
			
			String[] fullName = userData[1].split(" ");
			String userType = userData[2];
			
			int AcessCode = Integer.parseInt(userData[0]);
			String Name = fullName[0];
			
			String LastName = "";
			for(int i = 1; i < fullName.length; i++)
				LastName = LastName + fullName[i] + " ";
			
			String Password = userData[3];
			
			if(userType.toLowerCase().equals("gerente"))
			{
				Manager user = new Manager(AcessCode, 
										   Name, 
										   LastName, 
										   Password);
				
				USERSLIST.add(user);
			}
			
			if(userType.toLowerCase().equals("cliente"))
			{
				String Cpf = userData[4];
				String Address = userData[5];
				
				DateFormat df = new SimpleDateFormat("dd/mm/yyyy");
				Date DayOfBirth = new Date();
				

				DayOfBirth = df.parse(userData[6]);

				
				Customer user = new Customer(AcessCode,
											 Name, 
											 LastName, 
											 Password,
											 Cpf,
											 Address,
											 DayOfBirth);
				
			
				USERSLIST.add(user);
			}
				
		}
		
		usersFile.close();
	}
	
	public static void loadProductData() throws NumberFormatException, IOException
	{
		PRODUCTLIST = new ArrayList<Product>();
		
		FileReader eletroFile = new FileReader("eletro.txt");
		BufferedReader eletroReader = new BufferedReader(eletroFile);
		
		FileReader vestureFile = new FileReader("vestuario.txt");
		BufferedReader vestureReader = new BufferedReader(vestureFile);
		
		String line;
		while((line = eletroReader.readLine()) != null)
		{
			
			String[] productData = line.split(","); //splits the read line in a vector with the attributes of the product (on the document the attributes are split by commas)
			
			int Code = Integer.parseInt(productData[0]);
			String Description = productData[1];
			float Height = Float.parseFloat(productData[2]);
			float Width = Float.parseFloat(productData[3]);
			float Depth = Float.parseFloat(productData[4]);
			float Price = Float.parseFloat(productData[5]);
			int NumberInStock = Integer.parseInt(productData[6]);
			String Color = productData[7];
			
			Eletronic eletronic = new Eletronic(Code,
									  			Description, 
									  			Height, 
									  			Width,
									  			Depth,
									  			Price, 
									  			NumberInStock,
									  			Color); 
			
			PRODUCTLIST.add(eletronic);			
		}
		
		while((line = vestureReader.readLine()) != null)
		{
			String[] productData = line.split(",");
			
			int Code = Integer.parseInt(productData[0]);
			String Description = productData[1];
			String Size = productData[2];
			float Price = Float.parseFloat(productData[3]);
			int NumberInStock = Integer.parseInt(productData[4]);
			String Color = productData[5];
			String Gender = productData[6];
			
			Vesture vesture = new Vesture(Code,
										  Description,
										  Size,
										  Price,
										  NumberInStock,
										  Color,
										  Gender);
			
			PRODUCTLIST.add(vesture);
		}
		
		vestureFile.close();
		eletroFile.close();
	}
	
	
}
