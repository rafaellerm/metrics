package system.enumerators;

public enum VestureGender {
	FEMININO, MASCULINO, UNISSEX
}
