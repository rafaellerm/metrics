package system.enumerators;

public enum VestureSize {
	PP, P, M, G, XG
}
