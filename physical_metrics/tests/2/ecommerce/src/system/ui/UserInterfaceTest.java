package system.ui;

import static org.junit.Assert.*;

import org.junit.Test;

import system.core.User;

public class UserInterfaceTest {

	@Test
	public void test() {
		User User = new User(9, "Name", "LastName", "Password");
		UserInterface Interface = new UserInterface(User);
		assertTrue(User.equals(Interface.getCurrentUser())); 
		
	}

}
