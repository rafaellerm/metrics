package system.ui;

import system.core.User;
import system.commands.*;

public class ManagerInterface extends UserInterface {

	public ManagerInterface(){}
	
	public ManagerInterface(User CurrentUser)
	{
		super(CurrentUser);
		
		this.addCommand(new CustomersReport("Customers Report", this));
		this.addCommand(new SalesReport("Sales Report", this));
		this.addCommand(new StockReport("Stock Report", this));
		this.addCommand(new SearchProduct("Search Product", this));
		this.addCommand(new Logout("Logout", this));
		this.addCommand(new CloseSystem("Close System", this));
	}
	
	
}
