package system.ui;

import system.core.User;

public class UserInterface extends Interface {
	private User currentUser;
	
	public UserInterface(){}
	
	public UserInterface(User CurrentUser)
	{
		this.currentUser = CurrentUser;
	}
	
	public User getCurrentUser()
	{
		return this.currentUser;
	}
}
