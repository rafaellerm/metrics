package system.ui;

import system.commands.*;
import system.core.User;

public class CustomerInterface extends UserInterface {

	public CustomerInterface(){}
	
	public CustomerInterface(User CurrentUser)
	{
		super(CurrentUser);
		
		this.addCommand(new NewPurchase("New Purchase", this));
		this.addCommand(new CancelPurchase("Cancel Purchase", this));
		this.addCommand(new CompletePurchase("Complete Purchase", this));
		this.addCommand(new InsertProductInCart("Insert Product In Cart", this));
		this.addCommand(new RemoveProductFromCart("Remove Product From Cart", this));
		this.addCommand(new ViewCartItems("View Cart Items", this));
		this.addCommand(new SearchProduct("Search Product", this));
		this.addCommand(new Logout("Logout", this));
		this.addCommand(new CloseSystem("Close System", this));
	}
	
	
}
