package system.ui;


import java.util.ArrayList;
import java.util.List;

import system.Ecommerce;
import system.commands.Command;
import system.enumerators.CommandResponse;



public class Interface {
	private List<Command> commands;
	
	public Interface()
	{
		this.commands = new ArrayList<Command>();
	}
	
	public void addCommand(Command command)
	{
		commands.add(command);
	}
	
	public CommandResponse showUI()
	{
		CommandResponse behavior = CommandResponse.CONTINUE;
		while(behavior == CommandResponse.CONTINUE)
		{
	
			String choosenOption;
			int option = 0;
			boolean isOptionValid = false;
			
			while(!isOptionValid)
			{
				for(Command command : this.commands)
				{
					System.out.println(this.commands.indexOf(command) + " - " + command.getDescription());
				}
				
				System.out.print("Choose your option: ");
				
				try 
				{
					choosenOption = Ecommerce.IOREADER.readLine();
					option = Integer.parseInt(choosenOption);
					
					if(option >= 0 && option < this.commands.size())
					{
						isOptionValid = true;
					}
					else 
					{
						System.out.println("Invalid option.");
					}
					
				}
				catch(Exception e)
				{
					System.out.println("Invalid option.");
				}
			}
			
			behavior = this.commands.get(option).execute();
		}
		
		return behavior;		
		
	}
	

}
