package webstore.core;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Buy {

	private Integer clientCode;
	private Integer totalEletronics;
	private Integer totalVestuary;
	private Double valuebByElectronics;
	private Double valueByVestuary;
	private Date buyDate;

	public Buy(Integer clientCode, Integer totalEletronics, Integer totalVestuary, 
			Double valueByEletronics, Double valueByVestuary) {
		this.setClientCode(clientCode);
		this.setTotalEletronics(totalEletronics);
		this.setTotalVestuary(totalVestuary);
		this.setValuebByElectronics(valueByEletronics);
		this.setValueByVestuary(valueByVestuary);
		this.buyDate = new Date();
	}

	public Integer getClientCode() {
		return clientCode;
	}

	public void setClientCode(Integer clientCode) {
		this.clientCode = clientCode;
	}

	public Integer getTotalEletronics() {
		return totalEletronics;
	}

	public void setTotalEletronics(Integer totalEletronics) {
		this.totalEletronics = totalEletronics;
	}

	public Integer getTotalVestuary() {
		return totalVestuary;
	}

	public void setTotalVestuary(Integer totalVestuary) {
		this.totalVestuary = totalVestuary;
	}

	public Double getValuebByElectronics() {
		return valuebByElectronics;
	}

	public void setValuebByElectronics(Double valuebByElectronics) {
		this.valuebByElectronics = valuebByElectronics;
	}

	public Double getValueByVestuary() {
		return valueByVestuary;
	}

	public void setValueByVestuary(Double valueByVestuary) {
		this.valueByVestuary = valueByVestuary;
	}

	public String getBuyDate(){
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String result = dateFormat.format(buyDate);
		
		return result;
	}

}
