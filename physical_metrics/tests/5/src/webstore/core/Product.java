package webstore.core;

public abstract class Product {

	protected Integer code;
	protected String description;
	protected String color;
	protected Double price;
	protected Integer numberInInventory;

	protected Product(Integer code, String description, String color, Double price,
			Integer numberInInventory) {
		this.code = code;
		this.description = description;
		this.color = color;
		this.price = price;
		this.numberInInventory = numberInInventory;
	}

	/**
	 * @return the product code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * @return the product description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * @return the price
	 */
	public Double getPrice() {
		return price;
	}
	
	/**
	 * @return the number of itens in inventory
	 */
	public Integer getNumberInInventory() {
		return numberInInventory;
	}
	
	/**
	 * @param the product code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * @param the product description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @param the color
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * @param the price
	 */
	public void setPrice(Double price) {
		this.price = price;
	}
	
	/**
	 * @param the number of itens in inventory
	 */
	public void setNumberInInventory(Integer numberInInventory) {
		this.numberInInventory = numberInInventory;
	}
	
}
