package webstore.core;

public class EletronicProduct extends Product {
	
	protected String dimensions;

	public EletronicProduct(Integer code, String description, String dimensions, 
			String color, Double price, Integer numberInInventory) {
		super(code, description, color, price, numberInInventory);
		this.dimensions = dimensions;
		
	}
	
	/**
	 * @return the product dimensions
	 */
	public String getDimensions() {
		return dimensions;
	}

	/**
	 * @param the product dimensions
	 */
	public void setDimensions(String dimensions) {
		this.dimensions = dimensions;
	}
	

}