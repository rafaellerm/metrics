package webstore.core;

public abstract class User {

	protected String firstName;
	protected String lastName;
	protected String password;
	protected Integer code;

	protected User(String firstName, String lastName, Integer code, String password) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.code = code;
		this.password = password;
		
	}

	
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	public boolean isValidPassword(String password) {
		return this.password.equals(password);
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

}