package webstore.core;

public class VestuaryProduct extends Product {

	protected String size;
	protected String gender;
	
	public VestuaryProduct(Integer code, String description, String size, String gender,
			String color, Double price, Integer numberInInventory) {
		super(code, description, color, price, numberInInventory);
		this.size = size;
		this.gender = gender;
	}
	
	/**
	 * @return the product size
	 */
	public String getSize() {
		return size;
	}
	
	/**
	 * @return the product gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param the product size
	 */
	public void setSize(String size) {
		this.size = size;
	}
	
	/**
	 * @param the product gender
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}
}