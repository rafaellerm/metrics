package webstore.core;

import java.text.ParseException;

import webstore.WebStore;
import webstore.exception.BusinessException;

public class CurrentAccount {
	
	private Client client;
	private Integer number;
	private ShoppingCart cart;
		
	public CurrentAccount(Client client) {
		this.number = client.code;
		this.client = client;
		client.setAccount(this);
		this.cart = new ShoppingCart();		
	}
	
	/**
	 * @return the client
	 */
	public Client getClient() {
		return client;
	}

	/**
	 * @return the number
	 */
	public Integer getNumber() {
		return number;
	}
	
	public ShoppingCart getCart()
	{
		return cart;
	}
	
	public BusinessException insert(Integer code, Integer quantity)
	{	
		int i, exists = 0;
	
		for (i=0; i<WebStore.eletprods.size(); i++){
			if (WebStore.eletprods.get(i).code.equals(code)){
				EletronicProduct product = new EletronicProduct(WebStore.eletprods.get(i).code, WebStore.eletprods.get(i).description, 
					WebStore.eletprods.get(i).dimensions, WebStore.eletprods.get(i).color, WebStore.eletprods.get(i).price, quantity);
				
				cart.insert(null, product);
				
				exists = 1;
			}
		}
		for (i=0; i<WebStore.vestprods.size(); i++){
			if (WebStore.vestprods.get(i).code.equals(code)){
				VestuaryProduct product = new VestuaryProduct(WebStore.vestprods.get(i).code, WebStore.vestprods.get(i).description, 
					WebStore.vestprods.get(i).size, WebStore.vestprods.get(i).gender, WebStore.vestprods.get(i).color, 
					WebStore.vestprods.get(i).price, quantity);
				
				cart.insert(product, null);
				
				exists = 1;
				}
		}
		
		if (exists == 0)
		{
			return BusinessException.INEXIST_ITEM;
		}
		
		return null;
	}
	
	public BusinessException remove(Integer code, Integer quantity)
	{
		int i, exists = 0;
		BusinessException erro = null;
		
		for (i=0; i<WebStore.eletprods.size(); i++){
			if (WebStore.eletprods.get(i).code.equals(code)){
				EletronicProduct product = new EletronicProduct(WebStore.eletprods.get(i).code, WebStore.eletprods.get(i).description, 
					WebStore.eletprods.get(i).dimensions, WebStore.eletprods.get(i).color, WebStore.eletprods.get(i).price, quantity);
				
				erro = cart.remove(null, product);
				
				exists = 1;
			}
		}
		for (i=0; i<WebStore.vestprods.size(); i++){
			if (WebStore.vestprods.get(i).code.equals(code)){
				VestuaryProduct product = new VestuaryProduct(WebStore.vestprods.get(i).code, WebStore.vestprods.get(i).description, 
					WebStore.vestprods.get(i).size, WebStore.vestprods.get(i).gender, WebStore.vestprods.get(i).color, 
					WebStore.vestprods.get(i).price, quantity);
				
				erro = cart.remove(product, null);
				
				exists = 1;
				}
		}
		
		if (exists == 0)
		{
			return BusinessException.INEXIST_ITEM;
		}
		
		return erro;
	}

	public BusinessException cancelPurchase()
	{
		cart.cancelPurchase();
		return null;
	}
	
	public BusinessException newPurchase()
	{
		cart.newPurchase();
		return null;
	}
	
	public BusinessException finishPurchase() throws ParseException
	{
		return cart.FinishPurchase(this.client);
	}
}
	
