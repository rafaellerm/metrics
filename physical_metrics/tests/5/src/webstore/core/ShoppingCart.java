package webstore.core;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import webstore.WebStore;
import webstore.exception.BusinessException;

public class ShoppingCart {
	
	protected List<VestuaryProduct> vestprods;
	protected List <EletronicProduct> eletprods;
	protected Double totalValue;
	
	public ShoppingCart () {
		vestprods = new ArrayList<>();
		eletprods = new ArrayList<>();
		totalValue = 0.0;	
	}
	
	public List<VestuaryProduct> getVestprods()
	{
		return vestprods;
	}
	
	public List<EletronicProduct> getEletprods()
	{
		return eletprods;
	}
	
	public void insert (VestuaryProduct vestprod, EletronicProduct eletprod){
		
		int i=0;
		int contains=0;
		
		if (vestprod == null){
			for (i=0; i<eletprods.size(); i++){
				if (eletprods.get(i).code == eletprod.code){
					eletprods.get(i).numberInInventory+= eletprod.numberInInventory;
					totalValue += (eletprod.price * eletprod.numberInInventory);
					contains = 1;
				}
			}
			if (contains == 0){
				eletprods.add(eletprod);
				totalValue += (eletprod.price * eletprod.numberInInventory);
			}
		}
		else{
			
			for (i=0; i<vestprods.size(); i++){
				if (vestprods.get(i).code == vestprod.code){
					vestprods.get(i).numberInInventory += vestprod.numberInInventory;
					totalValue += (vestprod.price * vestprod.numberInInventory);
					contains = 1;
				}
			}
			if (contains == 0){
				vestprods.add(vestprod);
				totalValue += (vestprod.price * vestprod.numberInInventory);
			}
		}
	}
	
	public BusinessException remove (VestuaryProduct vestprod, EletronicProduct eletprod){
		int i=0;
		int contains=0;
		
		if (vestprod == null){
			for (i=0; i<eletprods.size(); i++){
				if (eletprods.get(i).code == eletprod.code){
					if(eletprod.numberInInventory > eletprods.get(i).numberInInventory)
					{
						return BusinessException.INVALID_QUANTITY;
					}
					else
					{
						eletprods.get(i).numberInInventory -= eletprod.numberInInventory;
						if (eletprods.get(i).numberInInventory == 0)
							eletprods.remove(i);
						totalValue -= (eletprod.price * eletprod.numberInInventory);
						contains = 1;
					}
				}
			}
			
			if (contains == 0){
				return BusinessException.INEXIST_IN_SHOPPING_CART;
			}
			
		}
		
		else{
			
			for (i=0; i<vestprods.size(); i++){
				if (vestprods.get(i).code == vestprod.code){
					if(vestprod.numberInInventory > vestprods.get(i).numberInInventory)
					{
						return BusinessException.INVALID_QUANTITY;
					}
					else
					{
						vestprods.get(i).numberInInventory -= vestprod.numberInInventory;
						if (vestprods.get(i).numberInInventory == 0)
							vestprods.remove(i);
						totalValue -= (vestprod.price * vestprod.numberInInventory);
						contains = 1;
					}
				}
			}
			
			if (contains == 0){
				return BusinessException.INEXIST_IN_SHOPPING_CART;
			}
		}
		
		return null;
			
	}
	
	public void cancelPurchase(){
		vestprods.clear();
		eletprods.clear();
		totalValue = 0.0;
	}
	
	public void newPurchase()
	{
		vestprods.clear();
		eletprods.clear();
		totalValue = 0.0;
	}
	
	public BusinessException FinishPurchase(Client client) throws ParseException{
		int i=0;
		Integer totalEletronics=0;
		Integer totalVestuary=0;
		Double valueByEletronics=0.0;
		Double valueByVestuary=0.0;
		
		for (i=0; i<eletprods.size(); i++){
			if (eletprods.get(i).code.equals(WebStore.eletprods.get(i).code)){
				if (eletprods.get(i).numberInInventory > WebStore.eletprods.get(i).numberInInventory)
					return BusinessException.INVALID_QUANTITY;
			
				else{
					totalEletronics+=eletprods.get(i).numberInInventory;
					valueByEletronics+=(eletprods.get(i).price *
							eletprods.get(i).numberInInventory);
					WebStore.eletprods.get(i).numberInInventory -= eletprods.get(i).numberInInventory;
					
				}
			}
		}
		
		for (i=0; i<vestprods.size(); i++){
			if (vestprods.get(i).code.equals(WebStore.vestprods.get(i).code)){
				if (vestprods.get(i).numberInInventory > WebStore.vestprods.get(i).numberInInventory)
					return BusinessException.INVALID_QUANTITY;
				
					else{
						totalVestuary+=vestprods.get(i).numberInInventory;
						valueByVestuary+=(vestprods.get(i).price *
								vestprods.get(i).numberInInventory);
						WebStore.vestprods.get(i).numberInInventory -= vestprods.get(i).numberInInventory;
					}
			}
		}
		
		Buy purchase = new Buy(client.code, totalEletronics, totalVestuary, 
				valueByEletronics, valueByVestuary);
		
		try {
		FileWriter arq;
		arq = new FileWriter("files/log.txt", true);
		PrintWriter writeArq = new PrintWriter(arq); 
				
		writeArq.println(purchase.getClientCode() + "," +
				purchase.getTotalEletronics() + "," +
				purchase.getTotalVestuary() + "," +
				purchase.getValuebByElectronics() + "," +
				purchase.getValueByVestuary() + "," +
				purchase.getBuyDate());
		
		arq.close();
		} 
		
		catch (IOException e) {
			e.printStackTrace();
		} 
		
		vestprods.clear();
		eletprods.clear();
		totalValue = 0.0;
		
		return null;
	}
}