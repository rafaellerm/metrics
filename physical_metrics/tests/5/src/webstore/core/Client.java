package webstore.core;

import java.util.Date;

public class Client extends User {

	private CurrentAccount account;
	private int cpf;
	private Date birthday;
	private String adress;

	public Client(String firstName, String lastName, String adress, 
			int cpf,  Date birthday, Integer code, String password ) {
		super(firstName, lastName, code, password);
		this.cpf = cpf;
		this.birthday = birthday;
		this.adress = adress;
		this.account = null;
	}

	/**
	 * @return the birthday
	 */
	public Date getBirthday() {
		return birthday;
	}
	
	/**
	 * @return the account
	 */
	public CurrentAccount getAccount() {
		return account;
	}

	/**
	 * @return the cpf
	 */
	public int getCpf() {
		return cpf;
	}
	
	/**
	 * @return the adress
	 */
	public String getAdress() {
		return adress;
	}

	/**
	 * @param account
	 *            the account to set
	 */
	public void setAccount(CurrentAccount account) {
		this.account = account;
	}
	
	/**
	 * @param birthday
	 *            the birthday to set
	 */
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	
	/**
	 * @param cpf
	 *            the cpf to set
	 */
	public void setCpf(int cpf) {
		this.cpf = cpf;
	}
	
	/**
	 * @param adress
	 *            the adress to set
	 */
	public void setAdress(String adress) {
		this.adress = adress;
	}

}