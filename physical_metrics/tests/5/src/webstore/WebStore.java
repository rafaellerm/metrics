package webstore;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import webstore.core.EletronicProduct;
import webstore.core.Manager;
import webstore.core.Client;
import webstore.core.CurrentAccount;
import webstore.core.VestuaryProduct;
import webstore.ui.StoreInterface;
import webstore.ui.ManagerInterface;
import webstore.ui.ClientInterface;


public class WebStore {
	
	public static final Map<Integer, CurrentAccount> ACCOUNTS;
	public static final Map<Integer, Manager> MANAGERS;
	public static final List<VestuaryProduct> vestprods;
	public static final List <EletronicProduct> eletprods;
	

	static {
		
		eletprods = new ArrayList<>();
		vestprods = new ArrayList<>();
		MANAGERS = new HashMap<>();
		ACCOUNTS = new HashMap<>();
		
		
		String nomeArq = "files/eletro.txt";
		String line = "";
		
		try{
	        FileReader reader = new FileReader(nomeArq);
	        BufferedReader linereader = new BufferedReader(reader);
	        
	        while(linereader.ready()){
	          line=linereader.readLine();
	          String[] array = line.split(",");
	          String code = array[0];  
	          String description = array[1];
	          String dimensions = array[2] + "x" + array[3] + "x" + array[4];
	          String price = array[5];
	          String numberInInventory = array[6];
	          String color = array[7];
	          
	          EletronicProduct eletprod = new EletronicProduct (Integer.parseInt(code), 
	        		  description, dimensions, color, Double.parseDouble(price),
	        		  Integer.parseInt(numberInInventory));

	          eletprods.add(eletprod);
	        }
	      linereader.close();
	      reader.close();

	      }
	      catch(Exception erro) {
	    	  erro.printStackTrace();
	      }
		
		nomeArq = "files/vestuario.txt";
		line = "";
		
		try{
	        FileReader reader = new FileReader(nomeArq);
	        BufferedReader linereader = new BufferedReader(reader);
	        
	        while(linereader.ready()){
	          line=linereader.readLine();
	          String[] array = line.split(",");
	          String code = array[0];  
	          String description = array[1];
	          String size = array[2];
	          String price = array[3];
	          String numberInInventory = array[4];
	          String color = array[5];
	          String gender = array[6];
	          
	          VestuaryProduct vestprod = new VestuaryProduct (Integer.parseInt(code), 
	        		  description, size,gender,color, Double.parseDouble(price),
	        		  Integer.parseInt(numberInInventory));

	          vestprods.add(vestprod);
	        }
	      linereader.close();
	      reader.close();

	      }
	      catch(Exception erro) {
	    	  erro.printStackTrace();
	      }
		
		nomeArq = "files/usuarios.txt";
		line = "";
		
		try{
	        FileReader reader = new FileReader(nomeArq);
	        BufferedReader leitor = new BufferedReader(reader);
	        
	        while(leitor.ready()){
	          line=leitor.readLine();
	          String[] array = line.split(",");
	          String[] arrayaux = array[1].split(" ");
	          String firstName = arrayaux[0];
	          String lastName = arrayaux[1];
	          String code = array[0];  
	          String post = array[2]; 
	          if (post.equals(" gerente") || post.equals("gerente"))
	      		MANAGERS.put(Integer.parseInt(code), new Manager(firstName,
	      				lastName, Integer.parseInt(code), "456" ));
	          
	      		
	      		else{	
	      			CurrentAccount currentAccount = new CurrentAccount (new Client(
	      					firstName, lastName, "", 0, new Date(),
	      					Integer.parseInt(code), "123"));
	      			ACCOUNTS.put(currentAccount.getNumber(), currentAccount);
	      		}	

	        }
	      leitor.close();

	      }
	      catch(Exception erro) {
	    	  erro.printStackTrace();
	      }

	}
	
	public static void main(String[] args) throws IOException
	{
		WebStore webStore = new WebStore();
		webStore.createAndShowGUI();
	}
	
	private ClientInterface client;
	private ManagerInterface manager;
	private StoreInterface currentInterface;
	
	protected final BufferedReader reader;
	
	public WebStore() 
	{
		this.client = new ClientInterface();
		this.manager = new ManagerInterface();
		this.currentInterface = null;
		
		this.reader = new BufferedReader(new InputStreamReader(System.in));
	}
	
	public void createAndShowGUI() throws IOException 
	{
		int is_C_or_M = 0;
		
		System.out.print(getMenu());
		Integer option = new Integer(reader.readLine());
		
		while (option != 0) {
			switch (option) {
			case 1:
				this.currentInterface = client;
				is_C_or_M = 1;
				break;
			case 2:
				this.currentInterface = manager;
				is_C_or_M = 1;
				break;				
			}
			if (is_C_or_M == 1)
				this.currentInterface.createAndShowGUI();
			is_C_or_M = 0;
			System.out.print(getMenu());
			option = new Integer(reader.readLine());
		}
	}
	
	private String getMenu() 
	{
		StringBuffer sb = new StringBuffer();
		sb.append("Welcome to Schupz WebStore!\n");
		sb.append("Choose your account (or 0 to exit):\n");
		sb.append("1 - Client\n");
		sb.append("2 - Manager\n");
		sb.append("0 - Exit\n");
		sb.append("Chosen option: ");
		return sb.toString();
	}
}
