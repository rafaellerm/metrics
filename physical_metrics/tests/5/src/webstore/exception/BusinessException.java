package webstore.exception;

public enum BusinessException {

	INEXIST_ITEM, INEXIST_IN_SHOPPING_CART, INVALID_QUANTITY, INVALID_DATE;

}