package webstore.ui;

import java.io.IOException;

import webstore.core.CurrentAccount;
import webstore.exception.BusinessException;

public class NewPurchaseCommand extends Command {
	
	public NewPurchaseCommand() {
		setEnabled(false);
	}
	
	public void execute(StoreInterface storeInterface) throws IOException 
	{
		CurrentAccount currentAccount = storeInterface.getCurrentAccount();
		
		if (currentAccount == null)
			return;
		
		BusinessException exception = currentAccount.newPurchase();
		
		if (exception == null) {
			System.out.println("Operation status: successfull");
		} else {
			System.out.println(exception);
		}
	}
}