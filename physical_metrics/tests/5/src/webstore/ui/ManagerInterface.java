package webstore.ui;

import webstore.core.CurrentAccount;
import webstore.core.Manager;
import webstore.ui.StoreInterface;

public class ManagerInterface extends StoreInterface {
	
	private Manager manager;

	
	public ManagerInterface() {
		this.commands.put("L", new ManagerLogin());
		this.commands.put("S", new SearchCommand());
		this.commands.put("R", new ReportCommand());
		this.commands.put("O", new LogoutCommand());		
	}
		
	public boolean isLoggedIn() {
		return manager != null;
	}
	
	public void login(Manager manager) {
		this.manager = manager;
		if (isLoggedIn()) {
			toogleCommands();
		}
	}
	
	public void logout() {
		this.manager = null;
		toogleCommands();
	}
	
	public Manager getManager() {
		return manager;
	}
	
	public CurrentAccount getCurrentAccount()
	{
		return null;
	}

}
