package webstore.ui;

import java.io.IOException;

public abstract class Command {
	
	private boolean isEnabled;
	
	public abstract void execute(StoreInterface storeInterface)
			throws IOException;
	
	public boolean isEnabled() {
		return isEnabled;
	}
	
	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}
}


