package webstore.ui;

import java.io.IOException;

import webstore.WebStore;

public class SearchCommand extends Command {
	
	public SearchCommand() {
		setEnabled(false);
	}
	
	public void execute(StoreInterface storeInterface) throws IOException
	{	
		int i, exists = 0;
		System.out.print(getMenu());
		Integer option = new Integer(storeInterface.reader.readLine());
		switch(option)
		{
			case 1:
				System.out.println("What is your product description? ");
				String searchString1 = storeInterface.reader.readLine();
				
				System.out.print("Related Produts:\n");
				for (i=0; i<WebStore.eletprods.size(); i++){
					if (WebStore.eletprods.get(i).getDescription().equals(searchString1))
					{
						System.out.print("Code: " + WebStore.eletprods.get(i).getCode() + " Description: " + WebStore.eletprods.get(i).getDescription() + 
							" Quantity: " + WebStore.eletprods.get(i).getNumberInInventory() + " Price: " + WebStore.eletprods.get(i).getPrice() + "\n");	
						
						exists = 1;
					}
				}
				for (i=0; i<WebStore.vestprods.size(); i++){
					if (WebStore.vestprods.get(i).getDescription().equals(searchString1))
					{
						System.out.print("Code: " + WebStore.vestprods.get(i).getCode() + " Description: " + WebStore.vestprods.get(i).getDescription() + 
								" Quantity: " + WebStore.vestprods.get(i).getNumberInInventory() + " Price: " + WebStore.vestprods.get(i).getPrice() + "\n");
						
						exists = 1;
					}
				}
				if (exists == 0)
				{
					System.out.print("There is no related product\n");
				}
				break;
			case 2:
				System.out.println("What is your product color? ");
				String searchString2 = new String(storeInterface.reader.readLine());
				
				System.out.print("Related Produts:\n");
				for (i=0; i<WebStore.eletprods.size(); i++){
					if (WebStore.eletprods.get(i).getColor().equals(searchString2))
					{
						System.out.print("Code: " + WebStore.eletprods.get(i).getCode() + " Description: " + WebStore.eletprods.get(i).getDescription() + 
								" Quantity: " + WebStore.eletprods.get(i).getNumberInInventory() + " Price: " + WebStore.eletprods.get(i).getPrice() + "\n");	
						
						exists = 1;
					}
				}
				for (i=0; i<WebStore.vestprods.size(); i++){
					if (WebStore.vestprods.get(i).getColor().equals(searchString2))
					{
						System.out.print("Code: " + WebStore.vestprods.get(i).getCode() + " Description: " + WebStore.vestprods.get(i).getDescription() + 
								" Quantity: " + WebStore.vestprods.get(i).getNumberInInventory() + " Price: " + WebStore.vestprods.get(i).getPrice() + "\n");
						
						exists = 1;
					}
				}
				break;
				
			case 3:
				System.out.println("What is your product code? ");
				Double searchDouble = new Double(storeInterface.reader.readLine());
			
				System.out.print("Related Produts:\n");
				for (i=0; i<WebStore.eletprods.size(); i++){
					if (WebStore.eletprods.get(i).getPrice().equals(searchDouble))
					{
						System.out.print("Code: " + WebStore.eletprods.get(i).getCode() + " Description: " + WebStore.eletprods.get(i).getDescription() + 
								" Quantity: " + WebStore.eletprods.get(i).getNumberInInventory() + " Price: " + WebStore.eletprods.get(i).getPrice() + "\n");
						
						exists = 1;
					}
				}
				for (i=0; i<WebStore.vestprods.size(); i++){
					if (WebStore.vestprods.get(i).getPrice().equals(searchDouble))
					{
						System.out.print("Code: " + WebStore.vestprods.get(i).getCode() + " Description: " + WebStore.vestprods.get(i).getDescription() + 
								" Quantity: " + WebStore.vestprods.get(i).getNumberInInventory() + " Price: " + WebStore.vestprods.get(i).getPrice() + "\n");
						
						exists = 1;
					}
				}
		}
	}
	
	private String getMenu() 
	{
		StringBuffer sb = new StringBuffer();
		sb.append("Choose how you wanna search:\n");
		sb.append("1 - Description\n");
		sb.append("2 - Color\n");
		sb.append("3 - Price\n");
		sb.append("Chosen option: ");
		return sb.toString();
	}

}
