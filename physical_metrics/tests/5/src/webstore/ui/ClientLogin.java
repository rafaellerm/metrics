package webstore.ui;

import java.io.IOException;

import webstore.WebStore;
import webstore.core.CurrentAccount;

public class ClientLogin extends Command {
	
	public ClientLogin() {
		setEnabled(true);
	}
	
	public void execute(StoreInterface storeInterface) throws IOException 
	{
		System.out.print("Code: ");
		String code = storeInterface.reader.readLine();
		System.out.print("Password: ");
		String password = storeInterface.reader.readLine();
		
		CurrentAccount currentAccount = WebStore.ACCOUNTS.get(Integer.parseInt(code));
		
		if (currentAccount != null) {
			boolean validPassword = currentAccount.getClient().isValidPassword(password);
			if (validPassword) {
				((ClientInterface) storeInterface).login(currentAccount);
				return;
			}
		}
		System.out.println("Invalid Account!");
	}
}

