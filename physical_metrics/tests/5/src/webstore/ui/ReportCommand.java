package webstore.ui;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import webstore.WebStore;
import webstore.core.Manager;

public class ReportCommand extends Command{
	
	public ReportCommand() 
	{
		setEnabled(false);
	}
	
	public void execute(StoreInterface storeInterface) throws IOException 
	{
		Manager manager = storeInterface.getManager();
		
		if (manager == null)
			return;
		
		System.out.print(getMenu());
		Integer option = new Integer(storeInterface.reader.readLine());
		
		switch (option) {
			case 1:
				int i;
				System.out.println("\nEletronic Products:");
				for (i=0; i<WebStore.eletprods.size(); i++){
					System.out.print("Code:" + WebStore.eletprods.get(i).getCode() + 
							"\t  Quantity:"+WebStore.eletprods.get(i).getNumberInInventory() +
							"\t  Description:" + WebStore.eletprods.get(i).getDescription() + "\n");
				}
				System.out.println();
				System.out.println("\nVestuary Products:");
				for (i=0; i<WebStore.vestprods.size(); i++){
					System.out.print("Code:" + WebStore.vestprods.get(i).getCode() +
							"\t  Quantity:"+WebStore.vestprods.get(i).getNumberInInventory() +
							"\t Description:" + WebStore.vestprods.get(i).getDescription() + "\n");
				}
				break;
			case 2:
				System.out.println("\nClients:");
				
				for (Integer key : WebStore.ACCOUNTS.keySet())
				{
					System.out.print("Code: " + key + "\t  Name: " 
							+ WebStore.ACCOUNTS.get(key).getClient().getFirstName() + " " 
							+ WebStore.ACCOUNTS.get(key).getClient().getLastName() +"\n");
				}
				break;
			case 3:				
				System.out.println("Date (dd/mm/yyyy): ");
				String date = storeInterface.reader.readLine();
				
				String nomeArq = "files/log.txt";
				String line = "";
				Double saleTotal = 0.0;
					
				try{
			        FileReader reader = new FileReader(nomeArq);
			        BufferedReader linereader = new BufferedReader(reader);
			        
			        while(linereader.ready()){
			          line=linereader.readLine();
			          String[] array = line.split(",");
			          String fileday = array[5];
			          date.toString();
			          if (fileday.equals(date)){
			        	  saleTotal += Double.parseDouble(array[3]) + Double.parseDouble(array[4]);
			          }
			        }
			        linereader.close();
				}
			      catch(Exception erro) {
			    	  erro.printStackTrace();
			      }
				
				System.out.println("Total Sales: ");
				System.out.println(saleTotal);
				
				break;
		}
	}
	
	private String getMenu() 
	{
		StringBuffer sb = new StringBuffer();
		sb.append("Choose what type of report:\n");
		sb.append("1 - Stock\n");
		sb.append("2 - Client List\n");
		sb.append("3 - Total Sales\n");
		sb.append("Chosen option: ");
		return sb.toString();
	}

}
