package webstore.ui;

import java.io.IOException;

import webstore.core.CurrentAccount;

public class ViewCommand extends Command {
	
	public ViewCommand() {
		setEnabled(false);
	}
	
	public void execute(StoreInterface storeInterface) throws IOException 
	{
		CurrentAccount currentAccount = storeInterface.getCurrentAccount();
		
		int i;
		
		System.out.print("\nVestuary Products:\n");
		for (i=0; i<currentAccount.getCart().getVestprods().size(); i++)
		{
			System.out.print("Code: " + currentAccount.getCart().getVestprods().get(i).getCode() + " Description: " + 
				currentAccount.getCart().getVestprods().get(i).getDescription() + " Quantity: "
				+ currentAccount.getCart().getVestprods().get(i).getNumberInInventory() + " Price: " + 
				currentAccount.getCart().getVestprods().get(i).getPrice() + "\n");	
		}
		
		System.out.print("\nEletronic Products:\n");
		for (i=0; i<currentAccount.getCart().getEletprods().size(); i++)
		{
			System.out.print("Code: " + currentAccount.getCart().getEletprods().get(i).getCode() + " Description: " 
				+ currentAccount.getCart().getEletprods().get(i).getDescription() + " Quantity: " 
				+ currentAccount.getCart().getEletprods().get(i).getNumberInInventory() + " Price: " 
				+ currentAccount.getCart().getEletprods().get(i).getPrice() + "\n");	
		}
		System.out.print("\n");
	}
}