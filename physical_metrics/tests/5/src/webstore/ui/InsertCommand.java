package webstore.ui;

import java.io.IOException;

import webstore.core.CurrentAccount;
import webstore.exception.BusinessException;


public class InsertCommand extends Command {
	
	public InsertCommand() 
	{
		setEnabled(false);
	}
	
	public void execute(StoreInterface storeInterface) throws IOException 
	{
		CurrentAccount currentAccount = storeInterface.getCurrentAccount();
		
		if (currentAccount == null)
			return;
		
		System.out.print("Product code: ");
		Integer code = new Integer(storeInterface.reader.readLine());
		
		System.out.print("Quantity: ");
		Integer quantity = new Integer(storeInterface.reader.readLine());
		
		BusinessException exception = currentAccount.insert(code, quantity);
		
		if (exception == null) {
			System.out.println("Operation status: successfull");
		} else {
			System.out.println(exception);
		}
	}
	
}

