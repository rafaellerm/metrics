package webstore.ui;

import java.io.IOException;


public class LogoutCommand extends Command {
	
	public LogoutCommand() {
		setEnabled(false);
	}

	@Override
	public void execute(StoreInterface storeInterface) throws IOException {
		storeInterface.logout();
	}

}
