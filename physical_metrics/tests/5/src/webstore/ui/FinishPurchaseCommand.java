package webstore.ui;

import java.io.IOException;
import java.text.ParseException;

import webstore.core.CurrentAccount;
import webstore.exception.BusinessException;

public class FinishPurchaseCommand extends Command 
{
	
		public FinishPurchaseCommand() 
		{
			setEnabled(false);
		}
		
		public void execute(StoreInterface storeInterface) throws IOException 
		{
			CurrentAccount currentAccount = storeInterface.getCurrentAccount();
			
			if (currentAccount == null)
				return;
			
			BusinessException exception = BusinessException.INVALID_DATE;
			try {
				exception = currentAccount.finishPurchase();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			if (exception == null) {
				System.out.println("Operation status: successfull");
			} else {
				System.out.println(exception);
			}
		
		}
		
}

