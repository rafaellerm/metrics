package webstore.ui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import webstore.core.Manager;
import webstore.core.CurrentAccount;

public abstract class StoreInterface {
	
	protected Map<String, Command> commands;
	protected final BufferedReader reader;
	
	protected StoreInterface() {
		this.commands = new HashMap<>();
		this.reader = new BufferedReader(new InputStreamReader(System.in));

	}
	
	public void createAndShowGUI() throws IOException
	{
		Command command = null;
		do {
			System.out.print(getMenu());
			String commandKey = reader.readLine();
			command = commands.get(commandKey);
			if (command != null) {
				command.execute(this);
			}
		} while (command != null);
		if (isLoggedIn()) {
			logout();
		}	
	}
	
	protected String getMenu() {
		StringBuffer sb = new StringBuffer();
		sb.append("Choose your action (or E to exit):\n");
		for (String key : commands.keySet()) {
			Command command = commands.get(key);
			if (command.isEnabled()) {
				sb.append(key).append(" - ")
						.append(command.getClass().getSimpleName())
						.append("\n");
			}
		}
		sb.append("Chosen option: ");

		return sb.toString();
	}
	
	public BufferedReader getReader() {
		return reader;
	}
	
	public abstract CurrentAccount getCurrentAccount();
	
	public abstract Manager getManager();
	
	public abstract boolean isLoggedIn();

	public abstract void logout();
	
	protected void toogleCommands() {
		for (String key : commands.keySet()) {
			Command command = commands.get(key);
			command.setEnabled(!command.isEnabled());
		}
	}

}
