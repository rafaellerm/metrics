package webstore.ui;

import webstore.core.CurrentAccount;
import webstore.core.Manager;
import webstore.ui.StoreInterface;

public class ClientInterface extends StoreInterface {
	
	private CurrentAccount currentAccount;

	
	public ClientInterface() {
		this.currentAccount = null;
		this.commands.put("L", new ClientLogin());
		this.commands.put("S", new SearchCommand());
		this.commands.put("I", new InsertCommand());
		this.commands.put("R", new RemoveCommand());
		this.commands.put("V", new ViewCommand());
		this.commands.put("N", new NewPurchaseCommand());
		this.commands.put("O", new LogoutCommand());
		this.commands.put("F", new FinishPurchaseCommand());
		this.commands.put("C", new CancelPurchaseCommand());
	}
	
	public boolean isLoggedIn() {
		return currentAccount != null;
	}
	
	public void login(CurrentAccount currentAccount) {
		this.currentAccount = currentAccount;
		if (isLoggedIn()) {
			toogleCommands();
		}
	}
	
	public void logout() {
		this.currentAccount = null;
		toogleCommands();
	}
	
	public CurrentAccount getCurrentAccount() {
		return currentAccount;
	}

	public Manager getManager()
	{
		return null;
	}
	
}
