package webstore.ui;

import java.io.IOException;

import webstore.core.Manager;
import webstore.WebStore;


public class ManagerLogin extends Command {
	
	public ManagerLogin() 
	{
		setEnabled(true);
	}
	
	public void execute(StoreInterface storeInterface) throws IOException 
	{
		System.out.print("Code: ");
		String code = storeInterface.reader.readLine();
		System.out.print("Password: ");
		String password = storeInterface.reader.readLine();
		
		Manager manager = WebStore.MANAGERS.get(Integer.parseInt(code));
		
		if (manager != null) {
			boolean validPassword = manager.isValidPassword(password);
			if (validPassword) {
				((ManagerInterface) storeInterface).login(manager);
				return;
			}
		}
		System.out.println("Invalid Account!");
	}
	
}

