package tests;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.util.Date;

import org.junit.Test;

import webstore.WebStore;
import webstore.core.Client;
import webstore.core.CurrentAccount;
import webstore.core.EletronicProduct;
import webstore.core.ShoppingCart;


public class FunctionalTest {

	@Test
	public void testInsert() {
		Date date = new Date();
		Client client = new Client ("João", "Silva", "Rua 1", 1, date, 1, "123");
		CurrentAccount currentAccount = new CurrentAccount(client);
		
		assertNull(currentAccount.insert(1, 1));
	}
	
	@Test
	public void testCancel(){
		Date date = new Date();
		Client client = new Client ("João", "Silva", "Rua 1", 1, date, 1, "123");
		CurrentAccount currentAccount = new CurrentAccount(client);
		
		assertNull(currentAccount.cancelPurchase());
		
	}
	
	@Test
	public void testNew(){
		Date date = new Date();
		Client client = new Client ("Jo�o", "Silva", "Rua 1", 1, date, 1, "123");
		CurrentAccount currentAccount = new CurrentAccount(client);
		
		assertNull(currentAccount.newPurchase());
		
	}
	
	@Test
	public void testFinish(){
		Date date = new Date();
		Client client = new Client ("Jo�o", "Silva", "Rua 1", 1, date, 1, "123");
		CurrentAccount currentAccount = new CurrentAccount(client);
		
		ShoppingCart sc = new ShoppingCart();
		EletronicProduct eletprod = new EletronicProduct(1, "geladeira",
				"10x80x50", "branca", 1200.0, 5);
		WebStore.eletprods.add(eletprod);
		eletprod.setNumberInInventory(2);
		
		sc.insert(null, eletprod);
		try {
			assertNull(currentAccount.finishPurchase());
		} catch (ParseException e) {
			e.printStackTrace();
		}
	
	}
	
	@Test
	public void testFinishError(){
		Date date = new Date();
		Client client = new Client ("Jo�o", "Silva", "Rua 1", 1, date, 1, "123");
		
		ShoppingCart sc = new ShoppingCart();
		EletronicProduct eletprod = new EletronicProduct(1, "geladeira",
				"10x80x50", "branca", 1200.0, 2);
		WebStore.eletprods.add(eletprod);
		eletprod.setNumberInInventory(5);
		
		sc.insert(null, eletprod);
		try {
			assertNotNull(sc.FinishPurchase(client));
		} catch (ParseException e) {
			e.printStackTrace();
		}
	
	}
	
	@Test
	public void testRemove(){
		Date date = new Date();
		Client client = new Client ("Jo�o", "Silva", "Rua 1", 1, date, 1, "123");
		CurrentAccount currentAccount = new CurrentAccount(client);
		
		EletronicProduct eletprod = new EletronicProduct(1, "geladeira",
				"10x80x50", "branca", 1200.0, 2);
		WebStore.eletprods.add(eletprod);
		
		currentAccount.insert(1, 1);
		
		assertNull(currentAccount.remove(1, 1));

	
	}
	
	@Test
	public void testRemoveError(){
		Date date = new Date();
		Client client = new Client ("Jo�o", "Silva", "Rua 1", 1, date, 1, "123");
		CurrentAccount currentAccount = new CurrentAccount(client);
		
		EletronicProduct eletprod = new EletronicProduct(1, "geladeira",
				"10x80x50", "branca", 1200.0, 2);
		WebStore.eletprods.add(eletprod);
		
		assertNotNull(currentAccount.remove(1, 1));

	
	}
	

}
