package store.dbi;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import store.Store;
import store.core.Client;
import store.core.Clothing;
import store.core.Eletronic;
import store.core.Manager;
import store.core.Product;
import store.core.ProductComparator;
import store.core.Purchase;
import store.core.StoreLog;

public class DBInterface {
private String[] filenames=new String[4];
//the convention is:
//[0] eletronics
//[1] clothing
//[2] clients
//[3] sales
//the default constructor sets them respectively as:
//eletronics.txt
//clothing.txt
//clients.txt
//sales.txt
//but that can be specified by passing the correct names
//as parameters to the constructor

public DBInterface(){
	this.filenames[0]="eletronics.txt";
	this.filenames[1]="clothing.txt";
	this.filenames[2]="clients.txt";
	this.filenames[3]="sales.txt";
}

public DBInterface(String eletronicsFilename,String clothingFilename,String clientsFilename,String salesFilename){
	this.filenames[0]=eletronicsFilename;
	this.filenames[1]=clothingFilename;
	this.filenames[2]=clientsFilename;
	this.filenames[3]=salesFilename;
}


public List<Product> loadStockFromTxt(){
	List<Product> listOfProducts = new ArrayList<Product>();
	Clothing clothingBuffer=new Clothing();
	Eletronic eletronicBuffer=new Eletronic();
	 File arquivo = new File(filenames[0]);     
     try {           
          if (arquivo.exists()) {
           FileReader fileReader = new FileReader(arquivo);       
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        while (bufferedReader.ready()) {
        String line = bufferedReader.readLine();
        String[] separatedLine;
        separatedLine=line.split(",");
        eletronicBuffer.setCode(Integer.parseInt(separatedLine[0]));
        eletronicBuffer.setColor(separatedLine[7]);
        eletronicBuffer.setDescrpiton(separatedLine[1]);
        eletronicBuffer.setPrice(Float.parseFloat(separatedLine[5]));
        eletronicBuffer.setDimensions(Float.parseFloat(separatedLine[2]), Float.parseFloat(separatedLine[3]), Float.parseFloat(separatedLine[4]));
        eletronicBuffer.setNumberOfProducts(Integer.parseInt(separatedLine[6]));
        
        listOfProducts.add(eletronicBuffer);
        eletronicBuffer=new Eletronic();
        }
        bufferedReader.close();
        fileReader.close();
        }
          else
          {
        	  IOException ioException = new IOException();
        	  throw ioException;
          }
     } catch (IOException ex) {
    	 listOfProducts.add(eletronicBuffer);
    	 System.out.println("Error opening "+filenames[0]+". Generated a default list to prevent damage to other modules.");
    }
     arquivo = new File(filenames[1]);     
     try {    
          if (arquivo.exists()) {
           FileReader fileReader = new FileReader(arquivo);       
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        while (bufferedReader.ready()) {
        String line = bufferedReader.readLine();
        String[] separatedLine;
        separatedLine=line.split(",");
        clothingBuffer.setCode(Integer.parseInt(separatedLine[0]));
        clothingBuffer.setColor(separatedLine[5]);
        clothingBuffer.setDescrpiton(separatedLine[1]);
        clothingBuffer.setSize(separatedLine[2]);
        clothingBuffer.setPrice(Float.parseFloat(separatedLine[3]));
        clothingBuffer.setNumberOfProducts(Integer.parseInt(separatedLine[4]));
        clothingBuffer.setGender(separatedLine[6]);
        listOfProducts.add(clothingBuffer);
        clothingBuffer=new Clothing();
        }
        bufferedReader.close();
        fileReader.close();
        }
          else
          {
        	  IOException ioException = new IOException();
        	  throw ioException;
          }
     } catch (IOException ex) {
    	 listOfProducts.add(clothingBuffer);
    	 System.out.println("Error opening "+filenames[1]+". Generated a default list to prevent damage to other modules.");
    }
	return listOfProducts;
}

public Map<String,Client> loadClientsFromTxt(){
	Map<String,Client> mapOfClients = new HashMap<String,Client>();
	Client clientBuffer=new Client();
	Manager managerBuffer=new Manager();
	 File arquivo = new File(filenames[2]);     
     try {           
          if (arquivo.exists()) {
           FileReader fileReader = new FileReader(arquivo);       
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        while (bufferedReader.ready()) {
        String line = bufferedReader.readLine();
        String[] separatedLine=new String[4];
        separatedLine=line.split(",");
        if(separatedLine[separatedLine.length-1].contains("client")){
        clientBuffer.setAccessCode(Integer.parseInt(separatedLine[0]));
        clientBuffer.setFirstName(separatedLine[1]);
        clientBuffer.setLastName(separatedLine[2]);
        mapOfClients.put(Integer.toString(clientBuffer.getAccessCode()),clientBuffer);
        clientBuffer=new Client();
        //clientBuffer had to be 
        //initalized after inserting
        //because otherwise the same
        //pointer would be used,
        //resulting in the same client being inserted
        //many times
        }
        else
        {
          	managerBuffer.setAccessCode(Integer.parseInt(separatedLine[0]));
          	managerBuffer.setFirstName(separatedLine[1]);  
            managerBuffer.setLastName(separatedLine[2]);
            Store.setManager(managerBuffer);
        }
        }
        bufferedReader.close();
        fileReader.close();
        } 
          else{
        	  System.out.println("File doesn't exist.");
          }
     } catch (IOException ex) {   
    	 System.out.println("Error opening clients.txt. Generated a default list to prevent damage to other modules."); 
    	 Store.setManager(managerBuffer);
    	 mapOfClients.put(clientBuffer.getAccessCode().toString(),clientBuffer);
    	 return mapOfClients;
    }
	return mapOfClients;	
}

@SuppressWarnings("deprecation")
public float loadSalesFromTxt(Date date) throws UnsupportedEncodingException, FileNotFoundException{
	 File arquivo = new File(filenames[3]);     
     try {           
          if (arquivo.exists()) {
           FileReader fileReader = new FileReader(arquivo);       
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        while (bufferedReader.ready()) {
        String line = bufferedReader.readLine();
        String[] separatedLine;
        separatedLine=line.split("-");
        String[] separatedDate=separatedLine[0].split("/");
        Date readDate=new Date();
        //the date type is troublesome
        //for setting years, months and dates
        //directly. if this ever needs to be changed
        //read the Date documentation first.
        readDate.setYear(Integer.parseInt(separatedDate[2])-1900);
        readDate.setMonth(Integer.parseInt(separatedDate[1])-1);
        readDate.setDate(Integer.parseInt(separatedDate[0]));
        if((readDate.getYear()==date.getYear())&&(readDate.getMonth()==date.getMonth())&&(readDate.getDate()==date.getDate())){
        	return Float.parseFloat(separatedLine[1]);
        }
        }
        bufferedReader.close();
        fileReader.close();
        }  
     } catch (IOException ex) {    
    	 System.out.println("Error opening sales.txt.");
    }
	return -1;
}

public void saveSales() throws IOException{
	try {
		boolean hasFoundTodayInfo=false;
		File file = new File(filenames[3]);
		Date date = new Date();
		if(file.exists()){
        FileReader fileReader = new FileReader(filenames[3]);
        BufferedReader salesReader = new BufferedReader(fileReader);
        String currentSales;
        StringBuilder salesContent = new StringBuilder();
        while ((currentSales = salesReader.readLine()) != null) {
            String tokens[] = currentSales.split("-");
            String readDate[]=tokens[0].split("/"); 
            if ((Integer.parseInt(readDate[2])==(date.getYear() + 1900))&&
            		(Integer.parseInt(readDate[1])==(date.getMonth() + 1))&&
            		(Integer.parseInt(readDate[0])==date.getDate())) {
            	 hasFoundTodayInfo=true;
                 tokens[1] = String.valueOf(StoreLog.getSalesFromToday());
                 String newLine = readDate[0]+"/"+readDate[1]+"/"+readDate[2]+"-"+tokens[1];
                 salesContent.append(newLine);
                 salesContent.append(System.getProperty("line.separator"));
               
                }
             else {
                salesContent.append(currentSales);
                salesContent.append(System.getProperty("line.separator"));
            }
        }
        if(!hasFoundTodayInfo){
        	date.setMonth(date.getMonth()+1);
        	date.setYear(date.getYear()+1900);
        	System.out.println(date.getDate()+"/"+date.getMonth()+"/"+date.getYear()+"-"+StoreLog.getSalesFromToday());
        	salesContent.append(date.getDate()+"/"+date.getMonth()+"/"+date.getYear()+"-"+StoreLog.getSalesFromToday());
        	salesContent.append(System.getProperty("line.separator"));
        }
        FileWriter fstreamWrite = new FileWriter("sales.txt");
        BufferedWriter out = new BufferedWriter(fstreamWrite);
        out.write(salesContent.toString());
        out.close();
        salesReader.close();
        }
		else{
		System.out.println("File sales.txt doesn't exist.");
		file.createNewFile();
		FileWriter fileWriter=new FileWriter(file);
		date.setMonth(date.getMonth()+1);
    	date.setYear(date.getYear()+1900);
		fileWriter.write(date.getDate()+"/"+date.getMonth()+"/"+date.getYear()+"-"+StoreLog.getSalesFromToday());
		fileWriter.close();
		}
    } catch (Exception e) {
        System.err.println("Error: " + e.getMessage());
    }
}


public void saveStock(List<Product> stock) throws IOException{
	 File eletronics = new File(filenames[0]);    
	 File clothings = new File(filenames[1]);
	 List<Eletronic> listOfEletronics=new ArrayList<Eletronic>();
	 List<Clothing> listOfClothing=new ArrayList<Clothing>();
	 if (eletronics.exists())
		 eletronics.delete();
	 if (eletronics.exists())
		 clothings.delete();
	 eletronics.createNewFile();
	 clothings.createNewFile();
	 FileWriter fileWriterEletronics=new FileWriter(eletronics);
	 FileWriter fileWriterClothing=new FileWriter(clothings);
	 Collections.sort(stock,new ProductComparator("Code"));
	for(int i=0;i<stock.size();i++){
		if(stock.get(i).getClass().toString().equals("class store.core.Eletronic")){
			Eletronic currentProduct=(Eletronic) stock.get(i);
			listOfEletronics.add(currentProduct);
		}
		else
		{
			Clothing currentProduct=(Clothing) stock.get(i);
			listOfClothing.add(currentProduct);			
		}
	}
    
	for(int i=0;i<listOfEletronics.size();i++)
	{
		Eletronic currentProduct=listOfEletronics.get(i);
		fileWriterEletronics.write(currentProduct.getCode()+","+currentProduct.getDescription()+","+currentProduct.getDimensions()[0]+","+currentProduct.getDimensions()[1]+","+currentProduct.getDimensions()[2]+","+currentProduct.getPrice()+","+currentProduct.getNumberOfProducts()+","+currentProduct.getColor());
		fileWriterEletronics.write(System.getProperty("line.separator"));
	}
	
	
	for(int i=0;i<listOfClothing.size();i++)
	{
		Clothing currentProduct=listOfClothing.get(i);
		fileWriterClothing.write(currentProduct.getCode()+","+currentProduct.getDescription()+","+currentProduct.getSize()+","+currentProduct.getPrice()+","+currentProduct.getNumberOfProducts()+","+currentProduct.getColor()+","+currentProduct.getGender());
		fileWriterClothing.write(System.getProperty("line.separator"));
	}
	fileWriterEletronics.close();
	fileWriterClothing.close();
}
}
