package store;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import store.core.Client;
import store.core.Clothing;
import store.core.Eletronic;
import store.core.Manager;
import store.core.Product;
import store.core.Stock;
import store.core.StoreLog;
import store.dbi.DBInterface;
import store.ui.ClientInterface;
import store.ui.ManagerInterface;
import store.ui.StoreInterface;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

public class Store {
	private static Manager manager;
	private static Map <String , Client> clients;
	private static Stock stock;
	/**
	 * @param args
	 * @throws FileNotFoundException 
	 * @throws UnsupportedEncodingException 
	 */
	public static void main(String[] args) throws IOException {
		Store store = new Store();
		store.createAndShowUI();
	}
	
	private ManagerInterface managerInterface;
	private ClientInterface clientInterface;
	private StoreInterface currentInterface;
	protected final BufferedReader reader;
	
	public Store() throws UnsupportedEncodingException, FileNotFoundException {
		this.managerInterface = new ManagerInterface();
		this.clientInterface = new ClientInterface();
		this.currentInterface = null;
		this.reader = new BufferedReader(new InputStreamReader(System.in));
		DBInterface dbInterface = new DBInterface();		
		Store.stock=new Stock();
		Store.stock.setStockProducts(dbInterface.loadStockFromTxt());
		//stock loaded
		clients=dbInterface.loadClientsFromTxt();
		//sales log initialized
		Date today = new Date();
		StoreLog storeLog = new StoreLog();
		float salesToday;
		if ((salesToday = dbInterface.loadSalesFromTxt(today)) != -1)
			StoreLog.setSalesFromToday(salesToday);
		//in case the program is loaded more than once in a day 
		//this would load the money earned this far
		return;
	}
	public static Product getProduct(int code){
			Iterator<Product> nextProduct = stock.getStockProducts().iterator();
			while(nextProduct.hasNext()){
				Product currentProduct = nextProduct.next();
				if (currentProduct.getCode() == code){
					if(currentProduct.getClass().toString().equals("class store.core.Eletronic")){
						Eletronic eletronic=new Eletronic(code,currentProduct.getDescription(),currentProduct.getColor(),currentProduct.getPrice(),currentProduct.getNumberOfProducts(),((Eletronic) currentProduct).getDimensions()[0],((Eletronic) currentProduct).getDimensions()[1],((Eletronic) currentProduct).getDimensions()[2]);
								return eletronic;
					}
					else{
						Clothing clothing=new Clothing(code,currentProduct.getDescription(),currentProduct.getColor(),currentProduct.getPrice(),currentProduct.getNumberOfProducts(),((Clothing) currentProduct).getColor(),((Clothing) currentProduct).getGender());
						return clothing;
					}
				}					
			}			
			return null;
	}
	
	
	public static Map <String , Client> getClientsList(){
		return clients;		
	}
	
	public static Manager getManager(){
		return manager;
	}
	
	public static List<Product> getStock(){
		return stock.getStockProducts();
	}
	
	public static void updateStock(List<Product> listOfProducts){
		Stock.updateStock(listOfProducts);
	}
	
	public void createAndShowUI() throws IOException{
	   
	    Integer option = 0;
	    boolean isInputValid = false;
	    
	    System.out.print(getMenu());
      	do{
      		
	  		try {
	  			option = new Integer(reader.readLine());
	  			isInputValid = true;
	  		} catch (NumberFormatException | IOException e) {
	  			System.out.println("Invalid Option.");
	  			System.out.print(getMenu());
	  		}
      	}
  		while(!isInputValid);
	    
	    while (option != 3) {
	    		
	      switch (option) {
	      case 1:
	        this.currentInterface = managerInterface;
	        break;
	      case 2:
	        this.currentInterface = clientInterface;
	        break;
	      default:
	        System.out.println("Invalid Option.");
	        break;
	      }
	    if(option == 1 || option == 2)
	      this.currentInterface.createAndShowUI();
	    
	      System.out.print(getMenu());
	      isInputValid = false;
	      	do{
		  		try {
		  			option = new Integer(reader.readLine());
		  			isInputValid = true;
		  		} catch (NumberFormatException | IOException e) {
		  			System.out.println("Invalid Option.");
		  			System.out.print(getMenu());
		  		}
	      	}
	  		while(!isInputValid);
	      
	    }
		DBInterface dbInterface=new DBInterface();
		dbInterface.saveSales();
		dbInterface.saveStock(stock.getStockProducts());
	}
	
	public static boolean hasEnoughInStock(int code,int quantity){
		return Stock.hasEnoughInStock(code, quantity);		
	}
	private String getMenu() {
		StringBuffer sb = new StringBuffer();
		sb.append("Choose the bank interface:\n");
		sb.append("1 - Manager\n");
		sb.append("2 - Client \n");
		sb.append("3 - Exit \n");
		sb.append("Chosen option: ");
		return sb.toString();
	}
	
	
	public static void setManager(Manager manager){
		Store.manager=manager;
	}
}


