package store.core;

public class Product {
	
	private int code;
	private String description;
	private String color;
	private float price;
	private int quantity; //used to be numberInStock, but quantity is more general
	
	public Product (int code, String description, String color, float price,int quantity){
		this.code=code;
		this.description=description;
		this.color=color;
		this.price=price;
		this.quantity=quantity;
	}
	public Product(){
		this.code=0;
		this.description="";
		this.color="";
		this.price=0;
	}
	
	public void setCode(int code){
		this.code=code;
	}
	public void setDescrpiton(String description){
		this.description=description;
	}
	public void setColor(String color){
		this.color=color;
	}
	public void setPrice(float price){
		this.price=price;
	}
	public void setNumberOfProducts(int numberOfProducts){
		this.quantity=numberOfProducts;
	}

	public int getCode(){
		return code;
	}
	
	public String getDescription(){
		return description;
	}
	
	public String getColor(){
		return color;
	}
	
	public float getPrice(){
		return price;
	}
	
	/*public int getNumberInStock(){
		return quantity;
	}*/
	
	public int getNumberOfProducts(){
		return quantity;
	}
	
}
