package store.core;

public class User {
	
	private String firstName;
	private String lastName;
	private int accessCode;
	private String password;
	
	public User(){
		this.firstName=null;
		this.lastName=null;
		this.accessCode=0;
		this.password="123";
	}
	public User(String firstName,String lastName, int accessCode,String password){
		this.firstName=firstName;
		this.lastName=lastName;
		this.accessCode=accessCode;
		this.password=password;
	}
	
	public boolean isValidPassword(String password) {
		return this.password.equals(password);
	}
	
	public Integer getAccessCode(){
		return accessCode;
	}
	
	public String getFullName(){
		return firstName+" "+lastName;
	}
	
	public String getPassword(){
		return password;
	}
	
	public void setAccessCode(int accessCode){
		this.accessCode = accessCode;
	}
	
	public void setFirstName(String firstName){
		this.firstName = firstName;
	}
	
	public void setLastName(String lastName){
		this.lastName = lastName;
	}
	
	public void setPassword(String password){
		this.password = password;
	}
	
	
	
	

}
