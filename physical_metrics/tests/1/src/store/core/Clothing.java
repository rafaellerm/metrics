package store.core;

public class Clothing extends Product{
	
	private String size;
	private String gender;
	
public Clothing(){
		
	}
public Clothing(int code, String description, String color, float price,int quantity,String size, String gender){
	super(code,description,color,price,quantity);
	this.size=size;
	this.gender=gender;
}
	
	public String getSize(){
		return size;
	}
	
	public String getGender(){
		return gender;
	}

	public void setSize(String size) {
	this.size=size;
		
	}

	public void setGender(String gender) {
		this.gender=gender;
	}
	

}
