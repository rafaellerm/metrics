package store.core;

import java.util.Date;

public class Client extends User {
	
	private String address;
	private String cpf;
	private Date birthDate;
	private ShoppingCart shoppingCart;

	
	public Client(String firstName,String lastName, int accessCode,String password,
			String address,String cpf,Date birthDate) {
		super(firstName,lastName,accessCode,password);
		this.address=address;
		this.cpf=cpf;
		this.birthDate=birthDate;
		this.shoppingCart=new ShoppingCart();		
	}
	
	
	public Client(){
		super();
		this.address=null;
		this.cpf=null;
		this.birthDate=new Date();
		this.shoppingCart=new ShoppingCart();
		}
	public ShoppingCart getShoppingCart(){
		return shoppingCart;
	}
	public String getAddress(){
		return address;
	}
	public String getCpf(){
		return cpf;
	}
	
	public Date getBirthDate(){
		return this.birthDate;
	}
}