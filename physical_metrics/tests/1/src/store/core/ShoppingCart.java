package store.core;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ShoppingCart {
	
	private List<Product> listOfProducts;
	private float totalPrice;
	
	public ShoppingCart(){
		this.listOfProducts=new ArrayList<Product>();
		this.totalPrice=0;
	}
	
	public void listProducts(){
		
	}
	
	public void emptyShoppingCart(){
		this.listOfProducts.clear();
		this.setTotalPrice(0);
	}
	
	public void insertProduct(Product product){
		for(int i=0;i<listOfProducts.size();i++){
			
				if(listOfProducts.get(i).getCode()==product.getCode()){
					
					this.setTotalPrice(this.getTotalPrice() + product.getNumberOfProducts()*product.getPrice());
					listOfProducts.get(i).setNumberOfProducts(product.getNumberOfProducts()+listOfProducts.get(i).getNumberOfProducts());
					return;
					
				}
		}
		
		this.setTotalPrice(this.getTotalPrice() + product.getNumberOfProducts()*product.getPrice());
		listOfProducts.add(product);
	}
				
	public int getQuantityOfAProduct(int code){
		for(int i=0;i<this.listOfProducts.size();i++){
			
			if(this.listOfProducts.get(i).getCode()==code){
				
				return this.listOfProducts.get(i).getNumberOfProducts();
			}
		}
		return 0;
	}
	
	public void removeProduct(int code, int quantityToBeRemoved){
		for(int i=0;i<listOfProducts.size();i++){
			
			if(listOfProducts.get(i).getCode()== code){	
				
				this.setTotalPrice(this.getTotalPrice() - quantityToBeRemoved*listOfProducts.get(i).getPrice());
				listOfProducts.get(i).setNumberOfProducts(listOfProducts.get(i).getNumberOfProducts()-quantityToBeRemoved);
				
				if(listOfProducts.get(i).getNumberOfProducts()== 0)
					listOfProducts.remove(i);
				return;
	
			}
			
		}
	}
	
	
	public boolean isCartEmpty(){
		if (listOfProducts.isEmpty())
			return true;
		else 
			return false;
		
	}
	
	public float getTotalPrice(){
		return totalPrice;
		
	}
	

	public void setTotalPrice(float totalPrice){
		this.totalPrice = totalPrice;;
		
	}
	
	public List<Product> getListOfProducts(){
		return listOfProducts;
		
	}

	public boolean hasEnoughInShoppingCart(int code, int quantity) {
		if (quantity <= 0)
			return false;
		else{
			Iterator<Product> nextProduct = this.listOfProducts.iterator();
			while(nextProduct.hasNext()){	
				Product currentProduct = nextProduct.next();
				if (currentProduct.getCode() == code){
					if (quantity - currentProduct.getNumberOfProducts() <= 0)
						return true;
					else
						return false;	
				}	
			}
			return false;
			
			}	
	}
}
