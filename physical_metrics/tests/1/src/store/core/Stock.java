package store.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import store.Store;

public class Stock {
	//TODO
	//refactorate this class. filterList(), sortList()
	//should be moved to SearchCommand.
	private List<Product> stockProducts;
	
	public Stock(){
		this.stockProducts =new ArrayList<Product>();
	}
	
	public void setStockProducts(List<Product> stockFromTxt){
		this.stockProducts=stockFromTxt;
	}
	
	public List<Product> getStockProducts(){
		return this.stockProducts;
	}
	
	
	public static boolean hasEnoughInStock(int code, int quantity){
		  if (quantity <= 0)
		   return false;
		  else{
		   Iterator<Product> nextProduct = Store.getStock().iterator();
		   while(nextProduct.hasNext()){		 
		    Product currentProduct = nextProduct.next();
		    if (currentProduct.getCode() == code){
		     if(currentProduct.getNumberOfProducts()  >= quantity)
		      return true;
		     else
		      return false;  
		    } 
		   }
		   return false;		   
		  }
}
	
	public static void updateStock(List<Product> listOfProducts){
		for(int i=0;i<listOfProducts.size();i++){
			for(int j=0;j<Store.getStock().size();j++){
				
				if(listOfProducts.get(i).getCode()==Store.getStock().get(j).getCode()){
					
					Store.getStock().get(j).setNumberOfProducts(Store.getStock().get(j).getNumberOfProducts() - listOfProducts.get(i).getNumberOfProducts());
					j = Store.getStock().size();
				}
				
			} 
		}
		Collections.sort(listOfProducts,new ProductComparator("Code"));
	}
	
}
