package store.core;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class Purchase {

	private int clientCode;
	private int numberOfItens;
	private List<Product> listOfProducts;
	private float totalPriceOfEletronics=0;
	private float totalPriceOfClothing=0;
	private Date dateOfPurchase;
	
	public Purchase (List<Product> listOfProducts, int clientCode){
		this.clientCode=clientCode;
		this.numberOfItens=listOfProducts.size();
		this.dateOfPurchase=new Date();
		Iterator<Product> nextProduct = listOfProducts.iterator();
		
		do{
			Product currentProduct = nextProduct.next();
			if (currentProduct.getClass().equals(Eletronic.class))
			{
				totalPriceOfEletronics+=currentProduct.getPrice()* currentProduct.getNumberOfProducts();
			}
			else
			{
				totalPriceOfClothing+=currentProduct.getPrice()* currentProduct.getNumberOfProducts();
			}
		}
		while(nextProduct.hasNext());
	}
	
	public void listProducts(){
	}
	
	public List<Product> getListofProducts(){
		return listOfProducts;
	}
	
	public float getTotalPriceOfEletronics(){
		return totalPriceOfEletronics;
	}
	
	public float getTotalPriceOfClothing(){
		return totalPriceOfClothing;
	}
	
	public Date getDateOfPurchase(){
		return dateOfPurchase;
	}
	
	
	
	
	
}
