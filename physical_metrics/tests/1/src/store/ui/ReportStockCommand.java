package store.ui;


import java.util.List;
import store.core.Product;
import util.Util;

public class ReportStockCommand extends Command {
	public ReportStockCommand(){
		setEnabled(false);
	}
	@Override
	public void execute(StoreInterface storeInterface) {
		List<Product> listOfProducts = store.Store.getStock();
		System.out.println("Stock:");
		Util.printListOfProducts(listOfProducts);
	}
}
