package store.ui;

import java.io.IOException;
import java.util.Map;

import store.core.Client;

public class ReportClientsCommand extends Command {
	
	public ReportClientsCommand(){
		setEnabled(false);
	}
	
	@Override
	public void execute(StoreInterface storeInterface) throws IOException {
		Map<String,Client> mapOfClients = store.Store.getClientsList();
		System.out.println("Clients:");
		for (String acessCode : mapOfClients.keySet()) {
		    Client currentClient = mapOfClients.get(acessCode);
		    System.out.println("Name: "+currentClient.getFullName()+", Access Code: "+currentClient.getAccessCode());
		         }    
		   
		return;
		
	}
	
	
}
