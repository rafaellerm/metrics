package store.ui;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import store.core.StoreLog;

public class ReportSalesCommand extends Command {
	
	public ReportSalesCommand(){
		setEnabled(false);
	}

	@Override
	public void execute(StoreInterface storeInterface) throws IOException {
		boolean isCorrectDate=false;
		String date = null;
		Date dateToBeChecked = new Date();
		while(!isCorrectDate){
		isCorrectDate=true;
		System.out.println("Enter the day you want to see the sales(dd/mm/yyyy):");
		date = new String (storeInterface.reader.readLine());
		SimpleDateFormat dateFormat = new SimpleDateFormat ("dd/MM/yyyy");
		dateToBeChecked = new Date();
		try {
			dateToBeChecked = dateFormat.parse(date);
		} catch (ParseException e) {
			System.out.println("Invalid date.");
			isCorrectDate=false;
		}
		}
		float totalValueEarned = StoreLog.getSales(dateToBeChecked);
		if(totalValueEarned==-1){
			System.out.println("There is no log of this date.");
		}else
		System.out.println("Money earned in "+date+": $"+totalValueEarned);
		
		return;
	}
	
	
}
