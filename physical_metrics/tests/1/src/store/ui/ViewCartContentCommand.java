package store.ui;

import java.io.IOException;
import util.Util;
import java.util.List;

import store.core.Product;

public class ViewCartContentCommand extends Command {
	
	public ViewCartContentCommand(){
		setEnabled(false);
	}

	@Override
	public void execute(StoreInterface storeInterface) throws IOException {
		List<Product> listOfProducts = ((ClientInterface)storeInterface).getUser().getShoppingCart().getListOfProducts();	
		Util.printListOfProducts(listOfProducts);
	}
	
	
}
