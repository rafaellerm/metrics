package store.ui;

import java.io.IOException;
import java.util.List;

import store.Store;
import store.core.Client;
import store.core.Product;
import store.core.Purchase;
import store.core.ShoppingCart;
import store.core.Stock;
import store.core.StoreLog;

public class FinishCommand extends Command {
	
	public FinishCommand(){
		setEnabled(false);
	}
	
	@Override
	public void execute(StoreInterface storeInterface) throws IOException {
		Client client = ((ClientInterface)storeInterface).getUser();
		ShoppingCart cart = client.getShoppingCart();
		if(!(cart.isCartEmpty())){
			List<Product> listOfProducts=cart.getListOfProducts();
			int clientCode = client.getAccessCode();
			Purchase purchase = new Purchase(listOfProducts, clientCode);
			StoreLog.updateLog(purchase);
			Store.updateStock(listOfProducts);
			System.out.println("Purchase status: successfull. Purchase Price: "+(purchase.getTotalPriceOfClothing()+purchase.getTotalPriceOfEletronics()));
		}else
			System.out.println("Purchase status: canceled. Shopping Cart is Empty.");
		
		cart.emptyShoppingCart();
		((ClientInterface)storeInterface).finish();
	}	
}
