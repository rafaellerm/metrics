package store.ui;

import java.io.IOException;

import store.core.Client;

public class ClientLogin extends Command {
	
	public ClientLogin(){
		setEnabled(true);
	}

	@Override
	public void execute(StoreInterface storeInterface) throws IOException {
		Client client = (Client) storeInterface.readUser("CLIENT");
		System.out.print("Password: ");
		String password = storeInterface.reader.readLine();
		if (client != null) {
			boolean validPassword = client.isValidPassword(password);
			if (validPassword) {
				((ClientInterface)storeInterface).login(client);
				return;
			}
		}
		System.out.println("Invalid data!");
	}	
}
	
	

