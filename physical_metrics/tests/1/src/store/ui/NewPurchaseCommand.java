package store.ui;

import java.io.IOException;

public class NewPurchaseCommand extends Command {
	
	public NewPurchaseCommand(){
		setEnabled(false);
	}
	@Override
	public void execute(StoreInterface storeInterface) throws IOException {
		((ClientInterface)storeInterface).newPurchase();		
		((ClientInterface)storeInterface).getUser().getShoppingCart().emptyShoppingCart();
	}
}
