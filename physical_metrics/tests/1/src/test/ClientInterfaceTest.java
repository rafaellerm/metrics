package test;

import java.io.IOException;

import org.junit.Test;

import store.core.Client;
import store.ui.ClientInterface;

public class ClientInterfaceTest {

	@Test
	public void test() throws IOException {
		Client client = new Client(null, null, 0, null, null, null, null);
		ClientInterface clientInterface=new ClientInterface();
		clientInterface.login(client);
		clientInterface.createAndShowUI();		
	}

}
