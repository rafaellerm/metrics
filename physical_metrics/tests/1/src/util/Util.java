package util;

import java.util.Iterator;
import java.util.List;
import store.core.Product;

public class Util {
	
	public static void printListOfProducts(List<Product> listOfProducts){
		if(!listOfProducts.isEmpty()){
		for(int i=0; i<listOfProducts.size(); i++){
		   Product currentProduct = listOfProducts.get(i);
		   System.out.println("Description: "+currentProduct.getDescription()+" - Quantity: "+currentProduct.getNumberOfProducts()+" - Price: "+currentProduct.getPrice()+" - Code: "+currentProduct.getCode());
		  }
		return;
	}
	else
		System.out.println("List of products is empty.");
	}

}
