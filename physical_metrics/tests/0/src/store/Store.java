package store;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import ui.CustomerInterface;
import ui.ManagerInterface;
import ui.StoreInterface;
import core.ClothingProduct;
import core.ElectronicProduct;
import core.User;
import data.FileParser;


public class Store {

	public static Map<Long, User> USERS;

	public static Map<Long, ElectronicProduct> ELECTRONICS;

	public static Map<Long, ClothingProduct> CLOTHING;
	
	private static FileParser fileParser = new FileParser();
	
	public static String pathEletroFile = "eletro.txt";
	
	public static String pathClothingFile = "vestuario.txt";
	
	public static String pathUsersFile = "usuarios.txt";
	
	public static String pathLogFile = "log.txt";
	
	
	static {
		USERS = new HashMap<>();
		USERS = fileParser.parseUsers(pathUsersFile);

		ELECTRONICS = new HashMap<>();
		ELECTRONICS = fileParser.parseElectronics(pathEletroFile);
		
		CLOTHING = new HashMap<>();
		CLOTHING = fileParser.parseClothing(pathClothingFile);
		

	}
	
	private ManagerInterface managerInterface;
	private CustomerInterface customerInterface;
	private StoreInterface currentInterface;
	
	protected final BufferedReader reader;
	
	public Store() {
		this.reader = new BufferedReader(new InputStreamReader(System.in));
		this.currentInterface = null;
		this.managerInterface = new ManagerInterface();
		this.customerInterface = new CustomerInterface();
	}


	public String getMenu() {
		StringBuffer menuOptions = new StringBuffer();
		menuOptions.append("Choose one option:\n");
		menuOptions.append("0 - Customer area\n");
		menuOptions.append("1 - Manager area\n");
		menuOptions.append("2 - Exit\n");
		
		return menuOptions.toString();
	}
	
	
	public void createAndShowGUI() throws NumberFormatException, IOException {
		
		boolean invalidCommand = false;
		
		System.out.println(getMenu());
		System.out.print("Option: ");
		Integer option = new Integer(reader.readLine());
		while(option != 2) {
			
			switch(option) {
				case 0:
					this.currentInterface = this.customerInterface;
					invalidCommand = false;
				break;
				case 1:
					this.currentInterface = this.managerInterface;
					invalidCommand = false;
				break;
				default:
					System.out.println("Invalid command!");
					invalidCommand = true;
				break;
			}
			
			if(!invalidCommand)
				this.currentInterface.createAndShowGUI();
			
				System.out.print(getMenu());
				option = new Integer(reader.readLine());
		}

	}
	

	public static void main(String[] args) throws IOException {
		Store store = new Store();
		store.createAndShowGUI();
		
		System.out.println("Good-bye!");
	}

}
