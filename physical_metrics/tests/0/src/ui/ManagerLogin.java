package ui;


import java.io.IOException;

import core.Manager;
import core.User;

/**
 * @author ingrid
 * 
 */
public class ManagerLogin extends Command {

	public ManagerLogin() {
		setEnabled(true);
	}


	@Override
	public void execute(StoreInterface storeInterface) throws IOException {
		User loggedUser = storeInterface.readUser();		
		System.out.print("Password: ");
		String password = storeInterface.reader.readLine();

		if (loggedUser != null && loggedUser.getClass().equals(Manager.class)) {
			boolean validPassword = loggedUser.isValidPassword(
					password);
			if (validPassword) {
					((ManagerInterface) storeInterface).login((Manager)loggedUser);
				return;
			}
		}
		System.out.println("Invalid data!");
	}
		
}


