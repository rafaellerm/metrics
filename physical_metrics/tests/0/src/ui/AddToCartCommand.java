package ui;

import java.io.IOException;

import core.Customer;
import core.Product;

import data.EnumProducts;
import data.ProductSearch;


public class AddToCartCommand extends Command {

	public AddToCartCommand() {
		setEnabled(false);
	}

	@Override
	public void execute(StoreInterface storeInterface) throws IOException {
		
		ProductSearch productSearch = new ProductSearch();
		
		getMenuTypeOfProduct();
		String option = storeInterface.reader.readLine();

		if(!selectTypeOfProduct(option, productSearch))
			return;
		
		showProducts(productSearch);
		
		System.out.print("Select the code of product you want to add in your shopping cart: ");
		String codeProduct = storeInterface.reader.readLine();
		
		Customer customer = (Customer) storeInterface.getLoggedUser();
		
		System.out.print("Quantity: ");
		String quantityProduct = storeInterface.reader.readLine();
		
		Product productAdded = (Product) productSearch.getBufferOut().get(Long.parseLong(codeProduct));
		
		if(productSearch.getBufferOut().get(Long.parseLong(codeProduct)) == null)
			System.out.println("\nInvalid code!\n");
		else {
			
			boolean added = customer.addShoppingCart(productAdded,Integer.parseInt(quantityProduct));
			if(added)
				System.out.println("\n" + productAdded.getDescription() + " was added in your shopping cart.\n");
		
		}
		
	}
	
	private void showProducts(ProductSearch productSearch) {
		
		if(productSearch.getBufferOut().isEmpty()) {
			System.out.println("There are not products with these caracteristics.\n");
			return;
		}
		
		for (Long key : productSearch.getBufferOut().keySet()) {
			Product product = (Product) productSearch.getBufferOut().get(key);
				System.out.println("\n" + product.getCode() + " - " + product.getDescription());
				System.out.println("Price: " + product.getPrice());
				System.out.println("Stock amount: " + product.getStockAmount() + "\n");
		}
	}

	
	private void getMenuTypeOfProduct() {
		System.out.println("Select the type of the product you are looking for:");
		System.out.println("1 - Electro-Electronics Products");
		System.out.println("2 - Clothing Products");
	}
	
	private boolean selectTypeOfProduct(String option, ProductSearch productSearch) {
		
		switch(option) {
			case "1":
				productSearch.setTypeOfProduct(EnumProducts.Electronic);
			break;
			case "2":
				productSearch.setTypeOfProduct(EnumProducts.Clothing);
			break;
			
			default:
				System.out.println("Invalid command!\n");
				return false;
		}
		
		return true;
	}
	
}
