package ui;


import java.io.IOException;

import core.Customer;
import core.User;

/**
 * @author ingrid
 * 
 */
public class CustomerLogin extends Command {

	public CustomerLogin() {
		setEnabled(true);
	}


	@Override
	public void execute(StoreInterface storeInterface) throws IOException {
		User loggedUser = storeInterface.readUser();
		System.out.print("Password: ");
		String password = storeInterface.reader.readLine();

		if (loggedUser != null && loggedUser.getClass().equals(Customer.class)) {
			boolean validPassword = loggedUser.isValidPassword(
					password);
			if (validPassword) {
					((CustomerInterface) storeInterface).login((Customer)loggedUser);
				return;
			}
		}
		System.out.println("Invalid data!");
	}
		
}


