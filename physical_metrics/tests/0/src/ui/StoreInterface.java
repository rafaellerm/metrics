package ui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import store.Store;
import core.User;

public abstract class StoreInterface {

	protected Map<String, Command> commands;
	protected final BufferedReader reader;


	public StoreInterface() {
		this.reader = new BufferedReader(new InputStreamReader(System.in));
		this.commands = new HashMap<>();
	}

	public void createAndShowGUI() throws IOException {
		Command command = null;
		String commandKey;
		do {
			System.out.print(getMenu());
			commandKey = reader.readLine();
			command = commands.get(commandKey);
			if (command != null && command.isEnabled()) {
				command.execute(this);
			} else {
				System.out.println("\nInvalid command.\n");
				
			}
		} while (command != null);
		if (isLoggedIn()) {
			logout();
		}
	}
	
	public String getMenu() {
		StringBuffer sb = new StringBuffer();
		sb.append("Choose one option (or E to exit):\n");
		for (String key : commands.keySet()) {
			Command command = commands.get(key);
			if (command.isEnabled()) {
				sb.append(key).append(" - ")
						.append(command.getClass().getSimpleName())
						.append("\n");
			}
		}
		sb.append("Option: ");

		return sb.toString();
	}

	public abstract boolean isLoggedIn();

	public abstract void logout();
	
	public abstract User getLoggedUser();
	
	
	protected void toogleCommands() {
		for (String key : commands.keySet()) {
			Command command = commands.get(key);
			command.setEnabled(!command.isEnabled());
		}
	}
	
	protected User readUser() throws IOException {
		System.out.print("Access Code: ");
		Long accessCode = new Long(reader.readLine());
		
		User loggedUser = Store.USERS.get(accessCode);
		if (loggedUser != null)
			return loggedUser;

		return null;
	}
	

}
