package ui;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import core.Product;
import data.EnumProducts;
import data.ProductSearch;


public class FindProductCommand extends Command {


	public FindProductCommand() {
		setEnabled(false);
	}

	@Override
	public void execute(StoreInterface storeInterface) throws IOException {
		
		ProductSearch productSearch = new ProductSearch();
		
		getMenuTypeOfProduct();
		String option = storeInterface.reader.readLine();

		if(!selectTypeOfProduct(option, productSearch))
			return;
		
		getMenuFilters();
		option = storeInterface.reader.readLine();
		
		// None
		if(!option.equals("4"))
		{
			getMenuFilterWord(option);
			String filterWord = storeInterface.reader.readLine();
			
			if(!selectFilter(option,filterWord,productSearch))
				return;
		}
		
		showProducts(productSearch);
		
	}
	
	
	private void showProducts(ProductSearch productSearch) {
		
		if(productSearch.getBufferOut().isEmpty()) {
			System.out.println("There are not products with these caracteristics.\n");
			return;
		}
		
		for (Long key : productSearch.getBufferOut().keySet()) {
			Product product = (Product) productSearch.getBufferOut().get(key);
				System.out.println("\nDescription: " + product.getDescription());
				System.out.println("Code: " + product.getCode());
				System.out.println("Color: " + product.getColor());
				System.out.println("Price: " + product.getPrice());
				System.out.println("Stock amount: " + product.getStockAmount() + "\n");
			}
	}
	
	private void getMenuFilterWord(String option) {
		
		switch(option) {
			case "1":
				System.out.print("Color: ");
			break;
			case "2":
				System.out.print("Maximum price: ");
			break;
			case "3":
				System.out.print("Description: ");
			break;
		}
	}
	
	private void getMenuTypeOfProduct() {
		System.out.println("Select the type of the product you are looking for:");
		System.out.println("1 - Electro-Electronics Products");
		System.out.println("2 - Clothing Products");
	}
	
	private boolean selectTypeOfProduct(String option, ProductSearch productSearch) {
		
		switch(option) {
			case "1":
				productSearch.setTypeOfProduct(EnumProducts.Electronic);
			break;
			case "2":
				productSearch.setTypeOfProduct(EnumProducts.Clothing);
			break;
			
			default:
				System.out.println("Invalid command!\n");
				return false;
		}
		
		return true;
	}
	
	
	private boolean selectFilter(String option, String filterWord, ProductSearch productSearch) {
		
		Map<String,String> filters = new HashMap<>();

		switch(option) {
			case "1":
				filters.put("color", filterWord);
			break;
			case "2":
				filters.put("price", filterWord);
			break;
			case "3":
				filters.put("description", filterWord);
			break;
			case "4":
				return true;
			
			default:
				System.out.println("Invalid command!\n");
			return false;
		}
		
		productSearch.setFilters(filters, true);
		
		return true;

	}
	
	private void getMenuFilters() {
		System.out.println("\nSelect one filter to apply:");
		System.out.println("1 - Color");
		System.out.println("2 - Price");
		System.out.println("3 - Description");
		System.out.println("4 - None");
	}
	
}
