package ui;

import core.Customer;

public class CustomerInterface extends StoreInterface {

	private Customer customer;
	
	public CustomerInterface() {
		this.commands.put("L", new CustomerLogin());
		this.commands.put("F", new FindProductCommand());
		this.commands.put("V", new ViewCartCommand());
		this.commands.put("R", new RemoveFromCartCommand());
		this.commands.put("A", new AddToCartCommand());
		this.commands.put("H", new CheckoutCommand());
		this.commands.put("C", new CancelPurchaseCommand());
		this.commands.put("O", new LogoutCommand());
	}

	public Customer getLoggedUser() {
		return customer;
	}

	public boolean isLoggedIn() {
		return customer != null;
	}

	public void login(Customer customer) {
		this.customer = customer;
		if (isLoggedIn()) {
			toogleCommands();
		}
	}


	@Override
	public void logout() {
		this.customer = null;
		toogleCommands();
	}

}
