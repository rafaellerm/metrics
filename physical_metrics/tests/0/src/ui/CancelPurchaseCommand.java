package ui;

import java.io.IOException;

import core.Customer;

/**
 *  @author ingrid 
 *   
 * 
 */
public class CancelPurchaseCommand extends Command {

	public CancelPurchaseCommand() {
		setEnabled(false);
	}


	@Override
	public void execute(StoreInterface storeInterface) throws IOException {
		
		Customer customer = new Customer();
		customer = (Customer) storeInterface.getLoggedUser();
		
		System.out.println("Do really want to clear your Shopping Cart? Y / N");
		String option = storeInterface.reader.readLine();
		
		switch(option) {
			case "Y":
			case "y":
				customer.getShoppingCart().clearCart();
			break;
			case "N":
			case "n":
				return;
				
			default:
				System.out.println("\nInvalid option!\n");
			
		}
		
		System.out.println("\nYour shopping cart is empty.\n");
	}

}
