package ui;

import core.Manager;

public class ManagerInterface extends StoreInterface {

	private Manager manager;

	public ManagerInterface() {
		this.commands.put("L", new ManagerLogin());
		this.commands.put("F", new FindProductCommand());
		this.commands.put("S", new StockReportCommand());
		this.commands.put("C", new CustomersReport());
		this.commands.put("V", new SalesReportCommand());
		this.commands.put("O", new LogoutCommand());
	}

	public Manager getManager() {
		return null;
	}

	public boolean isLoggedIn() {
		return manager != null;
	}

	public void login(Manager manager) {
		this.manager = manager;
		if (isLoggedIn()) {
			toogleCommands();
		}
	}

	public void logout() {
		this.manager = null;
		toogleCommands();
	}
	
	public Manager getLoggedUser() {
		return manager;
	}

}
