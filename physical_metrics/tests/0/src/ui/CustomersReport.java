package ui;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import core.User;
import data.UserSearch;


public class CustomersReport extends Command {

	public CustomersReport() {
		setEnabled(false);
	}

	@Override
	public void execute(StoreInterface storeInterface) throws IOException {
		
		Map<String,String> filters = new HashMap<String,String>();
		
		filters.put("post", "customer");
		
		UserSearch userSearch = new UserSearch();
		userSearch.setFilters(filters, true);
		
		if(userSearch.getBufferOut() == null) {
			System.out.println("There are not registered customers in the store.");
			return;
		}
		
		for (Long key : userSearch.getBufferOut().keySet()) {
			User user = (User) userSearch.getBufferOut().get(key);
			System.out.println("\nName: " + user.getName());
			System.out.println("Last name: " + user.getLastName());
			System.out.println("Access Code: " + user.getAccessCode() + "\n");

		}
		
		if(userSearch.getBufferOut().size() == 0)
			System.out.println("There are not registered customers in the store.");
		else
			System.out.println("Number of registered customers: " + userSearch.getBufferOut().size() + "\n");
		
		return;
		
	}

}
