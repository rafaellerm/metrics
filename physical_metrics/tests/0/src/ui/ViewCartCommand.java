package ui;

import java.io.IOException;

import core.Customer;
import core.Product;
import core.ShoppingCart;

public class ViewCartCommand extends Command {

	public ViewCartCommand() {
		setEnabled(false);
	}


	@Override
	public void execute(StoreInterface storeInterface) throws IOException {
		
		Customer customer = new Customer();
		customer = (Customer) storeInterface.getLoggedUser();
		

		showShoppingCart(customer.getShoppingCart());	
	}
	
	private void showShoppingCart(ShoppingCart shoppingCart) {
		
		if(shoppingCart.isEmpty()) {
			System.out.println("\nYour shopping cart is empty.\n");
			return;
		}
		
		for (Product product : shoppingCart.getShoppingCart().keySet()) {
			Integer quantityOfProducts = shoppingCart.getShoppingCart().get(product);
				System.out.println("\nDescription: " + product.getDescription());
				System.out.println("Code: " + product.getCode());
				System.out.println("Color: " + product.getColor());
				System.out.println("Price: " + product.getPrice());
				System.out.println("Quantity in cart: " + quantityOfProducts);
				System.out.println("Stock amount: " + product.getStockAmount() + "\n");
			}
	
		System.out.println("\nPrice of purchase: " + shoppingCart.getTotalAmount() + "\n");

	}
	
	
}
