package ui;

import java.io.IOException;

import core.Customer;

/**
 *  @author ingrid 
 *   
 * 
 */
public class LogoutCommand extends Command {

	public LogoutCommand() {
		setEnabled(false);
	}

	@Override
	public void execute(StoreInterface storeInterface) throws IOException {
		
		System.out.println("Do you really want to exit? Y/N");
		String option = new String(storeInterface.reader.readLine());
		
		if(option.toUpperCase().equals("Y")) {
		
			try {
				Customer customer = (Customer) storeInterface.getLoggedUser();
				if(customer != null)
					if(customer.getShoppingCart() != null)
						customer.getShoppingCart().clearCart();
			
			
			}catch(Exception e) {
				e.getMessage();
			}
			
			storeInterface.logout();
		}
		
	}

}
