package ui;

import java.io.IOException;

import store.Store;

import core.Customer;
import core.Purchase;
import data.FileParser;

public class CheckoutCommand extends Command {

	public CheckoutCommand() {
		setEnabled(false);
	}

	@Override
	public void execute(StoreInterface storeInterface) throws IOException {
		
		System.out.println("Do you really want to fisish this purchase? Y / N ");
		String option = storeInterface.reader.readLine();
		
		switch(option) {
			case "Y":
			case "y":
				checkOut(storeInterface);
			break;
			
			case "N":
			case "n":
				return;
				
			default:
				System.out.println("\nInvalid command.\n");
			break;
		}
		
	}

	private void checkOut(StoreInterface storeInterface) throws IOException {
		
		Customer customerLogged = (Customer) storeInterface.getLoggedUser();
		
		FileParser fileParser = new FileParser();
		
		if(!customerLogged.getShoppingCart().isEmpty()) {
		
			Purchase purchase = new Purchase(customerLogged.getAccessCode(), customerLogged.getShoppingCart());
			
			
			fileParser.writeLogFile(purchase, customerLogged);
			
			customerLogged.getShoppingCart().updateStock();
			
			customerLogged.getShoppingCart().clearCart();
			
			fileParser.updateClothingAmount(Store.pathClothingFile);
			fileParser.updateElectronicAmount(Store.pathEletroFile);

			
			System.out.println("\nYour purchase was generated!\n");
		
		} else
		{
			System.out.println("\nYour shopping cart is empty.\n");
		}
		
	}

}
