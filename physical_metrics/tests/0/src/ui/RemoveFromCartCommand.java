package ui;

import java.io.IOException;

import core.Customer;

public class RemoveFromCartCommand extends Command {

	public RemoveFromCartCommand() {
		setEnabled(false);
	}


	@Override
	public void execute(StoreInterface storeInterface) throws IOException {
		
		System.out.print("Code of product you want to remove from your shopping cart: ");
		String codeProduct = storeInterface.reader.readLine();
		
		System.out.print("Quantity: ");
		String quantityProduct = storeInterface.reader.readLine();
		
		Customer customer = (Customer) storeInterface.getLoggedUser();
		
		if(customer.getProductFromCart(Long.parseLong(codeProduct)) == null)
			System.out.println("\nInvalid code!\n");
		else {
			System.out.println("\n" + customer.getProductFromCart(Long.parseLong(codeProduct)).getDescription() + " was removed from your shopping cart.\n");
			customer.removeFromShoppingCart(Long.parseLong(codeProduct),Integer.parseInt(quantityProduct));
		}
		
		
		
	}

}
