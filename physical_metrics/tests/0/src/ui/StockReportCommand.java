package ui;

import java.io.IOException;

import core.Product;
import data.EnumProducts;
import data.ProductSearch;

public class StockReportCommand extends Command {

	public StockReportCommand() {
		setEnabled(false);
	}


	@Override
	public void execute(StoreInterface storeInterface) throws IOException {
		
		System.out.println("Stock Report:");
		
		electronicsStock();
		
		clothingStock();
		
		return;
		
		
	}
	
	private void electronicsStock() {
		
		ProductSearch productSearch = new ProductSearch();
		productSearch.setTypeOfProduct(EnumProducts.Electronic);
		
		System.out.println("\nElectro-electronics products:");

		if(productSearch.getBufferOut() == null) {
			System.out.println("There are not registered electro-electronics products in the store.\n");
			return;
		}
		
		for (Long key : productSearch.getBufferOut().keySet()) {
			Product electroElectronic = (Product) productSearch.getBufferOut().get(key);
			System.out.println("\nDescription:: " + electroElectronic.getDescription());
			System.out.println("Stock amount: " + electroElectronic.getStockAmount());
		}
		
		
		if(productSearch.getBufferOut().size() == 0)
			System.out.println("\nThere are not registered eletro-electronics products in the store.");
		else
			System.out.println("\nNumber of registered eletro-electronics products: " + productSearch.getBufferOut().size() + "\n");
	
	}
	
	private void clothingStock() {
		
		ProductSearch productSearch = new ProductSearch();
		productSearch.setTypeOfProduct(EnumProducts.Clothing);
		
		System.out.println("\nClothing products:");
		
		if(productSearch.getBufferOut() == null) {
			System.out.println("There are not registered clothing products in the store.");
			return;
		}
		
		for (Long key : productSearch.getBufferOut().keySet()) {
			Product clothing = (Product) productSearch.getBufferOut().get(key);
				System.out.println("\nDescription:: " + clothing.getDescription());
				System.out.println("Stock amount: " + clothing.getStockAmount());
			}
		
		
		if(productSearch.getBufferOut().size() == 0)
			System.out.println("\nThere are not registered clothing products in the store.\n");
		else
			System.out.println("\nNumber of registered clothing products: " + productSearch.getBufferOut().size() + "\n");
	
	}

}
