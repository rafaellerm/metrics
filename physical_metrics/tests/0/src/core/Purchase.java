package core;
     
import data.EnumProducts;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import java.util.Map;
     
public class Purchase {
     
	private Long customerCode;
    private Map<EnumProducts, Integer> totalNumOfItems;
    private Map<EnumProducts, Double> totalPrice;
    private String date;
     
    public Purchase(Long customerCode, ShoppingCart shoppingCart) {
    	
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        this.date = dateFormat.format(new Date());
    	this.customerCode = customerCode;
    	this.totalNumOfItems = new HashMap<EnumProducts, Integer>();
    	this.totalPrice = new HashMap<EnumProducts, Double>();
    	EnumProducts enumThisProduct = null;
    	
		for (Product product : shoppingCart.getShoppingCart().keySet()) {
			Integer quantityOfProducts = shoppingCart.getShoppingCart().get(product);
				
				
				switch(product.getType()) {
					case Electronic:
						enumThisProduct = EnumProducts.Electronic;
					break;
					case Clothing:
						enumThisProduct = EnumProducts.Clothing;
					break;
					
					default:
						enumThisProduct = null;
					break;
				}
				
				if(enumThisProduct != null) {
					if(this.totalNumOfItems.containsKey(enumThisProduct) && this.totalPrice.containsKey(enumThisProduct)) {
						this.totalNumOfItems.put(enumThisProduct, this.totalNumOfItems.get(enumThisProduct)+quantityOfProducts);
						this.totalPrice.put(enumThisProduct, this.totalPrice.get(enumThisProduct) + quantityOfProducts*product.getPrice());
					}
					else {
						this.totalNumOfItems.put(enumThisProduct, quantityOfProducts);
						this.totalPrice.put(enumThisProduct, quantityOfProducts*product.getPrice());
					}	
				}
			}
    }
    
    public String getDate() {
    	return this.date;
    }
     
    public Long getCustomerCode() {
    	return this.customerCode;
    }
     
    public Integer getNumOfItemsByType(EnumProducts productType) {
    	if(this.totalNumOfItems.containsKey(productType))
    		return this.totalNumOfItems.get(productType);
    	else
    		return 0;
    }
          
    
    public Double getTotPriceByType(EnumProducts productType) {
    	if(this.totalPrice.containsKey(productType))
    		return this.totalPrice.get(productType);
    	else
    		return 0.0;
    }
    
}

