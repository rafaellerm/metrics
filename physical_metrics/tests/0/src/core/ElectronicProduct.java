package core;

import data.EnumProducts;

public class ElectronicProduct extends Product {

	private Double [] dimensions;

	public ElectronicProduct(Long code, String description, String color, Double price, Long stockAmount, Double[] dimensions) {
		super(code, description, color, price, stockAmount);
		this.dimensions = dimensions;
	}
	
	public EnumProducts getType() { 
		return EnumProducts.Electronic;
	}

	public Double[] getDimensions() {
		return this.dimensions;
	}

}
