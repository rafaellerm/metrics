package core;

import java.util.Date;

public class Customer extends User {

	private String address;

	private Date birthDate;

	private String cpf;

	private ShoppingCart shoppingCart;

	public String getAddress() {
		return this.address;
	}

	public Date getBirthDate() {
		return this.birthDate;
	}

	public String getCpf() {
		return this.cpf;
	}

	public Customer(String name, String lastName, Long accessCode, String password, String address, Date birthDate, String cpf) {
		super(name, lastName, accessCode, password);
		this.birthDate = birthDate;
		this.cpf = cpf;
		this.address = address;
		this.shoppingCart = new ShoppingCart();
	}


	public Customer() {
		super(null, null, null, null);
		this.address = null;
		this.birthDate = null;
		this.shoppingCart = null;
		this.cpf = null;
		this.shoppingCart = new ShoppingCart();
	}

	public ShoppingCart getShoppingCart() {
		return this.shoppingCart;
	}
	
	
	public boolean addShoppingCart(Product product, Integer quantity) {
		return this.shoppingCart.addProduct(product,quantity);
	}
	
	public void removeFromShoppingCart(Long codeOfProduct, Integer quantity) {
		this.shoppingCart.removeProduct(codeOfProduct, quantity);
	}
	
	public Product getProductFromCart(Long codeProduct) {
		return this.shoppingCart.getProductFromCart(codeProduct);
	}
	

}
