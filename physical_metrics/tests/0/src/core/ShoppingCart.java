package core;

import java.util.HashMap;
import java.util.Map;

public class ShoppingCart {

	private Map<Product,Integer> productsInCart;

	private Double totalAmount;

	public ShoppingCart() {
		this.totalAmount = 0.0;
		this.productsInCart = new HashMap<Product,Integer>();
	}

	public boolean addProduct(Product product, Integer quantity) {
		
		if(product == null || quantity == null)
			return false;
				
		if(this.productsInCart.containsKey(product)) {
			
			if(this.productsInCart.get(product)+quantity > product.getStockAmount()) {
				System.out.println("\nWe do not have this quantity of this product in our stock.\n");
				return false;
			}
			
			this.productsInCart.put(product, this.productsInCart.get(product)+quantity);

			
		}else {
			
			if(quantity > product.getStockAmount()) {
				System.out.println("\nWe do not have this quantity of this product in our stock.\n");
				return false;
			}
				
			this.productsInCart.put(product, quantity);
		
		}
		
		this.totalAmount += product.getPrice()*quantity;
		
		
		return true;
		
	}

	public void removeProduct(Long code, Integer quantity) {
		
		for (Product product : this.productsInCart.keySet()) {
			if(product.getCode() == code) {
				if(quantity >= this.productsInCart.get(product)) {
					this.totalAmount -= product.getPrice()*this.productsInCart.get(product);
					this.productsInCart.remove(product);
				}
				else {
					this.totalAmount -= product.getPrice()*quantity;
					this.productsInCart.put(product, this.productsInCart.get(product)-quantity);

				}

			}
		}
		
	}

	public Double getTotalAmount() {
		return this.totalAmount;
	}

	public void clearCart() {
		this.productsInCart.clear();
		this.totalAmount = 0.0;
	}

	public Map<Product, Integer> getShoppingCart() {
		return this.productsInCart;
	}
	
	public Integer getQuantityProductInCart(Product product) {
		return this.productsInCart.get(product);
	}
	
	public Product getProductFromCart(Long productCode) {
		
		for (Product product : this.productsInCart.keySet())
			if(product.getCode() == productCode)
				return product;
		
		return null;
		
	}
	
	public boolean isEmpty() {

		if(this.productsInCart.isEmpty())
			return true;
		else
			return false;
	}
	
	public void updateStock() {
		
		for (Product product : this.productsInCart.keySet()) {
			Integer quantityOfProducts = this.productsInCart.get(product);
			product.setStockAmount(product.getStockAmount()-quantityOfProducts);	
		}
	}
	

}
