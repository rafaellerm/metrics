package core;

import data.EnumProducts;

public class ClothingProduct extends Product {

	private String clothSize;

	private String gender;


	public String getClothSize() {
		return this.clothSize;
	}

	public String getGender() {
		return this.gender;
	}


	public ClothingProduct(Long code, String description, String color, Double price, Long stockAmount, String clothSize, String gender) {
		super(code, description, color, price, stockAmount);
		this.gender = gender;
		this.clothSize = clothSize;
	}

	public EnumProducts getType(){ 
		return EnumProducts.Clothing; 
	}

}
