package core;

import data.EnumProducts;

public abstract class Product {

	private Long code;

	private String description;

	private String color;

	private Double price;

	private Long stockAmount;
	
	public Product(Long code, String description, String color, Double price, Long stockAmount) {
		this.code = code;
		this.description = description;
		this.color = color;
		this.price = price;
		this.stockAmount = stockAmount;
	}
	
	public void setStockAmount(Long stockAmount) {
		this.stockAmount = stockAmount;
	}

	public Long getCode() {
		return this.code;
	}

	public String getDescription() {
		return this.description;
	}

	public String getColor() {
		return this.color;
	}

	public Long getStockAmount() {
		return this.stockAmount;
	}

	public Double getPrice() {
		return this.price;
	}

	public abstract EnumProducts getType();
	

}
