package core;


public abstract class User {

	private String name;

	private String lastName;

	private Long accessCode;

	private String password;

	public String getName() {
		return this.name;
	}

	public String getLastName() {
		return this.lastName;
	}

	public Long getAccessCode() {
		return this.accessCode;
	}

	public String getPassword() {
		return this.password;
	}

	public User(String name, String lastName, Long accessCode, String password) {
		this.name = name;
		this.lastName = lastName;
		this.accessCode = accessCode;
		this.password = password;
	}
	
	public boolean isValidPassword(String password) {
		if(this.password.equals(password))
			return true;
		else
			return false;
	}

}
