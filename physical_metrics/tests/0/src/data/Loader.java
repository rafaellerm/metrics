package data;
     
import core.ClothingProduct;
import core.ElectronicProduct;
import core.User;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
     
public interface Loader {
     
	public abstract Map<Long, User> parseUsers(String filePath);
    
    public abstract Map<Long, ClothingProduct> parseClothing(String filePath);
    
    public abstract Map<Long, ElectronicProduct> parseElectronics(String filePath);
     
    public abstract Double purchasePerDay(String filePath, String date);
          
    public void updateClothingAmount(String filePath) throws FileNotFoundException, IOException;
    
    public void updateElectronicAmount(String filePath) throws FileNotFoundException, IOException;
     
}

