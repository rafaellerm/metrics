package data;

import java.util.HashMap;
import java.util.Map;


public abstract class StoreSearch {
	
	protected Map<Long, Object> bufferOut;

	
	public StoreSearch() {
		this.bufferOut = new HashMap<>();
		clearBufferOut();
	}
	
	public abstract void setFilters(Map<String,String> filters, boolean resetMaps);
	
	public abstract void refreshBufferOut();
	
	
	public Map<Long, Object> getBufferOut() {
		return this.bufferOut;
	}
	
	public void clearBufferOut() {
		if(this.bufferOut != null)
			this.bufferOut.clear();
	}
	
	

}
