package data;

import java.util.HashMap;
import java.util.Map;

import store.Store;

import core.Customer;
import core.Manager;
import core.User;

public class UserSearch extends StoreSearch {
	
	private Map<Long, User> users;
	
	
	public UserSearch() {
		this.users = new HashMap<>();
		resetUsersMap();
	}
	
	@Override
	public void setFilters(Map<String,String> filters, boolean resetMaps) {
		
		if(resetMaps)
			resetUsersMap();
		
		Map<Long, User> usersAux = new HashMap<>();
		usersAux.putAll(this.users);
		
		for (String filterField : filters.keySet()) {
			String filterWord = filters.get(filterField);
			
			for(Long accessCode : usersAux.keySet()) {
				User user = usersAux.get(accessCode);
			
				switch(filterField) {
					case "post":
						if(filterPost(user,filterWord))
							this.users.remove(accessCode);
						break;
					
					default:
					
					break;
						
				}
			
			}
			
		}
		
		refreshBufferOut();
		
	}

	
	private boolean filterPost(User user, String filterWord) {
		
		boolean remove = false;
		
		switch(filterWord) {
			case "manager":
				if(user.getClass().equals(Manager.class))
					remove = false;
				else
					remove = true;
			break;
			case "customer":
				if(user.getClass().equals(Customer.class))
					remove = false;
				else
					remove = true;
			break;
			
			default:
			
			break;
		}
		
		return remove;
		
	}
	
	@Override
	public void refreshBufferOut() {
		this.bufferOut.putAll(this.users);
	}
	
	public void resetUsersMap() {
		this.users.putAll(Store.USERS);
	}

}
