	
package data;
     
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;
     
    import core.ClothingProduct;
import core.Customer;
import core.ElectronicProduct;
import core.Manager;
import core.Purchase;
import core.User;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import store.Store;
     
    public class FileParser implements Loader {
     
        public FileParser() {
        }
     
        @SuppressWarnings("resource")
		@Override
        public Map<Long, User> parseUsers(String filePath) {
     
            File file = new File(filePath);
     
            Map<Long, User> userMap = new HashMap<>();
     
            String[] lineAux;
            String[] nameAux;
     
     
            Scanner scanner = null;
            try {
                scanner = new Scanner(file);
     
                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();
                    lineAux = line.split(",");
                    nameAux = lineAux[1].split(" ");
                    if (lineAux[2].trim().toLowerCase().equals("cliente")) {
                        Date date = null;
                        Customer customer = new Customer(nameAux[0], nameAux[1], Long.parseLong(lineAux[0]), "123", "Example_Address", date, "999.999.999-99");
                        userMap.put(Long.parseLong(lineAux[0]), customer);
                    } else {
                        Manager manager = new Manager(lineAux[1], "Last_Name", Long.parseLong(lineAux[0]), "123");
                        userMap.put(Long.parseLong(lineAux[0]), manager);
                    }
                }
            } catch (FileNotFoundException ex) {
                Logger.getLogger(FileParser.class.getName()).log(Level.SEVERE, null, ex);
            }
            return userMap;
        }
     
        @SuppressWarnings("resource")
		@Override
        public Map<Long, ClothingProduct> parseClothing(String filePath) {
            File file = new File(filePath);
     
            Map<Long, ClothingProduct> productMap = new HashMap<>();
     
            String[] lineAux;
     
            Scanner scanner;
            try {
                scanner = new Scanner(file);
     
                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();
                    lineAux = line.split(",");
                    ClothingProduct clothing = new ClothingProduct(Long.parseLong(lineAux[0]), lineAux[1], lineAux[5], Double.parseDouble(lineAux[3]), Long.parseLong(lineAux[4]), lineAux[2], lineAux[6]);
                    productMap.put(Long.parseLong(lineAux[0]), clothing);
                }
            } catch (FileNotFoundException ex) {
                Logger.getLogger(FileParser.class.getName()).log(Level.SEVERE, null, ex);
            }
            return productMap;
        }
     
        @SuppressWarnings("resource")
		@Override
        public Map<Long, ElectronicProduct> parseElectronics(String filePath) {
            File file = new File(filePath);
     
            Map<Long, ElectronicProduct> productMap = new HashMap<>();
     
            String[] lineAux;
            Double[] dimensions = new Double[3];
     
            Scanner scanner;
            try {
                scanner = new Scanner(file);
     
                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();
                    lineAux = line.split(",");
                    dimensions[0] = Double.parseDouble(lineAux[2]);
                    dimensions[1] = Double.parseDouble(lineAux[3]);
                    dimensions[2] = Double.parseDouble(lineAux[4]);
                    ElectronicProduct electronic = new ElectronicProduct(Long.parseLong(lineAux[0]), lineAux[1], lineAux[7], Double.parseDouble(lineAux[5]), Long.parseLong(lineAux[6]), dimensions);
                    productMap.put(Long.parseLong(lineAux[0]), electronic);
                }
            } catch (FileNotFoundException ex) {
                Logger.getLogger(FileParser.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
            return productMap;
        }
     
        @SuppressWarnings("resource")
		@Override
        public Double purchasePerDay(String filePath, String date) {
            File file = new File(filePath);
     
            String[] lineAux;
            String[] lineAux2;
            Double totalPricePerDay = 0.0;
     
            Scanner scanner;
            try {
                scanner = new Scanner(file);
     
                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();
                    lineAux = line.split(";");
                    for(int i = 0; i < lineAux.length ; i++) {
                    	if(lineAux[0].contains(date))
                    		if(lineAux[i].contains("Total price")) {
                    			lineAux2 = lineAux[i].split(" ");
                    			totalPricePerDay += Double.parseDouble(lineAux2[lineAux2.length-1]);
                        		
                    		}
                    			
                    }
                }
            } catch (FileNotFoundException ex) {
                Logger.getLogger(FileParser.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            return totalPricePerDay;
        }
     
        public void writeLogFile(Purchase purchase, Customer customer) throws IOException {
           
            String numOfItemsByType = new String();
            String priceOfAllByProdct = new String();

            try (FileWriter aWriter = new FileWriter(Store.pathLogFile, true)) {
     
                for (EnumProducts productType : EnumProducts.values()) { //iterates over all EnumProducts elements and generates
                    numOfItemsByType = numOfItemsByType + "Total amount of "+ productType +" products: "+ purchase.getNumOfItemsByType(productType) +"; ";
                    priceOfAllByProdct = priceOfAllByProdct + "Total price spent with "+productType +" products: "+ purchase.getTotPriceByType(productType) +" "+ "; ";
                }
     
                aWriter.write(purchase.getDate() + " - " + (Long) purchase.getCustomerCode() + "; "  + numOfItemsByType + priceOfAllByProdct + "\r\n\r\n");
                aWriter.flush();
               
            }
            
            
        }
     
        public void updateClothingAmount(String filePath) throws FileNotFoundException, IOException {
            try {
                StringBuilder fileContent = new StringBuilder();
                String newLine = new String();
                
                for (Map.Entry<Long, ClothingProduct> entry : Store.CLOTHING.entrySet()) {
                    newLine += entry.getValue().getCode() + "," + entry.getValue().getDescription() + "," + entry.getValue().getClothSize() +
                    		"," + entry.getValue().getPrice() + "," + entry.getValue().getStockAmount() + "," + entry.getValue().getColor()
                    		+ "," + entry.getValue().getGender();
                    newLine += "\n";
                }
                
                fileContent.append(newLine);

               
            // Now fileContent will have updated content , which you can override into file
            FileWriter fstreamWrite = new FileWriter(filePath);
            BufferedWriter out = new BufferedWriter(fstreamWrite);
     
            out.write(fileContent.toString());
            out.close();
            //Close the input stream

        }
        catch (Exception e
     
       
            ) {//Catch exception if any
                System.err.println("Error: " + e.getMessage());
     
        }

        }
        
        public void updateElectronicAmount(String filePath) throws FileNotFoundException, IOException {
	            try {
	                StringBuilder fileContent = new StringBuilder();
	                String newLine = new String();
	                
	                for (Map.Entry<Long, ElectronicProduct> entry : Store.ELECTRONICS.entrySet()) {
	                    Double[] dimensions = entry.getValue().getDimensions();
	                    newLine += entry.getValue().getCode() + "," + entry.getValue().getDescription() + "," + dimensions[0] + 
	                    		"," + dimensions[1] + "," + dimensions[2] +
	                    		"," + entry.getValue().getPrice() + "," + entry.getValue().getStockAmount() + "," + entry.getValue().getColor();
	                    newLine += "\n";
	                }
	                
	                fileContent.append(newLine);
	
	               
	            // Now fileContent will have updated content , which you can override into file
	            FileWriter fstreamWrite = new FileWriter(filePath);
	            BufferedWriter out = new BufferedWriter(fstreamWrite);
	     
	            out.write(fileContent.toString());
	            out.close();
	            //Close the input stream
	
	        }
	        catch (Exception e
	     
	       
	            ) {//Catch exception if any
	                System.err.println("Error: " + e.getMessage());
	     
	        }

        }

    }
        

