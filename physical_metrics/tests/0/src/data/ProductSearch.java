package data;

import java.util.HashMap;
import java.util.Map;
import store.Store;
import core.Product;

public class ProductSearch extends StoreSearch {
	
	private Map<Long, Product> electronicsProducts;
	
	private Map<Long, Product> clothingProducts;

	private EnumProducts typeOfProduct;
	
	public ProductSearch() {
		this.typeOfProduct = null;
		electronicsProducts = new HashMap<>();
		clothingProducts = new HashMap<>();
		resetProductsMaps();
	}
	
	public void setTypeOfProduct(EnumProducts typeOfProduct) {
		this.typeOfProduct = typeOfProduct;
		refreshBufferOut();
	}

	@Override
	public void setFilters(Map<String,String> filters, boolean resetMaps) {
		
		if(resetMaps)
			resetProductsMaps();
		
		Map<Long, Product> products = new HashMap<>();
		
		if(this.typeOfProduct == EnumProducts.Electronic)
			products.putAll(this.electronicsProducts);
		else if(this.typeOfProduct == EnumProducts.Clothing)
			products.putAll(this.clothingProducts);
				
		Map<Long, Product> productsAux = new HashMap<>();
		productsAux.putAll(products);
		
		for (String filterField : filters.keySet()) {
			String filterWord = filters.get(filterField);
			
			for(Long accessCode : productsAux.keySet()) {
				Product product = productsAux.get(accessCode);
			
				switch(filterField) {
					case "color":
						if(filterColor(product,filterWord))
							products.remove(accessCode);
					break;
					case "price":
						if(filterPrice(product,filterWord))
							products.remove(accessCode);
					break;
					case "description":
						if(filterDescription(product,filterWord))
							products.remove(accessCode);
					default:
					
					break;
						
				}
			
			}
			
		}
		
		this.electronicsProducts.clear();
		this.clothingProducts.clear();
		
		if(this.typeOfProduct == EnumProducts.Electronic)
			this.electronicsProducts.putAll(products);
		else if(this.typeOfProduct == EnumProducts.Clothing)
			this.clothingProducts.putAll(products);
		
		refreshBufferOut();
		
	}
	
	
	private boolean filterColor(Product product, String color) {
		
		boolean delete = false;
		
		if(!product.getColor().contains(color))
			delete = true;

		return delete;
		
	}
	
	private boolean filterPrice(Product product, String price) {
		
		boolean delete = false;
		
		if(Double.parseDouble(price) < product.getPrice())
			delete = true;
		
		return delete;
		
	}
	
	private boolean filterDescription(Product product, String description) {
		
		boolean delete = false;
		
		if(!product.getDescription().contains(description))
			delete = true;
		
		return delete;
		
	}

	@Override
	public void refreshBufferOut() {
		
			clearBufferOut();
			
			switch(this.typeOfProduct) {
			
				case Electronic:
					this.bufferOut.putAll(this.electronicsProducts);
				break;
				
				case Clothing:
					this.bufferOut.putAll(this.clothingProducts);
				break;
				
				default:
				break;
				
			}
			
	}
	
	
	public void resetProductsMaps() {
		this.electronicsProducts.putAll(Store.ELECTRONICS);
		this.clothingProducts.putAll(Store.CLOTHING);
	}

	
	
}
