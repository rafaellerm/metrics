package test;


import org.junit.Test;

import core.ClothingProduct;
import core.ShoppingCart;

public class ShoppingCartTest {

	@Test
	public void testAddProduct() {
		ShoppingCart shoppingCart2 = new ShoppingCart();
		ClothingProduct product = new ClothingProduct(1l,"Produto", "azul", 22.50, 7l, "M","masculino");
		shoppingCart2.addProduct(product, 2);
		
		shoppingCart2.addProduct(product, 0);
			
	}

	@Test
	public void testGetQuantityProductInCart() {
		ShoppingCart shoppingCart2 = new ShoppingCart();
		ClothingProduct product = new ClothingProduct(1l,"Produto", "azul", 22.50, 7l, "M","masculino");
		shoppingCart2.addProduct(product, 2);
		
		shoppingCart2.getQuantityProductInCart(product);
	}

	@Test
	public void testGetProductFromCart() {
		ShoppingCart shoppingCart2 = new ShoppingCart();
		ClothingProduct product = new ClothingProduct(1l,"Produto", "azul", 22.50, 7l, "M","masculino");
		shoppingCart2.addProduct(product, 2);
		
		shoppingCart2.getProductFromCart(1l);
	}


}
