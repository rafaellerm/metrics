package test;


import org.junit.Test;

import core.ClothingProduct;
import core.Purchase;
import core.ShoppingCart;
import data.EnumProducts;

public class PurchaseTest {

	@Test
	public void testGetNumOfItemsByType() {
		ShoppingCart shoppingCart = new ShoppingCart();
		Purchase purchase = new Purchase(2l, shoppingCart);
		
		purchase.getNumOfItemsByType(null);
		
		ShoppingCart shoppingCart2 = new ShoppingCart();
		ClothingProduct product = new ClothingProduct(1l,"Produto", "azul", 22.50, 7l, "M","masculino");
		shoppingCart2.addProduct(product, 2);
		Purchase purchase2 = new Purchase(2l, shoppingCart2);
		
		purchase2.getNumOfItemsByType(EnumProducts.Electronic);
		
	}

	@Test
	public void testGetTotPriceByType() {
		ShoppingCart shoppingCart = new ShoppingCart();
		Purchase purchase = new Purchase(2l, shoppingCart);
		
		purchase.getNumOfItemsByType(null);
		
		ShoppingCart shoppingCart2 = new ShoppingCart();
		ClothingProduct product = new ClothingProduct(1l,"Produto", "azul", 22.50, 7l, "M","masculino");
		shoppingCart2.addProduct(product, 2);
		Purchase purchase2 = new Purchase(2l, shoppingCart2);
		
		purchase2.getNumOfItemsByType(EnumProducts.Electronic);
		
	}

}
