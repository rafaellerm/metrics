package test;

import static org.junit.Assert.*;

import org.junit.Test;

import core.ClothingProduct;
import core.Customer;

public class CustomerTest {

	@Test
	public void testAddShoppingCart() {
		Customer customer = new Customer();
		ClothingProduct product = new ClothingProduct(1l,"Produto", "azul", 22.50, 7l, "M","masculino");
		Integer quantity = new Integer(7);
		assertTrue(customer.addShoppingCart(product, quantity));
		
		
		Customer customer2 = new Customer();
		ClothingProduct product2 = new ClothingProduct(null,null, null, null, null, null,null);
		Integer quantity2 = null;
		customer2.addShoppingCart(product2, quantity2);
	}

	@Test
	public void testRemoveFromShoppingCart() {
		Customer customer2 = new Customer();
		ClothingProduct product2 = new ClothingProduct(null,null, null, null, null, null,null);
		Integer quantity2 = null;
		customer2.addShoppingCart(product2, quantity2);
		
		customer2.removeFromShoppingCart(null, 7);
	}


}
