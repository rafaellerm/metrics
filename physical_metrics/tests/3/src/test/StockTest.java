package test;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

import org.junit.Test;

import store.Store;
import store.core.Stock;
import util.Util;

public class StockTest {

	@Test
	public void testNewStock(){
		new Stock();
	}
	
	@Test
	public void testHasEnoughInStock() throws UnsupportedEncodingException, FileNotFoundException{
		new Store();
		assertTrue(Stock.hasEnoughInStock(1, 1));
		assertFalse(Stock.hasEnoughInStock(1, 99999));
		assertFalse(Stock.hasEnoughInStock(1, -12303));
	}
	
	@Test
	public void testGetStockProducts() throws UnsupportedEncodingException, FileNotFoundException{
		new Store();
		Util.printListOfProducts(Store.getStock());
	}
}
