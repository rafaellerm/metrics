package test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import util.Util;

import org.junit.Test;

import store.core.Client;
import store.core.Eletronic;
import store.core.Product;
import store.dbi.DBInterface;

public class DBInterfaceTest {
	
	@Test
	public void testLoadClientsFromTxt() {
	DBInterface dbInterface=new DBInterface();
	Map<String,Client> mapOfClients=new HashMap<String,Client>();
	mapOfClients=dbInterface.loadClientsFromTxt();
	Collection<Client> collOfClients= mapOfClients.values();
	System.out.println("Clients:");	
	Iterator<Client> nextClient= collOfClients.iterator();
	while(nextClient.hasNext())
	{
		Client currentClient = nextClient.next();
		System.out.println("Name: "+currentClient.getFullName()+", Access Code: "+currentClient.getAccessCode());
	}
	}
	
	
	@Test public void testLoadStockFromTxt(){
		DBInterface dbInterface=new DBInterface();
		List<Product> loadedStock = new LinkedList<Product>();
		loadedStock=dbInterface.loadStockFromTxt();
		Util.printListOfProducts(loadedStock);
	}
	
	@SuppressWarnings("deprecation")
	@Test public void testLoadSalesFromTxt() throws UnsupportedEncodingException, FileNotFoundException{
		DBInterface dbInterface=new DBInterface();
		float price;
		Date date = new Date();
		date.setDate(29);
		date.setMonth(03);
		date.setYear(100);
		price = dbInterface.loadSalesFromTxt(date);
		System.out.println("Price: "+price+", date: "+date.toString());
	}
	
	
	@Test public void testSaveStock() throws IOException{
		DBInterface dbInterface=new DBInterface();
		List<Product> loadedStock = new LinkedList<Product>();
		Eletronic newProduct = new Eletronic(1,"geladeira","verde",100,6,1,1,1);
		//loadedStock=dbInterface.loadStockFromTxt();
		loadedStock.add(newProduct);
		dbInterface.saveStock(loadedStock);
	}
}