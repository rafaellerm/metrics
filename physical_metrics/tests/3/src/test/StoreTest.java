package test;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import org.junit.Test;

import store.Store;
import store.core.Client;
import util.Util;

public class StoreTest {
	@Test public void testNewStore() throws UnsupportedEncodingException, FileNotFoundException{
		Store store = new Store();
	}
	
	@Test public void testGetClientsList() throws UnsupportedEncodingException, FileNotFoundException{
		Store store = new Store();
		Map<String,Client> mapOfClients=Store.getClientsList();
		Collection<Client> collOfClients= mapOfClients.values();
		Iterator<Client> nextClient=collOfClients.iterator();
		while(nextClient.hasNext()){
			Client currentClient=nextClient.next();
			System.out.println("Client name: "+currentClient.getFullName()+".");
		}
	}
	@Test public void testGetStock() throws UnsupportedEncodingException, FileNotFoundException{
		Store store = new Store();
		Util.printListOfProducts(Store.getStock());
	}
	@Test public void testGetManager() throws UnsupportedEncodingException, FileNotFoundException{
		Store store = new Store();
		System.out.println("Manager name: "+Store.getManager().getFullName()+".");
		return;
	}
	
}
