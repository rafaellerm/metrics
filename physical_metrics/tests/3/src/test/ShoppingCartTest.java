package test;

import org.junit.Test;

import store.core.Product;
import store.core.ShoppingCart;
import util.Util;

public class ShoppingCartTest {
	
	@Test
	public void testInsertAndRemoveProduct(){
		ShoppingCart shoppingCart = new ShoppingCart();
		Product product = new Product(1,"geladeira","preto",10,1);
		Product product2 = new Product(2,"geladeira","preto",5,5);
		shoppingCart.insertProduct(product);
		shoppingCart.insertProduct(product2);
		Util.printListOfProducts(shoppingCart.getListOfProducts());
		shoppingCart.removeProduct(product2.getCode(),1);
		Util.printListOfProducts(shoppingCart.getListOfProducts());
	}

}
