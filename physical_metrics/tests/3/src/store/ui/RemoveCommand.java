package store.ui;

import java.io.IOException;

import store.core.Product;

public class RemoveCommand extends Command {
	
	public RemoveCommand(){
		setEnabled(false);
	}
	
	@Override
	public void execute(StoreInterface storeInterface) {
		int code = 0;
		int quantity=0;
		boolean inputsAreCorrect=false;
		while(!inputsAreCorrect){
			System.out.println("Enter the product code:");
		try {
			code = Integer.parseInt(storeInterface.reader.readLine());
			inputsAreCorrect=true;
			} catch (NumberFormatException e) {
			System.out.println("Input is invalid.");
			} catch (IOException e) {
			System.out.println("Input is invalid.");
		}
		
		
		}
		inputsAreCorrect=false;
		//reseting the control variable
		while(!inputsAreCorrect){
			System.out.println("Enter the quantity you want to remove from the cart:");
		try {			
			quantity = Integer.parseInt(storeInterface.reader.readLine());
			inputsAreCorrect=true;
			} catch (NumberFormatException e) {
			System.out.println("Input is invalid.");
			} catch (IOException e) {
			System.out.println("Input is invalid.");
			}
		}
		if (quantity > 0){
			boolean isValid=((ClientInterface)storeInterface).getUser().getShoppingCart().hasEnoughInShoppingCart(code,quantity);
			if(isValid){
				((ClientInterface)storeInterface).getUser().getShoppingCart().removeProduct(code,quantity);
				return;
			}
		}
		
		System.out.println("Invalid data.");
		return;	
	}
}
