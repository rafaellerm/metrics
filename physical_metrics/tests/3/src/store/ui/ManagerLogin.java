package store.ui;

import java.io.IOException;

import store.core.Manager;

public class ManagerLogin extends Command{
	
	public ManagerLogin(){
		setEnabled(true);
	}

	@Override
	public void execute(StoreInterface storeInterface) throws IOException {
		Manager manager = (Manager) storeInterface.readUser("MANAGER");
		System.out.print("Password: ");
		String password = storeInterface.reader.readLine();

		if (manager != null) {
			boolean validPassword = manager.isValidPassword(
					password);
			if (validPassword) {
				((ManagerInterface)storeInterface).login(manager);
				return;
			}
		}
		System.out.println("Invalid data!");
	}

		
}
	

