package store.ui;

import java.io.IOException;

public class CancelCommand extends Command{
	
	public CancelCommand(){
		setEnabled(false);
	}
	
	@Override
	public void execute(StoreInterface storeInterface) throws IOException {
		((ClientInterface)storeInterface).getUser().getShoppingCart().emptyShoppingCart();
		((ClientInterface)storeInterface).cancel();
		System.out.println("Purchase status: canceled.");
		return;
	}
}
