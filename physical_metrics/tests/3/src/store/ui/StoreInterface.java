package store.ui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import store.Store;
import store.core.Client;
import store.core.User;

public abstract class StoreInterface {

	protected Map<String, Command> commands;
	protected final BufferedReader reader;

	protected StoreInterface() {
		this.reader = new BufferedReader(new InputStreamReader(System.in));
		this.commands = new HashMap<>();
	}

	 public void createAndShowUI() throws IOException {
		  Command command = null;
		  boolean getBack = false;
		  do {
		   System.out.print(getMenu());
		   String commandKey = reader.readLine();
		   if (commandKey.equalsIgnoreCase("B"))
		    getBack = true;
		   command = commands.get(commandKey);
		   if (command != null && command.isEnabled()) {
		    command.execute(this);
		   }else
				if(!commandKey.equalsIgnoreCase("B"))
					System.out.println("Invalid Option.");
		   
		   
		  } while (!getBack);
		  if(isLoggedIn()){
			  if (this.getUser().getAccessCode() == Store.getManager().getAccessCode())
				 logout();
			 else
			 {	 command = commands.get("N");
			 	 if(!command.isEnabled())
			 		 ((ClientInterface)this).cancel();
				 logout();
			 }
		  }
			 
		 }

	public abstract User getUser();
	
	protected String getMenu() {
		StringBuffer sb = new StringBuffer();
		sb.append("Choose the store interface (or B to get back to Start Menu):\n");
		for (String key : commands.keySet()) {
			Command command = commands.get(key);
			if (command.isEnabled()) {
				sb.append(key).append(" - ")
						.append(command.getClass().getSimpleName())
						.append("\n");
			}
		}
		sb.append("Chosen option: ");

		return sb.toString();
	}

	/**
	 * @return the reader
	 */
	public BufferedReader getReader() {
		return reader;
	}

	public abstract boolean isLoggedIn();
	
	public abstract void logout();

	public User readUser(String user)  {
		String accessCode = null;
		Integer accessCodeManager = null;
		boolean isValid=false;
		while(!isValid){
			System.out.print("Access Code: ");
		try {
			accessCode = reader.readLine();
			accessCodeManager=Integer.parseInt(accessCode);
			isValid=true;
		} catch (NumberFormatException | IOException e) {
  			System.out.println("Invalid input.");
  		}
		}
		
		if(user == "CLIENT"){
			Client client = Store.getClientsList().get(accessCode);
			if (client != null) {
				return client;
			}
		}
		else{
			if (user == "MANAGER"){	
				
				
				if ( accessCodeManager == Store.getManager().getAccessCode()) {
					return Store.getManager();
				}
			}
		}
		return null;
	}
	

	
	protected void toogleCommands(String option) {
		if (option.equals("NEW PURCHASE") || option.equals("FINISH") || option.equals("CANCEL")){
			for (String key : commands.keySet()) {
				if (!(key.equals("L"))){
					Command command = commands.get(key);
					command.setEnabled(!command.isEnabled());
					}				
			}
		}
		else
		if (option.equals("LOGOUT") || option.equals("LOGIN")){
			for (String key : commands.keySet()) {
				
				if (key.equals("N") || key.equals("O") ||key.equals("L")){
					Command command = commands.get(key);
					command.setEnabled(!command.isEnabled());
				}
			}
		}
}
	
	
	protected void toogleCommands() {
		for (String key : commands.keySet()) {
			Command command = commands.get(key);
			command.setEnabled(!command.isEnabled());
		}
	}
	
	
}

	
