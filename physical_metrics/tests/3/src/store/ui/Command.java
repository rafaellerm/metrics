/**
 * 
 */
package store.ui;

import java.io.IOException;

/**
 * @author ingrid
 * 
 */
public abstract class Command {

	private boolean isEnabled;

	public abstract void execute(StoreInterface storeInterface)
			throws IOException;

	/**
	 * @return the isEnabled
	 */
	public boolean isEnabled() {
		return isEnabled;
	}

	/**
	 * @param isEnabled
	 *            the isEnabled to set
	 */
	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}
}
