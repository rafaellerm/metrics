package store.ui;

import store.core.Manager;

public class ManagerInterface extends StoreInterface{

	private Manager manager;
	
	public ManagerInterface(){
		this.manager = null;
		this.commands.put("M", new ManagerLogin());
		this.commands.put("R", new ReportSalesCommand());
		this.commands.put("S", new SearchCommand());
		this.commands.put("P", new ReportStockCommand());
		this.commands.put("C", new ReportClientsCommand());
		this.commands.put("O", new LogoutCommand());
	}

	@Override
	public boolean isLoggedIn() {
		return manager != null;
	}

	
	public void login(Manager manager) {
		this.manager=manager;
		if (isLoggedIn()) {
			toogleCommands();
		}
	}
	
	@Override
	public void logout() {
		this.manager=null;
		toogleCommands();
	}

	@Override
	public Manager getUser() {
		return manager;
	}

}
