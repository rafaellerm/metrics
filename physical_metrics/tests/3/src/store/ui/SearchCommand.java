package store.ui;

import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;

import store.Store;
import store.core.Product;
import store.core.ProductComparator;
import util.Util;


public class SearchCommand extends Command {
	List<Product> filterableProducts;
	public SearchCommand(){
		setEnabled(false);
	}
	
	@Override
	public void execute(StoreInterface storeInterface) throws IOException  {
		filterableProducts=Store.getStock();
		String parameterType = "";
		boolean exit=true;
		boolean isValid=false;
		while(!isValid){
		System.out.println("What type of product do you want to search? (Eletronic or Clothing)");		
		parameterType = storeInterface.reader.readLine();		
		if(parameterType.equalsIgnoreCase("eletronic")){
			parameterType="class store.core.Eletronic";
			isValid=true;
			}
		else if(parameterType.equalsIgnoreCase("clothing")){
				parameterType="class store.core.Clothing";
				isValid=true;
			}
		else
		{
			System.out.println("Invalid type.");
		}
		}
		
		String option=readSearchOption(storeInterface);
		switch(option){
		case"P":
			Float price=readPrice(storeInterface);
			searchProductInStock(parameterType, price);
			break;
		case"D":
			String description=readDescription(storeInterface);		
			searchProductInStock(parameterType, description, option);
			break;
		case"C":
			String color=readColor(storeInterface);
			searchProductInStock(parameterType, color, option);
			break;
		default:
			searchProductInStock(parameterType);
			break;
		}
		System.out.println("Results:");
		Util.printListOfProducts(filterableProducts);
		while(exit)
		{
			do{
					option = readOption(storeInterface);
				if(!(option.equalsIgnoreCase("E")||option.equalsIgnoreCase("F")||option.equalsIgnoreCase("S"))){
					System.out.println("Invalid option.");
				}
			}while(!(option.equalsIgnoreCase("E")||option.equalsIgnoreCase("F")||option.equalsIgnoreCase("S")));
			
		if(option.equalsIgnoreCase("E")){
			exit=false;
		}
		else
		if(option.equalsIgnoreCase("F"))
		{
			do{
				option = readFilterOption(storeInterface);
				if(!(option.equalsIgnoreCase("E")||option.equalsIgnoreCase("M")||option.equalsIgnoreCase("N")||option.equalsIgnoreCase("C")))
					System.out.println("Invalid option.");
			}while(!(option.equalsIgnoreCase("E")||option.equalsIgnoreCase("M")||option.equalsIgnoreCase("N")||option.equalsIgnoreCase("C")));
			switch(option){
			case"M":
				Float maximumPrice = readPrice(storeInterface);
				filterList(maximumPrice, option);
				break;
			case"N":
				Float minimumPrice = readPrice(storeInterface);
				System.out.println("Enter the minimum price:");
				filterList(minimumPrice, option);		
				break;
			case"C":
				String color= readColor(storeInterface);
				filterList(color);
				break;
			case"E":
				exit=false;
				break;			
			}
		}
		else{
			do{
				option = readSortOption(storeInterface);
				if(!(option.equalsIgnoreCase("D")||option.equalsIgnoreCase("C")||option.equalsIgnoreCase("E")))
					System.out.println("Invalid option.");
			}while(!(option.equalsIgnoreCase("D")||option.equalsIgnoreCase("C")||option.equalsIgnoreCase("E")));
			switch(option){
			case"D":
				sortList("Decrescent");
				break;
			case"C":
				sortList("Crescent");
				break;
			case"E":
				exit=false;
				break;
			}
		}
		System.out.println("Results:");
		Util.printListOfProducts(filterableProducts);
		}
	}
	
	
	public String readSortOption(StoreInterface storeInterface) {
		boolean isValid=false;
		String option = null;
		while(!isValid){
		System.out.println("How do you want to sort the list?");
		System.out.println("D - Decrescent;");
		System.out.println("C - Crescent.");
		System.out.println("E - Exit;");
		try {
			option=storeInterface.reader.readLine();
			isValid=true;
		} catch (IOException e) {
			System.out.println("Invalid option.");
		}		
		}
		return option;
	}
	
	public String readDescription(StoreInterface storeInterface) {
		boolean isValid=false;
		String description = null;
		while(!isValid){
			System.out.println("Enter the description:");
		try {
			description = storeInterface.reader.readLine();
			isValid=true;
		} catch (IOException e) {
			System.out.println("Invalid input.");
		}
		}
			return description;
	}
	
	public Float readPrice(StoreInterface storeInterface){
		Float price = (float) 0;
		boolean isValid=false;
		while(!isValid){
			System.out.println("Enter the price:");
		try {
			price = Float.parseFloat(storeInterface.reader.readLine());
			isValid=true;
		} catch (NumberFormatException | IOException e) {
			System.out.println("Invalid Input");
		}
		}
		return price;
	}
	
	public String readColor(StoreInterface storeInterface) throws IOException{
		boolean isValid=false;
		String color = null;
		while(!isValid){
			System.out.println("Enter the color:");
		try {
			color = storeInterface.reader.readLine();
			isValid=true;
		} catch (IOException e) {
			System.out.println("Invalid input.");
		}
		}
			return color;
	}
	
	public String readOption(StoreInterface storeInterface) throws IOException{
		boolean isValid=false;
		String option = null;
		while(!isValid){		
			System.out.println("What do you want to do now?");
			System.out.println("F - Filter the list of found products;");
			System.out.println("S - Sort the list of found products by price;");
			System.out.println("E - Exit;");
		try {
			option = storeInterface.reader.readLine();
			isValid=true;
		} catch (IOException e) {
			System.out.println("Invalid input.");
		}
		}
			return option;
	}
	
	public String readFilterOption(StoreInterface storeInterface) throws IOException{
		boolean isValid=false;
		String option = null;
		while(!isValid){				
			System.out.println("Choose which parameter you want to use to filter:");
			System.out.println("M - Maximum price;");
			System.out.println("N - Minimum price;");
			System.out.println("C - Color.");
			System.out.println("E - Exit;");
		try {
			option = storeInterface.reader.readLine();
			isValid=true;
		} catch (IOException e) {
			System.out.println("Invalid input.");
		}
		}
			return option;
	}
	
	
	private String readSearchOption(StoreInterface storeInterface) throws IOException {
		boolean isValid=false;
		String option = null;
		while(!isValid){				
			System.out.println("Choose which parameter you want to use to search:");
			System.out.println("P - Price;");
			System.out.println("D - Description;");
			System.out.println("C - Color;");
			System.out.println("N - None.");
		try {
			option = storeInterface.reader.readLine();
			isValid=true;
		} catch (IOException e) {
			System.out.println("Invalid input.");
		}
		}
			return option;
	}

	public void searchProductInStock(String type, String property, String option){
		Iterator<Product> nextProduct = filterableProducts.iterator();
		List<Product> temporaryProducts= new ArrayList<Product>();
		Product currentProduct;
		if (option.equalsIgnoreCase("C")){
			do{
				currentProduct = nextProduct.next();	
				if((currentProduct.getColor().equalsIgnoreCase(property))&&(currentProduct.getClass().toString().equalsIgnoreCase(type))){
					temporaryProducts.add(currentProduct);
				}
			}while(nextProduct.hasNext());
			filterableProducts=temporaryProducts;
			return;
		}else{
			do{
				currentProduct = nextProduct.next();
				if(currentProduct.getDescription().contains(property)){
					temporaryProducts.add(currentProduct);
				}
			}while(nextProduct.hasNext());
			filterableProducts=temporaryProducts;
			return;
		}
		
	}
		
	public void searchProductInStock(String type, Float price){
		Iterator<Product> nextProduct = filterableProducts.iterator();
		List<Product> temporaryProducts= new ArrayList<Product>();
		Product currentProduct;
		do{
			currentProduct = nextProduct.next();	
			if((price.equals(currentProduct.getPrice()))&&(currentProduct.getClass().toString().equalsIgnoreCase(type))){
				temporaryProducts.add(currentProduct);
			}
		}while(nextProduct.hasNext());
		filterableProducts=temporaryProducts;
		return;
	}
	
	
	public void searchProductInStock(String type){
		Iterator<Product> nextProduct = filterableProducts.iterator();
		List<Product> temporaryProducts= new ArrayList<Product>();		
		Product currentProduct;
		do{
			currentProduct = nextProduct.next();	
			if(currentProduct.getClass().toString().equalsIgnoreCase(type)){
				temporaryProducts.add(currentProduct);
			}
		}while(nextProduct.hasNext());
		filterableProducts=temporaryProducts;
		return;
	}
	
	public void searchProductInStock(String type, Integer code){
		Iterator<Product> nextProduct = filterableProducts.iterator();
		List<Product> temporaryProducts= new ArrayList<Product>();		
		Product currentProduct;
		do{
			currentProduct = nextProduct.next();
			if(code.equals((currentProduct.getCode()))&&(currentProduct.getClass().toString().equalsIgnoreCase(type))){
				temporaryProducts.add(currentProduct);
			}
		}while(nextProduct.hasNext());
		filterableProducts=temporaryProducts;
		return;
	}
	
	public void filterList(float boundPrice, String option){
		Iterator<Product> nextProduct = filterableProducts.iterator();
		List<Product> temporaryProducts= new ArrayList<Product>();		
		Product currentProduct;
		if (option.equalsIgnoreCase("M")){
			while(nextProduct.hasNext()){
				
				currentProduct = nextProduct.next();	
				if(!(currentProduct.getPrice()>boundPrice)){
					temporaryProducts.add(currentProduct);
				}
				
				
			}
			filterableProducts=temporaryProducts;
			return;
		}
		else
		{
			while(nextProduct.hasNext()){
				
				currentProduct = nextProduct.next();	
				if(!(currentProduct.getPrice()<boundPrice)){
					temporaryProducts.add(currentProduct);
				}
			}
			filterableProducts=temporaryProducts;
			return;
		}
	}
	
	public void filterList(String color){
		Iterator<Product> nextProduct = filterableProducts.iterator();
		List<Product> temporaryProducts= new ArrayList<Product>();		
		Product currentProduct;
		while(nextProduct.hasNext()){
			
			currentProduct = nextProduct.next();	
			if((currentProduct.getColor().equalsIgnoreCase(color))){
				temporaryProducts.add(currentProduct);
			
			}
		}
		filterableProducts=temporaryProducts;
		return;
	}
	
	
	public void sortList(String parameter){		
		Collections.sort(filterableProducts,new ProductComparator(parameter));
	}
	
}
