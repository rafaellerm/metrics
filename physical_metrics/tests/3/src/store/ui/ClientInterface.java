package store.ui;

import store.core.Client;

public class ClientInterface extends StoreInterface {

	private Client client; 
		
	
	public ClientInterface(){
		this.client = null;
		this.commands.put("L", new ClientLogin());
		this.commands.put("N", new NewPurchaseCommand());
		this.commands.put("I", new InsertCommand());
		this.commands.put("R", new RemoveCommand());
		this.commands.put("S", new SearchCommand());
		this.commands.put("V", new ViewCartContentCommand());
		this.commands.put("F", new FinishCommand());
		this.commands.put("C", new CancelCommand());
		this.commands.put("O", new LogoutCommand());
	}

	@Override
	public boolean isLoggedIn() {
		return client != null;
	}

	public void login(Client client) {
		this.client = client;
		if (isLoggedIn()) {
			toogleCommands("LOGIN");
		}
	}
	
	@Override
	public void logout() {
		this.client=null;
		toogleCommands("LOGOUT");
	}
	
	public void newPurchase() {
		toogleCommands("NEW PURCHASE");
	}
	
	public void cancel() {
		toogleCommands("CANCEL");
	}
	
	public void finish() {
		toogleCommands("FINISH");
	}
	
	@Override
	public Client getUser() {
		return client;
	}

	
}
