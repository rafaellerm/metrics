package store.ui;

import java.io.IOException;

import store.core.Product;

public class InsertCommand extends Command {
	
	public InsertCommand(){
		setEnabled(false);
	}
	
	@Override
	public void execute(StoreInterface storeInterface) {
			int quantity=0;
			
			int code=0;
			boolean inputsAreCorrect=false;
			while(!inputsAreCorrect){
			try {
			System.out.println("Enter the product code:");
			code = Integer.parseInt(storeInterface.reader.readLine());
			inputsAreCorrect=true;
			} catch (NumberFormatException e) {
			System.out.println("Input is invalid.");
			} catch (IOException e) {
			System.out.println("Input is invalid.");
			}
			}
			inputsAreCorrect=false;
			//reseting the control variable
			while(!inputsAreCorrect){			
			System.out.println("Enter the quantity you want to add to the cart:");
			try {
			quantity = Integer.parseInt(storeInterface.reader.readLine());
			inputsAreCorrect=true;
			} catch (NumberFormatException e) {
			System.out.println("Input is invalid.");
			} catch (IOException e) {
			System.out.println("Input is invalid.");
			}
			}
			if(quantity > 0){
				boolean isValid= store.Store.hasEnoughInStock(code, quantity+((ClientInterface)storeInterface).getUser().getShoppingCart().getQuantityOfAProduct(code));
				if(isValid){
					Product product=store.Store.getProduct(code);
					product.setNumberOfProducts(quantity);
					((ClientInterface)storeInterface).getUser().getShoppingCart().insertProduct(product);
					return;
				}
			}
			
			System.out.println("Invalid data.");				
			return;
	}
}