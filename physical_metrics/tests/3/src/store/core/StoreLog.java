package store.core;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import store.dbi.DBInterface;

public class StoreLog {
	
	private static List<Purchase> listOfSalesToday;
	private static Float totalMoneyEarnedToday;
	
	public StoreLog(){
		StoreLog.totalMoneyEarnedToday=(float) 0;
		StoreLog.listOfSalesToday=new ArrayList<Purchase>();
	}
	
	public static void setSalesFromToday(float moneyEarnedToday){
		StoreLog.totalMoneyEarnedToday=moneyEarnedToday;
	}	
	public static Float getSalesFromToday(){
		return StoreLog.totalMoneyEarnedToday;
	}
	public static void updateLog(Purchase purchase){
		Float totalPriceOfPurchase = purchase.getTotalPriceOfClothing()+purchase.getTotalPriceOfEletronics();
		totalMoneyEarnedToday+=totalPriceOfPurchase;
		//listOfSalesToday.add(purchase);
	}
	
	public static float getSales(Date dateToBeChecked) throws UnsupportedEncodingException, FileNotFoundException{
		Date today = new Date();
		if (today.getDate() == dateToBeChecked.getDate() && today.getMonth() == dateToBeChecked.getMonth() && today.getYear() == dateToBeChecked.getYear())
			return totalMoneyEarnedToday;
		DBInterface loader = new DBInterface();
		return loader.loadSalesFromTxt(dateToBeChecked);
	}
}
