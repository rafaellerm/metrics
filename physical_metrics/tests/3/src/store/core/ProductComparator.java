package store.core;

import java.util.Comparator;

public class ProductComparator implements Comparator<Product>{
 
 private String order;
 
 public ProductComparator(String order){
  this.order = order;
 }
 
 @Override
    public int compare (Product firstProduct, Product secondProduct) {
  if (order.equals("Code"))
   return firstProduct.getCode() > secondProduct.getCode() ? 1 : (firstProduct.getCode() < secondProduct.getCode() ? -1 : 0);
  else
  if (order.equals("Crescent"))
   return firstProduct.getPrice() > secondProduct.getPrice() ? 1 : (firstProduct.getPrice() < secondProduct.getPrice() ? -1 : 0);
  else
   return firstProduct.getPrice() < secondProduct.getPrice() ? 1 : (firstProduct.getPrice() > secondProduct.getPrice() ? -1 : 0);
    }
}