package store.core;

public class Eletronic extends Product {
	
	private float heigth;
	private float width;
	private float length;
	
	public Eletronic(int code, String description, String color, float price,int quantity, float x,float y,float z){
		super(code, description, color, price,quantity);
		this.heigth=x;
		this.width=y;
		this.length=z;
		
	}
	public Eletronic(){
		super();
		this.heigth=0;
		this.width=0;
		this.length=0;
	}
	
	public float[] getDimensions(){
		float[] dimensions=new float[3];
		dimensions[0]=this.heigth;
		dimensions[1]=this.width;
		dimensions[2]=this.length;
		return dimensions;
	}

	public void setDimensions(float heigth,float width,float length){
		this.heigth=heigth;
		this.width=width;
		this.length=length;
	}

}
