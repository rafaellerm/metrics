USER_DELAY=1
ITERATIONS=30

APP_START_COUNT=0
APP_END_COUNT=5

RESULTS_DIR=results

PERF_LEVEL=""
rm -rf $RESULTS_DIR
mkdir -p $RESULTS_DIR
for i in $(seq $APP_START_COUNT $APP_END_COUNT)
do
	echo APP $i
	OUTPUT_DIR=${RESULTS_DIR}/${i}
	mkdir -p ${OUTPUT_DIR}
	for j in $(seq 1 $ITERATIONS)
	do
		echo IT $j
		./simulate_user.sh $i $USER_DELAY $PERF_LEVEL > /dev/null

		cat perf.log >> ${OUTPUT_DIR}/perf.log
		cat cache.log >> ${OUTPUT_DIR}/cache.log
		rm -f perf.log cache.log
	done
done
mv ${RESULTS_DIR} all

PERF_LEVEL=":u"
rm -rf $RESULTS_DIR
mkdir -p $RESULTS_DIR
for i in $(seq $APP_START_COUNT $APP_END_COUNT)
do
	echo APP $i
	OUTPUT_DIR=${RESULTS_DIR}/${i}
	mkdir -p ${OUTPUT_DIR}
	for j in $(seq 1 $ITERATIONS)
	do
		echo IT $j
		./simulate_user.sh $i $USER_DELAY $PERF_LEVEL > /dev/null

		cat perf.log >> ${OUTPUT_DIR}/perf.log
		cat cache.log >> ${OUTPUT_DIR}/cache.log
		rm -f perf.log cache.log
	done
done
mv ${RESULTS_DIR} user

PERF_LEVEL=":k"
rm -rf $RESULTS_DIR
mkdir -p $RESULTS_DIR
for i in $(seq $APP_START_COUNT $APP_END_COUNT)
do
	echo APP $i
	OUTPUT_DIR=${RESULTS_DIR}/${i}
	mkdir -p ${OUTPUT_DIR}
	for j in $(seq 1 $ITERATIONS)
	do
		echo IT $j
		./simulate_user.sh $i $USER_DELAY $PERF_LEVEL > /dev/null

		cat perf.log >> ${OUTPUT_DIR}/perf.log
		cat cache.log >> ${OUTPUT_DIR}/cache.log
		rm -f perf.log cache.log
	done
done
mv ${RESULTS_DIR} kernel
